/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.9-MariaDB : Database - fotocopy
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fotocopy` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `fotocopy`;

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `address` text,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `admin_id` int(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `status_active` varchar(50) DEFAULT 'active' COMMENT 'active/inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/*Data for the table `customers` */

insert  into `customers`(`id`,`name`,`address`,`phone`,`email`,`birthday`,`city`,`admin_id`,`created_date`,`last_modified_date`,`status_active`) values (5,'Windy Hendra Supardi','Jalan Kusbini','089693230603','reinarduswindy@gmail.com','2016-01-01 00:00:00','Yogyakarta',1,'2016-01-19 00:24:13','2016-01-19 21:27:42','active'),(6,'sdfsdfsdf','asdasd','sadasd','sadasf','1994-12-05 12:00:00','asfasd',1,'2016-01-19 00:28:50',NULL,'inactive'),(7,'Windy Hendra Supardi','Jalan Kusbini','089693230603','reinarduswindy@gmail.com','1994-12-05 12:00:00','Yogyakarta',1,'2016-01-19 01:26:33',NULL,'inactive'),(8,'windy hendra supardi 123125234','Jalan Kusbini','08912312324123','reinarduswindy@gmail.com','2016-01-04 00:00:00','jakarta',1,'2016-01-19 02:09:11','2016-01-19 02:09:11','inactive'),(9,'windy hendra supardi 123','Jalan Kusbini','08912312324123','reinarduswindy@gmail.com','0000-00-00 00:00:00','jakarta',1,'2016-01-19 02:12:15','2016-01-19 02:12:15','inactive'),(10,'ehehehe','hehehe',NULL,NULL,'2016-01-19 11:29:58',NULL,1,'2016-01-19 11:30:00','2016-01-19 11:30:02','inactive'),(11,'Windy Hendra S','Jalan Kusbini','089693230603','windy.hendra@ti.ukdw.ac.id','1994-12-05 00:00:00','Sintang',1,'2016-01-19 20:41:37','2016-01-19 20:49:16','active'),(12,'Yustinus Arjuna Purnama Putra','Jalan Magelang','085252624451','yustinus@gmail.com','1994-12-01 00:00:00','Yogyakarta',1,'2016-01-19 20:51:34','2016-01-19 20:52:04','active'),(13,'Windy','Jalan Kusbini','1203123','','1970-01-01 01:00:00','',1,'2016-01-19 20:53:48','2016-01-19 20:53:48','active'),(14,'windyyy','jalan klitren\r\n','12321332','','1970-01-01 01:00:00','',1,'2016-01-19 20:54:09','2016-01-19 20:54:09','active'),(15,'Windy Hendra','Jalan jalan','0124123','','1970-01-01 01:00:00','',1,'2016-01-19 20:57:35','2016-01-19 20:57:35','active'),(16,'safdf','asdgfasd','123123','','1970-01-01 01:00:00','',1,'2016-01-19 20:57:48','2016-01-19 20:57:48','active'),(17,'heheheh','rasdsada','34124123','','1970-01-01 01:00:00','',1,'2016-01-19 20:57:57','2016-01-19 20:57:57','active'),(18,'fegeege','egegagsadde','123125123','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:08','2016-01-19 20:58:08','active'),(19,'rheeheheh','gsagweffd','12312313','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:19','2016-01-19 20:58:19','active'),(20,'dgdbdf','agsdfdfds','12313','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:31','2016-01-19 20:58:31','active'),(21,'sdgfsdgsdgsdf','sdbdsgvdf','12312412','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:47','2016-01-19 20:58:47','active'),(22,'egdsgsdgsdg','sdgsdgsdgsg','23124123','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:52','2016-01-19 20:58:52','active'),(23,'sdgsdgsdg','sdgsdgsd','12312','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:58','2016-01-19 20:58:58','active'),(24,'dgdgsdg','sgsdgsdgsdg','234235','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:04','2016-01-19 20:59:04','active'),(25,'ewtsgdgdg','gsdgsdggds','123124','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:12','2016-01-19 20:59:12','active'),(26,'dsggcg','sgsdgdbcvb','34242','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:17','2016-01-19 20:59:17','active'),(27,'ddfgdfbfb','sdgsdgsgsdg','24123','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:24','2016-01-19 20:59:24','active'),(28,'safgsdg','safsd','12312','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:33','2016-01-19 20:59:33','active'),(29,'gdfgdfgd','sdgsdgsdgsdg','23412412','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:37','2016-01-19 20:59:37','active'),(30,'sdgsdgsdg','sgfsdgsgsd','234124','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:42','2016-01-19 20:59:42','inactive'),(31,'dasfasff','sdgsdgsdgsdf','1312441','','1970-01-01 01:00:00','',1,'2016-01-19 21:29:03','2016-01-19 21:29:03','active'),(32,'qwe','qweqwe','08192934752','qwe@qwe.com','1980-10-23 12:00:00','qweqwe',1,'2016-01-20 23:46:06','2016-01-20 23:46:06','inactive');

/*Table structure for table `division_employees` */

DROP TABLE IF EXISTS `division_employees`;

CREATE TABLE `division_employees` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `division_employees` */

insert  into `division_employees`(`id`,`division_id`,`employee_id`,`created_date`,`last_modified_date`,`admin_id`) values (1,NULL,NULL,'2016-01-21 00:27:00','2016-01-21 00:27:00',NULL),(2,NULL,16,'2016-01-21 00:34:42','2016-01-21 00:34:42',NULL),(3,3,16,'2016-01-21 01:15:21','2016-01-21 01:15:21',NULL),(4,4,19,'2016-01-21 01:16:43','2016-01-21 01:16:43',1),(7,8,16,'2016-01-21 01:20:09','2016-01-21 01:20:09',1),(8,9,21,'2016-01-21 01:21:05','2016-01-21 01:21:05',1),(9,9,17,'2016-01-21 01:21:05','2016-01-21 01:21:05',1),(10,9,16,'2016-01-21 01:21:05','2016-01-21 01:21:05',1),(11,9,22,'2016-01-21 01:21:05','2016-01-21 01:21:05',1),(12,10,22,'2016-01-21 01:28:14','2016-01-21 01:28:14',1),(13,10,16,'2016-01-21 01:28:14','2016-01-21 01:28:14',1);

/*Table structure for table `divisions` */

DROP TABLE IF EXISTS `divisions`;

CREATE TABLE `divisions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `divisions` */

insert  into `divisions`(`id`,`name`,`created_date`,`last_modified_date`,`admin_id`) values (1,'Fotocopy 1','2016-01-21 12:26:59','2016-01-21 00:27:00',1),(2,'asdfsadasd','2016-01-21 12:34:42','2016-01-21 00:34:42',1),(3,'fsafasd','2016-01-21 01:15:21','2016-01-21 01:15:21',1),(4,'asdassafasd','2016-01-21 01:16:43','2016-01-21 01:16:43',1),(8,'asfasdad','2016-01-21 01:20:09','2016-01-21 01:20:09',1),(9,'asfasdasd','2016-01-21 01:21:05','2016-01-21 01:21:05',1),(10,'Fotocopy 213','2016-01-21 01:28:14','2016-01-21 01:28:14',1);

/*Table structure for table `employees` */

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` text,
  `phone` varchar(50) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `base_salary` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `join_date` datetime DEFAULT NULL,
  `resign_date` datetime DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `status_active` varchar(50) DEFAULT 'active' COMMENT 'active/inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `employees` */

insert  into `employees`(`id`,`name`,`address`,`phone`,`division`,`base_salary`,`created_date`,`join_date`,`resign_date`,`admin_id`,`last_modified_date`,`status_active`) values (16,'Windy Hendra Supardi','Jalan Magelang','089693230603','Divisi 3',12000,'2016-01-20 12:24:58',NULL,NULL,1,'2016-01-20 00:24:58','active'),(17,'Windy Hendra Supardiiiii','Jalan Jalan','085252624451','Divisi 5',1231312,'2016-01-20 12:25:15',NULL,NULL,1,'2016-01-20 00:25:42','active'),(18,'Windy Hendra','ajlasdnslafka','090124021231','21sdgdg',1231254123,'2016-01-20 12:25:26',NULL,NULL,1,'2016-01-20 00:25:26','inactive'),(19,'windy','jalan','0123','divisi 123',2132,'2016-01-20 01:00:12',NULL,'0000-00-00 00:00:00',1,'2016-01-20 01:00:12','active'),(20,'asfassad','safasfasd','123123','sdfsdgsd',121231,'2016-01-20 01:01:12','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'2016-01-20 01:01:12','active'),(21,'fasfasd','afasfasd','12313','12asfs',123,'2016-01-20 01:03:19','2016-01-01 12:00:00','2016-01-31 12:00:00',1,'2016-01-20 01:03:19','active'),(22,'Windy Hendra','Jalan jalan','09131293','Divisi 1231242',12312412,'2016-01-20 01:18:57','2016-01-01 12:00:00','1970-01-01 01:00:00',1,'2016-01-20 01:18:57','active'),(23,'asfsgsfs','asfsagsdgsf','12312412','agsdgsdf',123123,'2016-01-20 01:20:42','2016-01-01 12:00:00',NULL,1,'2016-01-20 01:20:42','active');

/* Trigger structure for table `test` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `lina_asu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `lina_asu` AFTER INSERT ON `test` FOR EACH ROW begin
  insert into `fotosarubanget`.`test` (test) values (new.test);
end */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
