/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.32 : Database - fotocopy
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fotocopy` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `fotocopy`;

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cash_nominal` int(11) DEFAULT NULL,
  `transfer_nominal` int(11) DEFAULT NULL,
  `transfer_bank_name` varchar(50) DEFAULT NULL,
  `transfer_bank_account` varchar(50) DEFAULT NULL,
  `transfer_name` varchar(50) DEFAULT NULL,
  `total_nominal` int(11) DEFAULT NULL,
  `payment_purpose` varchar(50) DEFAULT NULL COMMENT 'dp/paid_off/receivable',
  `payment_for` varchar(50) DEFAULT NULL COMMENT 'sales/pos/dll.',
  `payment_journal_type` varchar(50) DEFAULT NULL COMMENT 'income/outcome',
  `payment_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `admin_id` int(10) DEFAULT NULL,
  `ref_number` varchar(50) DEFAULT NULL COMMENT 'menyimpan refnumber, misal punya sale,purchase dll.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf16;

/*Data for the table `payments` */

insert  into `payments`(`id`,`cash_nominal`,`transfer_nominal`,`transfer_bank_name`,`transfer_bank_account`,`transfer_name`,`total_nominal`,`payment_purpose`,`payment_for`,`payment_journal_type`,`payment_date`,`created_date`,`last_modified_date`,`admin_id`,`ref_number`) values (8,50000,NULL,'','','',50000,'paid_off','sales','income','2016-01-21 00:00:00','2016-01-21 23:38:32','2016-01-21 23:38:32',1,'20160121-S00016'),(9,200000,NULL,'','','',200000,'dp','sales','income','2016-01-21 00:00:00','2016-01-21 23:39:16','2016-01-21 23:39:16',1,'20160121-S00017'),(10,50000,50000,'BNI','9090-9090','Yustinus',100000,'paid_off','sales','income','2016-01-21 00:00:00','2016-01-21 23:40:08','2016-01-21 23:40:08',1,'20160121-S00017'),(11,NULL,NULL,'','','',0,'paid_off',NULL,NULL,'0000-00-00 00:00:00','2016-01-21 23:50:16','2016-01-21 23:50:16',NULL,NULL),(12,50000,NULL,'','','',50000,'dp','sales','income','2016-01-21 00:00:00','2016-01-21 23:51:42','2016-01-21 23:52:21',1,'20160121-S00019'),(13,NULL,NULL,'','','',0,'paid_off',NULL,NULL,'0000-00-00 00:00:00','2016-01-21 23:53:41','2016-01-21 23:53:41',NULL,NULL),(14,50000,NULL,'','','',50000,'dp','sales','income','2016-01-21 00:00:00','2016-01-21 23:54:10','2016-01-21 23:54:10',1,'20160121-S00021'),(15,30000,NULL,'','','',30000,'dp','sales','income','2016-01-22 00:00:00','2016-01-22 00:03:35','2016-01-22 00:03:35',1,'20160122-S00001'),(16,30000,NULL,'','','',30000,'paid_off','sales','income','2016-01-22 00:00:00','2016-01-22 06:06:34','2016-01-22 06:06:34',1,'20160122-S00001'),(17,10000,NULL,'','','',10000,'paid_off','sales','income','2016-01-22 00:00:00','2016-01-22 06:12:22','2016-01-22 06:12:22',1,'20160121-S00021');

/*Table structure for table `sale_counters` */

DROP TABLE IF EXISTS `sale_counters`;

CREATE TABLE `sale_counters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `day` varchar(2) DEFAULT NULL,
  `month` varchar(2) DEFAULT NULL,
  `year` varchar(4) DEFAULT NULL,
  `counter` int(11) DEFAULT NULL,
  `last_deleted` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `sale_counters` */

insert  into `sale_counters`(`id`,`day`,`month`,`year`,`counter`,`last_deleted`) values (1,'21','01','2016',21,NULL),(2,'22','01','2016',1,NULL);

/*Table structure for table `sale_items` */

DROP TABLE IF EXISTS `sale_items`;

CREATE TABLE `sale_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `sale_items` */

insert  into `sale_items`(`id`,`product_id`,`sale_id`,`qty`,`unit_price`,`total_price`,`admin_id`,`created_date`,`last_modified_date`,`employee_id`,`division_id`) values (13,2,10,300,1000,300000,1,'2016-01-21 23:39:16','2016-01-21 23:39:16',1,1),(14,3,11,1,10000,10000,1,'2016-01-21 23:50:16','2016-01-21 23:50:16',1,1),(15,4,12,4,20000,80000,1,'2016-01-21 23:51:42','2016-01-21 23:51:42',1,2),(16,2,13,1,100000,100000,1,'2016-01-21 23:53:41','2016-01-21 23:53:41',1,1),(17,3,14,1,100000,100000,1,'2016-01-21 23:54:10','2016-01-21 23:54:10',1,2),(18,3,15,5,9000,45000,1,'2016-01-22 00:03:35','2016-01-22 00:03:35',1,1);

/*Table structure for table `sales` */

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ref_number` varchar(50) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `total_nominal` int(11) DEFAULT NULL,
  `status_sales` varchar(50) DEFAULT 'unpaid' COMMENT 'paid/unpaid',
  `status_payment` varchar(50) DEFAULT NULL COMMENT 'dp/paid_off',
  `sales_type` varchar(50) DEFAULT NULL COMMENT 'order/onsite',
  `created_date` datetime DEFAULT NULL,
  `admin_id` int(10) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `status_active` varchar(50) DEFAULT 'active' COMMENT 'active/inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `sales` */

insert  into `sales`(`id`,`ref_number`,`customer_id`,`total_nominal`,`status_sales`,`status_payment`,`sales_type`,`created_date`,`admin_id`,`last_modified_date`,`status_active`) values (9,'20160121-S00016',1,90000,'paid','paid_off','onsite','2016-01-21 23:38:32',1,'2016-01-21 23:38:47','inactive'),(10,'20160121-S00017',1,300000,'paid','paid_off','onsite','2016-01-21 23:39:15',1,'2016-01-21 23:40:08','active'),(11,'20160121-S00018',1,10000,'unpaid','paid_off','order','2016-01-21 23:50:16',1,'2016-01-21 23:50:16','active'),(12,'20160121-S00019',1,80000,'paid','dp','order','2016-01-21 23:51:42',1,'2016-01-21 23:51:42','active'),(13,'20160121-S00020',1,100000,'unpaid','paid_off','onsite','2016-01-21 23:53:41',1,'2016-01-21 23:53:41','active'),(14,'20160121-S00021',1,100000,'paid','paid_off','order','2016-01-21 23:54:10',1,'2016-01-22 06:12:22','active'),(15,'20160122-S00001',1,45000,'paid','paid_off','onsite','2016-01-22 00:03:35',1,'2016-01-22 06:06:34','active');

/* Trigger structure for table `test` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `lina_asu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `lina_asu` AFTER INSERT ON `test` FOR EACH ROW begin
  insert into `fotosarubanget`.`test` (test) values (new.test);
end */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
