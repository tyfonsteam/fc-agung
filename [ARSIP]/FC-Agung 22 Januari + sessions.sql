/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.32 : Database - fotocopy
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fotocopy` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `fotocopy`;

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `privilege` text NOT NULL,
  `status` varchar(20) DEFAULT 'active' COMMENT 'active, inactive',
  `photo` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `admins` */

insert  into `admins`(`id`,`created_date`,`username`,`password`,`name`,`address`,`phone`,`email`,`privilege`,`status`,`photo`) values (1,'2015-09-21 18:39:34','admin','$2a$10$/lpimIkKfy/zSFw0VI0ECe8heglJaem5UoUcwEKJZfH1Hwf/uwJYW','Admin','-','-','tyfons.id@gmail.com','{\"admin\":{\"dashboard\":[\"1\"],\"users\":[\"1\",\"2\",\"4\",\"8\"],\"privileges\":[\"1\",\"2\",\"4\",\"8\"],\"products\":[\"1\",\"2\",\"4\",\"8\"],\"sales\":[\"1\",\"2\",\"4\",\"8\"]}}','active',NULL),(2,'2016-01-05 10:42:37','test','$2a$10$qqmMAiFbigcuBBNlM6BX8OEP12anGm9dS7n2z/Ie.wp/JxW11fNo.','test','asdf','123123','','{\"admin\":{\"dashboard\":[\"1\"],\"users\":[\"1\",\"2\",\"4\",\"8\"],\"privileges\":[\"1\",\"2\",\"4\",\"8\"]}}','inactive',NULL);

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `admin_id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `categories` */

insert  into `categories`(`id`,`created_date`,`admin_id`,`category`) values (1,'2016-01-06 15:48:06',1,'Fotokopi'),(2,'2016-01-06 15:48:52',1,'Cp BUF Kert ');

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `address` text,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `admin_id` int(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `status_active` varchar(50) DEFAULT 'active' COMMENT 'active/inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

/*Data for the table `customers` */

insert  into `customers`(`id`,`name`,`address`,`phone`,`email`,`birthday`,`city`,`admin_id`,`created_date`,`last_modified_date`,`status_active`) values (5,'Windy Hendra Supardi','Jalan Kusbini','089693230603','reinarduswindy@gmail.com','2016-01-01 00:00:00','Yogyakarta',1,'2016-01-19 00:24:13','2016-01-19 21:27:42','active'),(6,'sdfsdfsdf','asdasd','sadasd','sadasf','1994-12-05 12:00:00','asfasd',1,'2016-01-19 00:28:50',NULL,'inactive'),(7,'Windy Hendra Supardi','Jalan Kusbini','089693230603','reinarduswindy@gmail.com','1994-12-05 12:00:00','Yogyakarta',1,'2016-01-19 01:26:33',NULL,'inactive'),(8,'windy hendra supardi 123125234','Jalan Kusbini','08912312324123','reinarduswindy@gmail.com','2016-01-04 00:00:00','jakarta',1,'2016-01-19 02:09:11','2016-01-19 02:09:11','inactive'),(9,'windy hendra supardi 123','Jalan Kusbini','08912312324123','reinarduswindy@gmail.com','0000-00-00 00:00:00','jakarta',1,'2016-01-19 02:12:15','2016-01-19 02:12:15','inactive'),(10,'ehehehe','hehehe',NULL,NULL,'2016-01-19 11:29:58',NULL,1,'2016-01-19 11:30:00','2016-01-19 11:30:02','inactive'),(11,'Windy Hendra S','Jalan Kusbini','089693230603','windy.hendra@ti.ukdw.ac.id','1994-12-05 00:00:00','Sintang',1,'2016-01-19 20:41:37','2016-01-19 20:49:16','active'),(12,'Yustinus Arjuna Purnama Putra','Jalan Magelang','085252624451','yustinus@gmail.com','1994-12-01 00:00:00','Yogyakarta',1,'2016-01-19 20:51:34','2016-01-19 20:52:04','inactive'),(13,'Windy','Jalan Kusbini','1203123','','1970-01-01 01:00:00','',1,'2016-01-19 20:53:48','2016-01-19 20:53:48','active'),(14,'windyyy','jalan klitren\r\n','12321332','','1970-01-01 01:00:00','',1,'2016-01-19 20:54:09','2016-01-19 20:54:09','active'),(15,'Windy Hendra','Jalan jalan','0124123','','1970-01-01 01:00:00','',1,'2016-01-19 20:57:35','2016-01-19 20:57:35','active'),(16,'safdf','asdgfasd','123123','','1970-01-01 01:00:00','',1,'2016-01-19 20:57:48','2016-01-19 20:57:48','active'),(17,'heheheh','rasdsada','34124123','','1970-01-01 01:00:00','',1,'2016-01-19 20:57:57','2016-01-19 20:57:57','active'),(18,'fegeege','egegagsadde','123125123','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:08','2016-01-19 20:58:08','active'),(19,'rheeheheh','gsagweffd','12312313','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:19','2016-01-19 20:58:19','active'),(20,'dgdbdf','agsdfdfds','12313','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:31','2016-01-19 20:58:31','active'),(21,'sdgfsdgsdgsdf','sdbdsgvdf','12312412','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:47','2016-01-19 20:58:47','active'),(22,'egdsgsdgsdg','sdgsdgsdgsg','23124123','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:52','2016-01-19 20:58:52','active'),(23,'sdgsdgsdg','sdgsdgsd','12312','','1970-01-01 01:00:00','',1,'2016-01-19 20:58:58','2016-01-19 20:58:58','active'),(24,'dgdgsdg','sgsdgsdgsdg','234235','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:04','2016-01-19 20:59:04','active'),(25,'ewtsgdgdg','gsdgsdggds','123124','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:12','2016-01-19 20:59:12','active'),(26,'dsggcg','sgsdgdbcvb','34242','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:17','2016-01-19 20:59:17','active'),(27,'ddfgdfbfb','sdgsdgsgsdg','24123','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:24','2016-01-19 20:59:24','active'),(28,'safgsdg','safsd','12312','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:33','2016-01-19 20:59:33','active'),(29,'gdfgdfgd','sdgsdgsdgsdg','23412412','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:37','2016-01-19 20:59:37','active'),(30,'sdgsdgsdg','sgfsdgsgsd','234124','','1970-01-01 01:00:00','',1,'2016-01-19 20:59:42','2016-01-19 20:59:42','inactive'),(31,'dasfasff','sdgsdgsdgsdf','1312441','','1970-01-01 01:00:00','',1,'2016-01-19 21:29:03','2016-01-19 21:29:03','active'),(32,'qwe','qweqwe','08192934752','qwe@qwe.com','1980-10-23 12:00:00','qweqwe',1,'2016-01-20 23:46:06','2016-01-20 23:46:06','inactive'),(33,'Yustinus','Jalan Jalan','085252624451','reinarduswindy@gmail.com','2016-01-02 12:00:00','Yogyakarta',1,'2016-01-21 19:43:28','2016-01-21 19:43:28','active'),(34,'asdsad','asdasfasd','90321','reinarduswindy@gmail.com','2016-01-01 12:00:00','asfasd',1,'2016-01-21 19:45:58','2016-01-21 19:45:58','active'),(35,'asdasdd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(36,'asdasdsda',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(37,'asfsadasdasf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(38,'asdasfdsada',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(39,'sadsafasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(40,'safasdasdasdasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(41,'asfasda',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(42,'asdafgsafdsa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(43,'sadasfgasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(44,'safsadasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(45,'asgasfdsadsa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(46,'gasfsagfsad',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(47,'asdasfasdasdas',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(48,'safasdsaasd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'active'),(49,'sadasdsa','jflsajflkas','124123','windy@gmail.com','2016-01-21 12:00:00','sdfjsdlkgsdf',1,'2016-01-21 19:54:15','2016-01-21 19:54:15','active'),(50,'aasdfsadasd','afsasd','123213','','1970-01-01 01:00:00','',1,'2016-01-21 20:55:22','2016-01-21 20:55:22','active'),(51,'asdasd','afsdsasa','123123','','1970-01-01 01:00:00','',1,'2016-01-21 20:55:29','2016-01-21 20:55:29','active'),(52,'gwesdf','fafasdas','12312','','1970-01-01 01:00:00','',1,'2016-01-21 20:55:39','2016-01-21 20:55:39','active'),(53,'gsdsfsdf','afssgasf','123213','','1970-01-01 01:00:00','',1,'2016-01-21 20:55:44','2016-01-21 20:55:44','active'),(54,'gasdfasd','agfasdas','213123','','1970-01-01 01:00:00','',1,'2016-01-21 20:55:50','2016-01-21 20:55:50','active'),(55,'asdasd','asd','082728382','asd@asd.com','2005-11-02 00:00:00','asfasd',1,'2016-01-22 00:54:12','2016-01-22 00:55:02','active'),(56,'test no telp','123123','081328456333','test@test.com','2015-11-25 12:00:00','123123',1,'2016-01-22 00:54:42','2016-01-22 00:54:42','active');

/*Table structure for table `division_employees` */

DROP TABLE IF EXISTS `division_employees`;

CREATE TABLE `division_employees` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `division_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `division_employees` */

insert  into `division_employees`(`id`,`division_id`,`employee_id`,`created_date`,`last_modified_date`,`admin_id`) values (3,3,16,'2016-01-21 01:15:21','2016-01-21 01:15:21',NULL),(7,8,16,'2016-01-21 01:20:09','2016-01-21 01:20:09',1),(17,1,16,'2016-01-21 23:32:33','2016-01-21 23:32:33',1),(18,10,20,'2016-01-22 01:03:27','2016-01-22 01:03:27',1),(19,10,16,'2016-01-22 01:03:27','2016-01-22 01:03:27',1),(20,10,17,'2016-01-22 01:03:27','2016-01-22 01:03:27',1);

/*Table structure for table `divisions` */

DROP TABLE IF EXISTS `divisions`;

CREATE TABLE `divisions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `divisions` */

insert  into `divisions`(`id`,`name`,`created_date`,`last_modified_date`,`admin_id`) values (1,'Fotocopy 1','2016-01-21 12:26:59','2016-01-21 23:32:33',1),(2,'Fotocopy 2','2016-01-21 12:34:42','2016-01-21 23:23:38',1),(3,'Fotocopy 3','2016-01-21 01:15:21','2016-01-21 23:23:56',1),(4,'Fotocopy 4','2016-01-21 01:16:43','2016-01-21 23:24:05',1),(8,'Fotocopy 5','2016-01-21 01:20:09','2016-01-21 23:24:16',1),(9,'Fotocopy 6','2016-01-21 01:21:05','2016-01-21 23:24:23',1),(10,'Divis A','2016-01-22 01:03:27','2016-01-22 01:03:27',1);

/*Table structure for table `employees` */

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` text,
  `phone` varchar(50) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `base_salary` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `join_date` datetime DEFAULT NULL,
  `resign_date` datetime DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `status_active` varchar(50) DEFAULT 'active' COMMENT 'active/inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `employees` */

insert  into `employees`(`id`,`name`,`address`,`phone`,`division`,`base_salary`,`created_date`,`join_date`,`resign_date`,`admin_id`,`last_modified_date`,`status_active`) values (16,'Windy Hendra Supardi','Jalan Magelang','089693230603','',12000,'2016-01-20 12:24:58',NULL,NULL,1,'2016-01-20 00:24:58','active'),(17,'Windy Hendra Supardiiiii','Jalan Jalan','085252624451','',1231312,'2016-01-20 12:25:15',NULL,NULL,1,'2016-01-20 00:25:42','active'),(18,'Windy Hendra','ajlasdnslafka','090124021231','',1231254123,'2016-01-20 12:25:26',NULL,NULL,1,'2016-01-20 00:25:26','inactive'),(19,'windy','jalan','0123','',2132,'2016-01-20 01:00:12',NULL,'0000-00-00 00:00:00',1,'2016-01-20 01:00:12','active'),(20,'Edo','safasfasd','123123','',121231,'2016-01-20 01:01:12','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'2016-01-20 01:01:12','active'),(21,'Yanuar','afasfasd','12313','',123,'2016-01-20 01:03:19','2016-01-01 12:00:00','2016-01-31 12:00:00',1,'2016-01-20 01:03:19','active'),(22,'Windy Hendra','Jalan jalan','09131293','',12312412,'2016-01-20 01:18:57','2016-01-01 12:00:00','1970-01-01 01:00:00',1,'2016-01-20 01:18:57','active'),(23,'Bonggol','asfsagsdgsf','12312412','',123123,'2016-01-20 01:20:42','2016-01-01 12:00:00',NULL,1,'2016-01-20 01:20:42','active'),(24,'zxczxc','zxczxc','125123',NULL,124123,'2016-01-22 12:56:29','2016-01-21 12:00:00',NULL,1,'2016-01-22 00:56:29','active'),(25,'sdfgdfg','fdgsdfg','12421',NULL,12512412,'2016-01-22 12:56:42','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'2016-01-22 00:57:54','active'),(26,'xcvb','xvcbxc','1251243',NULL,125123,'2016-01-22 12:56:51','2016-01-21 12:00:00',NULL,1,'2016-01-22 00:56:51','inactive'),(27,'ngbnbv','gnghnhg','51261212',NULL,151253,'2016-01-22 12:57:09','2016-01-08 12:00:00','0000-00-00 00:00:00',1,'2016-01-22 00:57:09','inactive'),(28,'xcvb','12312','124123',NULL,12312312,'2016-01-22 01:00:25','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'2016-01-22 01:01:38','active'),(29,'xcvb','124123','124123',NULL,124123,'2016-01-22 01:00:41','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'2016-01-22 01:01:03','active');

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cash_nominal` int(11) DEFAULT NULL,
  `transfer_nominal` int(11) DEFAULT NULL,
  `transfer_bank_name` varchar(50) DEFAULT NULL,
  `transfer_bank_account` varchar(50) DEFAULT NULL,
  `transfer_name` varchar(50) DEFAULT NULL,
  `debit_bank_name` varchar(50) DEFAULT NULL,
  `debit_bank_account` varchar(50) DEFAULT NULL,
  `debit_nominal` int(11) DEFAULT NULL,
  `total_nominal` int(11) DEFAULT NULL,
  `payment_purpose` varchar(50) DEFAULT NULL COMMENT 'dp/paid_off/receivable',
  `payment_for` varchar(50) DEFAULT NULL COMMENT 'sales/pos/dll.',
  `payment_journal_type` varchar(50) DEFAULT NULL COMMENT 'income/outcome',
  `payment_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `admin_id` int(10) DEFAULT NULL,
  `ref_number` varchar(50) DEFAULT NULL COMMENT 'menyimpan refnumber, misal punya sale,purchase dll.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf16;

/*Data for the table `payments` */

insert  into `payments`(`id`,`cash_nominal`,`transfer_nominal`,`transfer_bank_name`,`transfer_bank_account`,`transfer_name`,`debit_bank_name`,`debit_bank_account`,`debit_nominal`,`total_nominal`,`payment_purpose`,`payment_for`,`payment_journal_type`,`payment_date`,`created_date`,`last_modified_date`,`admin_id`,`ref_number`) values (8,50000,NULL,'','','',NULL,NULL,NULL,50000,'paid_off','sales','income','2016-01-21 00:00:00','2016-01-21 23:38:32','2016-01-21 23:38:32',1,'20160121-S00016'),(9,200000,NULL,'','','',NULL,NULL,NULL,200000,'dp','sales','income','2016-01-21 00:00:00','2016-01-21 23:39:16','2016-01-21 23:39:16',1,'20160121-S00017'),(10,50000,50000,'BNI','9090-9090','Yustinus',NULL,NULL,NULL,100000,'paid_off','sales','income','2016-01-21 00:00:00','2016-01-21 23:40:08','2016-01-21 23:40:08',1,'20160121-S00017'),(11,NULL,NULL,'','','',NULL,NULL,NULL,0,'paid_off',NULL,NULL,'0000-00-00 00:00:00','2016-01-21 23:50:16','2016-01-21 23:50:16',NULL,NULL),(12,50000,NULL,'','','',NULL,NULL,NULL,50000,'dp','sales','income','2016-01-21 00:00:00','2016-01-21 23:51:42','2016-01-21 23:52:21',1,'20160121-S00019'),(13,NULL,NULL,'','','',NULL,NULL,NULL,0,'paid_off',NULL,NULL,'0000-00-00 00:00:00','2016-01-21 23:53:41','2016-01-21 23:53:41',NULL,NULL),(14,50000,NULL,'','','',NULL,NULL,NULL,50000,'dp','sales','income','2016-01-21 00:00:00','2016-01-21 23:54:10','2016-01-21 23:54:10',1,'20160121-S00021'),(15,30000,NULL,'','','',NULL,NULL,NULL,30000,'dp','sales','income','2016-01-22 00:00:00','2016-01-22 00:03:35','2016-01-22 00:03:35',1,'20160122-S00001'),(16,30000,NULL,'','','',NULL,NULL,NULL,30000,'paid_off','sales','income','2016-01-22 00:00:00','2016-01-22 06:06:34','2016-01-22 06:06:34',1,'20160122-S00001'),(17,10000,NULL,'','','',NULL,NULL,NULL,10000,'paid_off','sales','income','2016-01-22 00:00:00','2016-01-22 06:12:22','2016-01-22 06:12:22',1,'20160121-S00021'),(19,50000,NULL,'','','','',NULL,NULL,50000,'paid_off','sales','income','2016-01-22 00:00:00','2016-01-22 18:42:12','2016-01-22 18:42:12',1,'20160122-S00003');

/*Table structure for table `privileges` */

DROP TABLE IF EXISTS `privileges`;

CREATE TABLE `privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `code` int(10) unsigned NOT NULL,
  `module` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `privileges` */

insert  into `privileges`(`id`,`name`,`parent_id`,`code`,`module`) values (1,'dashboard',0,0,'admin'),(2,'show',1,1,'admin'),(3,'users',0,0,'admin'),(4,'Show',3,1,'admin'),(5,'Add',3,2,'admin'),(6,'Edit',3,4,'admin'),(7,'Delete',3,8,'admin'),(8,'privileges',0,0,'admin'),(9,'Show',8,1,'admin'),(10,'Add',8,2,'admin'),(11,'Edit',8,4,'admin'),(12,'Delete',8,8,'admin'),(13,'products',0,0,'admin'),(14,'Show',13,1,'admin'),(15,'Add',13,2,'admin'),(16,'Edit',13,4,'admin'),(17,'Delete',13,8,'admin'),(18,'sales',0,0,'admin'),(22,'show',18,1,'admin'),(23,'add',18,2,'admin'),(24,'edit',18,4,'admin'),(25,'delete',18,8,'admin');

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  `admin_id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` text,
  `price` double NOT NULL DEFAULT '0',
  `status_active` varchar(20) DEFAULT 'active' COMMENT 'active, inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `products` */

insert  into `products`(`id`,`created_date`,`last_modified`,`admin_id`,`category`,`name`,`desc`,`price`,`status_active`) values (2,'2016-01-06 10:12:35','2016-01-06 10:12:35',0,'Fotokopi','test',NULL,123213,'active'),(3,'2016-01-06 10:40:40','2016-01-06 10:40:40',1,'Fotokopi','Hehe2','',123213,'active'),(4,'2016-01-06 10:41:08','2016-01-06 10:41:08',1,'Fotokopi','Coba1','',123213,'active');

/*Table structure for table `sale_counters` */

DROP TABLE IF EXISTS `sale_counters`;

CREATE TABLE `sale_counters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `day` varchar(2) DEFAULT NULL,
  `month` varchar(2) DEFAULT NULL,
  `year` varchar(4) DEFAULT NULL,
  `counter` int(11) DEFAULT NULL,
  `last_deleted` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `sale_counters` */

insert  into `sale_counters`(`id`,`day`,`month`,`year`,`counter`,`last_deleted`) values (1,'21','01','2016',21,NULL),(2,'22','01','2016',3,NULL);

/*Table structure for table `sale_items` */

DROP TABLE IF EXISTS `sale_items`;

CREATE TABLE `sale_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `sale_items` */

insert  into `sale_items`(`id`,`product_id`,`sale_id`,`qty`,`unit_price`,`total_price`,`admin_id`,`created_date`,`last_modified_date`,`employee_id`,`division_id`) values (13,2,10,300,1000,300000,1,'2016-01-21 23:39:16','2016-01-21 23:39:16',1,1),(14,3,11,1,10000,10000,1,'2016-01-21 23:50:16','2016-01-21 23:50:16',1,1),(15,4,12,4,20000,80000,1,'2016-01-21 23:51:42','2016-01-21 23:51:42',1,2),(16,2,13,1,100000,100000,1,'2016-01-21 23:53:41','2016-01-21 23:53:41',1,1),(17,3,14,1,100000,100000,1,'2016-01-21 23:54:10','2016-01-21 23:54:10',1,2),(18,3,15,5,9000,45000,1,'2016-01-22 00:03:35','2016-01-22 00:03:35',1,1),(20,2,17,5,10000,50000,1,'2016-01-22 18:42:12','2016-01-22 18:42:12',16,3);

/*Table structure for table `sales` */

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ref_number` varchar(50) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `total_nominal` int(11) DEFAULT NULL,
  `status_sales` varchar(50) DEFAULT 'unpaid' COMMENT 'paid/unpaid',
  `status_payment` varchar(50) DEFAULT NULL COMMENT 'dp/paid_off',
  `sales_type` varchar(50) DEFAULT NULL COMMENT 'order/onsite',
  `created_date` datetime DEFAULT NULL,
  `admin_id` int(10) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `status_active` varchar(50) DEFAULT 'active' COMMENT 'active/inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `sales` */

insert  into `sales`(`id`,`ref_number`,`customer_id`,`total_nominal`,`status_sales`,`status_payment`,`sales_type`,`created_date`,`admin_id`,`last_modified_date`,`status_active`) values (9,'20160121-S00016',1,90000,'paid','paid_off','onsite','2016-01-21 23:38:32',1,'2016-01-21 23:38:47','inactive'),(10,'20160121-S00017',1,300000,'paid','paid_off','onsite','2016-01-21 23:39:15',1,'2016-01-21 23:40:08','active'),(11,'20160121-S00018',1,10000,'unpaid','paid_off','order','2016-01-21 23:50:16',1,'2016-01-21 23:50:16','active'),(12,'20160121-S00019',1,80000,'paid','dp','order','2016-01-21 23:51:42',1,'2016-01-21 23:51:42','active'),(13,'20160121-S00020',1,100000,'unpaid','paid_off','onsite','2016-01-21 23:53:41',1,'2016-01-21 23:53:41','active'),(14,'20160121-S00021',1,100000,'paid','paid_off','order','2016-01-21 23:54:10',1,'2016-01-22 06:12:22','active'),(15,'20160122-S00001',1,45000,'paid','paid_off','onsite','2016-01-22 00:03:35',1,'2016-01-22 06:06:34','active'),(17,'20160122-S00003',15,50000,'paid','paid_off','onsite','2016-01-22 18:42:12',1,'2016-01-22 18:42:12','active');

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `test` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `test` */

insert  into `test`(`id`,`created_date`,`test`) values (1,'0000-00-00 00:00:00','LINA LONTHE'),(2,'0000-00-00 00:00:00','LINA LONTHE');

/*Table structure for table `transaction_session_histories` */

DROP TABLE IF EXISTS `transaction_session_histories`;

CREATE TABLE `transaction_session_histories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'income' COMMENT 'income/outcome',
  `total` int(11) NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT 'cash' COMMENT 'cash, debet, transfer',
  `sale_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `transaction_session_histories` */

insert  into `transaction_session_histories`(`id`,`admin_id`,`created_date`,`status`,`total`,`type`,`sale_id`,`session_id`,`purchase_id`,`last_modified_date`) values (1,1,'2016-01-22 18:42:12','income',50000,'cash',17,1,NULL,'2016-01-22 18:42:12');

/*Table structure for table `transaction_sessions` */

DROP TABLE IF EXISTS `transaction_sessions`;

CREATE TABLE `transaction_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `init_balance` int(11) NOT NULL,
  `closed_balance` int(11) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `desc` text,
  `next_admin_id` int(11) DEFAULT NULL,
  `cr_balance` int(11) DEFAULT '0',
  `status` varchar(50) DEFAULT 'clear' COMMENT 'clear, unclear',
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `transaction_sessions` */

insert  into `transaction_sessions`(`id`,`admin_id`,`created_date`,`init_balance`,`closed_balance`,`start_date`,`end_date`,`desc`,`next_admin_id`,`cr_balance`,`status`,`last_modified_date`) values (1,1,'2016-01-22 15:36:24',1000000,50000,'2016-01-22 15:36:23','2016-01-22 18:43:27',NULL,1,50000,'clear','2016-01-22 18:43:27');

/* Trigger structure for table `test` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `lina_asu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `lina_asu` AFTER INSERT ON `test` FOR EACH ROW begin
  insert into `fotosarubanget`.`test` (test) values (new.test);
end */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
