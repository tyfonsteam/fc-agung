/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.9-MariaDB : Database - fotocopy
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fotocopy` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `fotocopy`;

/*Table structure for table `privileges` */

DROP TABLE IF EXISTS `privileges`;

CREATE TABLE `privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `code` int(10) unsigned NOT NULL,
  `module` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

/*Data for the table `privileges` */

insert  into `privileges`(`id`,`name`,`parent_id`,`code`,`module`) values (1,'dashboard',0,0,'admin'),(2,'show',1,1,'admin'),(3,'users',0,0,'admin'),(4,'Show',3,1,'admin'),(5,'Add',3,2,'admin'),(6,'Edit',3,4,'admin'),(7,'Delete',3,8,'admin'),(8,'privileges',0,0,'admin'),(9,'Show',8,1,'admin'),(10,'Add',8,2,'admin'),(11,'Edit',8,4,'admin'),(12,'Delete',8,8,'admin'),(13,'products',0,0,'admin'),(14,'Show',13,1,'admin'),(15,'Add',13,2,'admin'),(16,'Edit',13,4,'admin'),(17,'Delete',13,8,'admin'),(18,'sales',0,0,'admin'),(22,'show',18,1,'admin'),(23,'add',18,2,'admin'),(24,'edit',18,4,'admin'),(25,'delete',18,8,'admin'),(26,'customers',0,0,'admin'),(27,'show',26,1,'admin'),(28,'add',26,2,'admin'),(29,'edit',26,4,'admin'),(30,'delete',26,8,'admin'),(31,'detail',26,16,'admin'),(32,'employees',0,0,'admin'),(33,'show',32,1,'admin'),(34,'add',32,2,'admin'),(35,'edit',32,4,'admin'),(36,'delete',32,8,'admin'),(37,'detail',32,16,'admin'),(38,'divisions',0,0,'admin'),(39,'show',38,1,'admin'),(40,'add',38,2,'admin'),(41,'edit',38,4,'admin'),(42,'delete',38,8,'admin'),(43,'detail',38,16,'admin'),(44,'reports',0,0,'admin'),(45,'show',44,1,'admin'),(46,'add',44,2,'admin'),(47,'edit',44,4,'admin'),(49,'detail',44,16,'admin'),(50,'delete',44,8,'admin');

/* Trigger structure for table `test` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `lina_asu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `lina_asu` AFTER INSERT ON `test` FOR EACH ROW begin
  insert into `fotosarubanget`.`test` (test) values (new.test);
end */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
