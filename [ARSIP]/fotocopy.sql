/*
SQLyog Ultimate v11.33 (32 bit)
MySQL - 5.6.25 : Database - fotocopy
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fotocopy` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `fotocopy`;

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `privilege` text NOT NULL,
  `status` varchar(20) DEFAULT 'active' COMMENT 'active, inactive',
  `photo` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `admins` */

insert  into `admins`(`id`,`created_date`,`username`,`password`,`name`,`address`,`phone`,`email`,`privilege`,`status`,`photo`) values (1,'2015-09-21 18:39:34','admin','$2a$10$/lpimIkKfy/zSFw0VI0ECe8heglJaem5UoUcwEKJZfH1Hwf/uwJYW','Admin','-','-','tyfons.id@gmail.com','{\"admin\":{\"dashboard\":[\"1\"],\"users\":[\"1\",\"2\",\"4\",\"8\"],\"privileges\":[\"1\",\"2\",\"4\",\"8\"],\"products\":[\"1\",\"2\",\"4\",\"8\"]}}','active',NULL),(2,'2016-01-05 10:42:37','test','$2a$10$qqmMAiFbigcuBBNlM6BX8OEP12anGm9dS7n2z/Ie.wp/JxW11fNo.','test','asdf','123123','','{\"admin\":{\"dashboard\":[\"1\"],\"users\":[\"1\",\"2\",\"4\",\"8\"],\"privileges\":[\"1\",\"2\",\"4\",\"8\"]}}','inactive',NULL);

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `admin_id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `categories` */

insert  into `categories`(`id`,`created_date`,`admin_id`,`category`) values (1,'2016-01-06 15:48:06',1,'Fotokopi'),(2,'2016-01-06 15:48:52',1,'Cp BUF Kert ');

/*Table structure for table `pos_session_transactions` */

DROP TABLE IF EXISTS `pos_session_transactions`;

CREATE TABLE `pos_session_transactions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'debet' COMMENT 'debet, kredit',
  `total` int(11) NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT 'cash' COMMENT 'cash, debet',
  `sales_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pos_session_transactions` */

/*Table structure for table `pos_sessions` */

DROP TABLE IF EXISTS `pos_sessions`;

CREATE TABLE `pos_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `init_balance` int(11) NOT NULL,
  `closed_balance` int(11) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `desc` text,
  `next_admin_id` int(11) DEFAULT NULL,
  `cr_balance` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT 'clear' COMMENT 'clear, unclear',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pos_sessions` */

/*Table structure for table `privileges` */

DROP TABLE IF EXISTS `privileges`;

CREATE TABLE `privileges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `code` int(10) unsigned NOT NULL,
  `module` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `privileges` */

insert  into `privileges`(`id`,`name`,`parent_id`,`code`,`module`) values (1,'dashboard',0,0,'admin'),(2,'show',1,1,'admin'),(3,'users',0,0,'admin'),(4,'Show',3,1,'admin'),(5,'Add',3,2,'admin'),(6,'Edit',3,4,'admin'),(7,'Delete',3,8,'admin'),(8,'privileges',0,0,'admin'),(9,'Show',8,1,'admin'),(10,'Add',8,2,'admin'),(11,'Edit',8,4,'admin'),(12,'Delete',8,8,'admin'),(13,'products',0,0,'admin'),(14,'Show',13,1,'admin'),(15,'Add',13,2,'admin'),(16,'Edit',13,4,'admin'),(17,'Delete',13,8,'admin');

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  `admin_id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` text,
  `price` double NOT NULL DEFAULT '0',
  `status_active` varchar(20) DEFAULT 'active' COMMENT 'active, inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `products` */

insert  into `products`(`id`,`created_date`,`last_modified`,`admin_id`,`category`,`name`,`desc`,`price`,`status_active`) values (2,'2016-01-06 10:12:35','2016-01-06 10:12:35',0,'Fotokopi','test',NULL,123213,'inactive'),(3,'2016-01-06 10:40:40','2016-01-06 10:40:40',1,'Fotokopi','Hehe2','',123213,'inactive'),(4,'2016-01-06 10:41:08','2016-01-06 10:41:08',1,'Fotokopi','Coba1','',123213,'inactive');

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `test` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `test` */

insert  into `test`(`id`,`created_date`,`test`) values (1,'0000-00-00 00:00:00','LINA LONTHE'),(2,'0000-00-00 00:00:00','LINA LONTHE');

/* Trigger structure for table `test` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `lina_asu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `lina_asu` AFTER INSERT ON `test` FOR EACH ROW begin
  insert into `fotosarubanget`.`test` (test) values (new.test);
end */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
