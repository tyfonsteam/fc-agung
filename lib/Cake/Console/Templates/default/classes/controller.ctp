<?php
/**
 * Controller bake template file
 *
 * Allows templating of Controllers generated from bake.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.classes
 * @since         CakePHP(tm) v 1.3
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

echo "<?php\n";
echo "App::uses('{$plugin}AppController', '{$pluginPath}Controller');\n";
?>
/**
 * <?php echo $controllerName; ?> Controller
 *
<?php
if (!$isScaffold) {
	$defaultModel = Inflector::singularize($controllerName);
	echo " * @property {$defaultModel} \${$defaultModel}\n";
	if (!empty($components)) {
		foreach ($components as $component) {
			echo " * @property {$component}Component \${$component}\n";
		}
	}
}
?>
 */
class <?php echo $controllerName; ?>Controller extends <?php echo $plugin; ?>AppController {

<?php if ($isScaffold): $modelName = Inflector::singularize($controllerName);?>
	public $uses = array('<?php echo $plugin; ?>.<?php echo Inflector::singularize($controllerName)?>');

	public function isAuthorized($user){
		/* sesuaikan privilege */
		$this->parent = 'product_settings';
		$this->module = '<?php echo strtolower($plugin)?>';
		return true;
	}

	public function index(){
		if(!$this->checkPrivilege(1)) $this->notAuthorized();
		$this->breadcrumbs = array(
			array('Dashboard', '/<?php echo strtolower($plugin)?>', 'fa-dashboard'),
			array('List', '', 'fa-list')
		);
		$data = $this->request->query;	
		$query = array();
		if(!empty($data)){
			if($data['query'] != ""){
				$query['OR']['<?php echo $modelName ?>.name LIKE'] = '%'.$data['query'].'%';
			}
		}else{
			$data = null;
		}
		$this->set('searchData', $data);

		$this->Paginator->settings = array(
			'limit' => 20,
			'conditions' => $query,
		);

		$this->set('<?php echo strtolower($controllerName)?>', $this->Paginator->paginate('<?php echo $modelName ?>'));
	}

	public function add(){
		if($this->request->is('post')){
			$data = $this->request->data;
			if(!$this-><?php echo $modelName;?>->save($this->request->data)){
				$this->renderAdd($data, $this-><?php echo $modelName;?>->validationErrors);
				return;
			}
			$this->setFlash('<?php echo $modelName?> has been added.', 'success', '/<?php echo strtolower($plugin);?>/<?php echo strtolower($controllerName)?>/add');
		}
		$this->renderAdd();
	}
	private function renderAdd($data = array(), $validationErrors = array()){	
		$this->breadcrumbs = array(
			array('Dashboard', '/<?php echo strtolower($plugin)?>', 'fa-dashboard'),
			array('List', '/<?php echo strtolower($plugin);?>/<?php echo strtolower($controllerName)?>', 'fa-list'),
			array('Add', '', 'fa-plus-circle')
		);

		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function edit(){
		$id = $this->params['id'];
		if($id == null) $this->invalidRequest();

		$<?php echo strtolower($controllerName)?> = $this-><?php echo $modelName;?>->findById($id);
		if(empty($<?php echo strtolower($controllerName)?>)) $this->invalidRequest();
		if($this->request->is('post')){
			$data = $this->request->data;
			if(!$this-><?php echo $modelName;?>->save($this->request->data)){
				$this->renderAdd($data, $this-><?php echo $modelName;?>->validationErrors);
				return;
			}
			$this->setFlash('<?php echo $modelName?> has been updated.', 'success', '/<?php echo strtolower($plugin);?>/<?php echo strtolower($controllerName)?>/edit/'.$<?php echo strtolower($controllerName)?>['<?php echo $modelName;?>']['id']);
		}
		$this->renderAdd();
	}
	private function renderEdit($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/<?php echo strtolower($plugin)?>', 'fa-dashboard'),
			array('List', '/<?php echo strtolower($plugin);?>/<?php echo strtolower($controllerName)?>', 'fa-list'),
			array('Edit', '', 'fa-pencil')
		);

		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function delete($id = null){
		if($id == null) $this->invalidRequest();
		$<?php echo strtolower($controllerName)?> = $this-><?php echo $modelName;?>->findById($id);
		if(empty($<?php echo strtolower($controllerName)?>)) $this->invalidRequest();

		if(!$this-><?php echo $modelName;?>->delete($id)){
			$this->setFlash('Terjadi kesalahan sistem.', 'danger', '/<?php echo strtolower($plugin)?>/<?php echo strtolower($controllerName)?>');
			return;
		}
		$this->setFlash('<?php echo $modelName?> has been deleted.', 'warning', '/<?php echo strtolower($plugin)?>/<?php echo strtolower($controllerName)?>');
	}
<?php else:

	if (count($helpers)):
		echo "/**\n * Helpers\n *\n * @var array\n */\n";
		echo "\tpublic \$helpers = array(";
		for ($i = 0, $len = count($helpers); $i < $len; $i++):
			if ($i != $len - 1):
				echo "'" . Inflector::camelize($helpers[$i]) . "', ";
			else:
				echo "'" . Inflector::camelize($helpers[$i]) . "'";
			endif;
		endfor;
		echo ");\n\n";
	endif;

	if (count($components)):
		echo "/**\n * Components\n *\n * @var array\n */\n";
		echo "\tpublic \$components = array(";
		for ($i = 0, $len = count($components); $i < $len; $i++):
			if ($i != $len - 1):
				echo "'" . Inflector::camelize($components[$i]) . "', ";
			else:
				echo "'" . Inflector::camelize($components[$i]) . "'";
			endif;
		endfor;
		echo ");\n\n";
	endif;

	if (!empty($actions)) {
		echo trim($actions) . "\n";
	}

endif; ?>
}
