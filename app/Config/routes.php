<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	
	/* Privileges CRUD */
	Router::connect('/admin/privileges', array('plugin' => 'admin', 'controller' => 'Privileges', 'action'=>'index'));
	Router::connect('/admin/privileges/lists/:module', array('plugin' => 'admin', 'controller' => 'Privileges', 'action'=>'lists'));
	Router::connect('/admin/privileges/add/:parent/:module/:parent_id', array('plugin' => 'admin', 'controller' => 'Privileges', 'action'=>'add'));
	Router::connect('/admin/privileges/add/module', array('plugin' => 'admin', 'controller' => 'Privileges', 'action'=>'addModule'));
	Router::connect('/admin/privileges/add/:module', array('plugin' => 'admin', 'controller' => 'Privileges', 'action'=>'addParent'));
	Router::connect('/admin/privileges/edit/:id', array('plugin' => 'admin', 'controller' => 'Privileges', 'action'=>'edit'), array('id'=>'[0-9]+'));
	/* End Privileges CRUD */

	Router::connect('/', array('plugin'=>'admin', 'controller' => 'Dashboards', 'action' => 'index'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	
	Router::connect('/admin', array('plugin'=>'admin', 'controller'=>'Dashboards', 'action'=>'index'));
	Router::connect('/admin/dashboard', array('plugin'=>'admin', 'controller'=>'Dashboards', 'action'=>'index'));

	Router::connect('/admin/users/profile', array('plugin'=>'admin', 'controller'=>'Users', 'action'=>'profile'));	
	Router::connect('/admin/users', array('plugin'=>'admin', 'controller'=>'Users', 'action'=>'index'));
	Router::connect('/admin/users/edit/:id', array('plugin'=>'admin', 'controller'=>'Users', 'action' => 'edit'));
	Router::connect('/admin/users/add', array('plugin'=>'admin','controller'=>'Users', 'action'=>'add'));

	/* Products */
	Router::connect('/admin/products', array('plugin' => 'admin', 'controller'=>'Products', 'action'=>'index'));
	Router::connect('/admin/products/add', array('plugin' => 'admin', 'controller'=>'Products', 'action'=>'add'));
	Router::connect('/admin/products/edit/:id', array('plugin' => 'admin', 'controller'=>'Products', 'action'=>'edit'));
	/* End Products */

	/* Sales */
	Router::connect('/admin/sales', array('plugin' => 'admin', 'controller'=>'Sales', 'action'=>'index'));
	Router::connect('/admin/sales/add', array('plugin' => 'admin', 'controller'=>'Sales', 'action'=>'add'));
	Router::connect('/admin/sales/edit/:id', array('plugin' => 'admin', 'controller'=>'Sales', 'action'=>'edit'));
	Router::connect('/admin/sales/detail/:id', array('plugin' => 'admin', 'controller'=>'Sales', 'action'=>'detail'));
	Router::connect('/admin/sales/delete/:id', array('plugin' => 'admin', 'controller'=>'Sales', 'action'=>'delete'));
	Router::connect('/admin/sales/payoff/:id', array('plugin' => 'admin', 'controller'=>'Sales', 'action'=>'payoff'));
	/* End Sales */

	/* Customers */
	Router::connect('/admin/customers', array('plugin' => 'admin', 'controller'=>'Customers', 'action'=>'index'));
	Router::connect('/admin/customers/add', array('plugin' => 'admin', 'controller'=>'Customers', 'action'=>'add'));
	Router::connect('/admin/customers/edit/:id', array('plugin' => 'admin', 'controller'=>'Customers', 'action'=>'edit'));
	Router::connect('/admin/customers/detail/:id', array('plugin' => 'admin', 'controller'=>'Customers', 'action'=>'detail'));
	Router::connect('/admin/customers/delete/:id', array('plugin' => 'admin', 'controller'=>'Customers', 'action'=>'delete'));
	/* End Customers */

	/* Divisions */
	Router::connect('/admin/purchases', array('plugin' => 'admin', 'controller'=>'Purchases', 'action'=>'index'));
	Router::connect('/admin/purchases/add', array('plugin' => 'admin', 'controller'=>'Purchases', 'action'=>'add'));
	Router::connect('/admin/purchases/edit/:id', array('plugin' => 'admin', 'controller'=>'Purchases', 'action'=>'edit'));
	Router::connect('/admin/purchases/detail/:id', array('plugin' => 'admin', 'controller'=>'Purchases', 'action'=>'detail'));
	Router::connect('/admin/purchases/delete/:id', array('plugin' => 'admin', 'controller'=>'Purchases', 'action'=>'delete'));
	/* End Divisions */

	/* Employees */
	Router::connect('/admin/employees', array('plugin' => 'admin', 'controller'=>'Employees', 'action'=>'index'));
	Router::connect('/admin/employees/add', array('plugin' => 'admin', 'controller'=>'Employees', 'action'=>'add'));
	Router::connect('/admin/employees/edit/:id', array('plugin' => 'admin', 'controller'=>'Employees', 'action'=>'edit'));
	Router::connect('/admin/employees/detail/:id', array('plugin' => 'admin', 'controller'=>'Employees', 'action'=>'detail'));
	Router::connect('/admin/employees/delete/:id', array('plugin' => 'admin', 'controller'=>'Employees', 'action'=>'delete'));
	/* End Employees */

	/* Divisions */
	Router::connect('/admin/divisions', array('plugin' => 'admin', 'controller'=>'Divisions', 'action'=>'index'));
	Router::connect('/admin/divisions/add', array('plugin' => 'admin', 'controller'=>'Divisions', 'action'=>'add'));
	Router::connect('/admin/divisions/edit/:id', array('plugin' => 'admin', 'controller'=>'Divisions', 'action'=>'edit'));
	Router::connect('/admin/divisions/detail/:id', array('plugin' => 'admin', 'controller'=>'Divisions', 'action'=>'detail'));
	Router::connect('/admin/divisions/delete/:id', array('plugin' => 'admin', 'controller'=>'Divisions', 'action'=>'delete'));
	/* End Divisions */

	/* Reports */
	Router::connect('/admin/reports/customers', array('plugin' => 'admin', 'controller'=>'Reports', 'action'=>'reports_by_customer'));
	Router::connect('/admin/reports/customers/detail/:id', array('plugin' => 'admin', 'controller'=>'Reports', 'action'=>'reports_by_customer_detail'));
	/* End Reports */

	/* TransactionSessions */
	
	Router::connect('/admin/sessions/open', array('plugin' => 'admin', 'controller'=>'TransactionSessions', 'action'=>'open'));
	Router::connect('/admin/sessions/close', array('plugin' => 'admin', 'controller'=>'TransactionSessions', 'action'=>'close'));
	
	/* End TransactionSessions */

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
