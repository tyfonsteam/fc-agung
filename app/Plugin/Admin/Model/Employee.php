<?php
App::uses('AdminAppModel', 'Admin.Model');
/**
 * Product Model
 *
 * @property Admin $Admin
 */
class Employee extends AdminAppModel {

	public function beforeSave($options = array()) {
		$now = new DateTime();
		if(!isset($this->data[$this->alias]['created_date'])){
            $this->data[$this->alias]['created_date'] = $now->format("Y-m-d H:i:s");  
        }
		$this->data[$this->alias]['last_modified_date'] = $now->format("Y-m-d H:i:s");
		// $this->data[$this->alias]['join_date'] = $now->format("Y-m-d H:i:s");
		// $this->data[$this->alias]['resign_date'] = $now->format("Y-m-d H:i:s");
        
        return true;
    }
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'created_date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		// 'join_date' => array(
		// 	'datetime' => array(
		// 		'rule' => array('datetime'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		// 'resign_date' => array(
		// 	'datetime' => array(
		// 		'rule' => array('datetime'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		'admin_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Admin' => array(
			'className' => 'Admin.Admin',
			'foreignKey' => 'admin_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
