<?php
App::uses('AdminAppModel', 'Admin.Model');
/**
 * Product Model
 *
 * @property Admin $Admin
 */
class SaleItem extends AdminAppModel {

	public function beforeSave($options = array()) {
		$now = new DateTime();
        if(!isset($this->data[$this->alias]['created_date'])){
            $this->data[$this->alias]['created_date'] = $now->format("Y-m-d H:i:s");  
        }
        $this->data[$this->alias]['last_modified_date'] = $now->format('Y-m-d H:i:s');
        return true;
    }
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'created_date' => array(
			'datetime' => array(
				'rule' => array('datetime')
			)
		),
		'last_modified_date' => array(
			'datetime' => array(
				'rule' => array('datetime')
			)
		),
		'admin_id' => array(
			'numeric' => array(
				'rule' => array('numeric')
			)
		),
		'product_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank')
			),
			'numeric' => array(
				'rule' => array('numeric')
			)
		),
		'unit_price' => array(
			'notBlank' => array(
				'rule' => array('notBlank')
			),
			'numeric' => array(
				'rule' => array('numeric')
			)
		),
		'qty' => array(
			'notBlank' => array(
				'rule' => array('notBlank')
			),
			'numeric' => array(
				'rule' => array('numeric')
			)
		),
		'total_price' => array(
			'numeric' => array(
				'rule' => array('numeric')
			),
			'notBlank' => array(
				'rule' => array('notBlank')
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Admin' => array(
			'className' => 'Admin.Admin',
			'foreignKey' => 'admin_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Product' => array(
			'className' => 'Admin.Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
