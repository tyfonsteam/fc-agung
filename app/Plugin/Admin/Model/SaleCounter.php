<?php
App::uses('AdminAppModel', 'Admin.Model');
class SaleCounter extends AppModel{
	public $useTable = 'sale_counters';
	public function beforeSave($options = array()){
		
	}

	public function getRefNumber(){
		$now = new DateTime();
		$year = $now->format('Y');
		$month = $now->format('m');
		$day = $now->format('d');
		$counter = $this->findByYearAndMonthAndDay($year,$month,$day);
		if(empty($counter)){
			$this->create();
			$counterData = array(
				'year' => $year, 
				'month' => $month, 
				'day' => $day
				);
			$this->save($counterData);

			$refNumber = $year.$month.$day ."-". "S".str_pad(1, 5, 0, STR_PAD_LEFT);
			return $refNumber;
		}
		$counter['SaleCounter']['counter']+=1;
		$this->save($counter);
		$refNumber = $counter['SaleCounter']['year'].$counter['SaleCounter']['month'].$counter['SaleCounter']['day'] . "-" . "S".str_pad($counter['SaleCounter']['counter'], 5, 0, STR_PAD_LEFT);
		return $refNumber;
	}
	
}