<?php
App::uses('AdminAppModel', 'Admin.Model');
/**
 * Product Model
 *
 * @property Admin $Admin
 */
class Customer extends AdminAppModel {

	public function beforeSave($options = array()) {
		$now = new DateTime();
        if(!isset($this->data[$this->alias]['created_date'])){
            $this->data[$this->alias]['created_date'] = $now->format("Y-m-d H:i:s");  
        }
        $this->data[$this->alias]['last_modified_date'] = $now->format('Y-m-d H:i:s');
        return true;
    }
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'created_date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'last_modified' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'admin_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Admin' => array(
			'className' => 'Admin.Admin',
			'foreignKey' => 'admin_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	 /* Customer Report */
    /**
    * Query untuk mendapatkan pembelian produk berdasarkan tipe
    */
    public function get_sales_by_product_category($id, $from, $to){
    	$db = $this->getDataSource();
    	return $db->fetchAll(
		    'SELECT COUNT(pr.category) AS tc, pr.category AS category
			FROM
				sales AS s INNER JOIN
				sale_items AS sp ON s.`id` = sp.`sale_id` INNER JOIN
				products AS pr ON pr.`id` = sp.`product_id`
			WHERE
				s.`customer_id` = :customer_id AND
				s.`created_date` BETWEEN :from AND :to
			GROUP BY
				pr.category',
		    array('customer_id' => $id, 'from' => $from, 'to' => $to)
		);

    }
    /* END */
}
