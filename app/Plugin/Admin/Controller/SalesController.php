<?php
App::uses('PosAppController', 'Admin.Controller');
/**
 * PosDashboards Controller
 *
 */
class SalesController extends AdminAppController {

	public $uses = array('Admin.TransactionSessionHistory','Admin.TransactionSession','Admin.Division','Admin.Employee','Admin.Customer','Admin.Sale','Admin.Product','Admin.Payment','Admin.SaleCounter','Admin.SaleItem');

	public function isAuthorized($user){
		$this->parent = 'sales';
		$this->module = 'admin';

		return true;
	}

	public function index(){
		//check if there is customer birthday 
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('Penjualan', '', 'fa-shopping-cart')
		);	

		$data = $this->request->query;	
		$query = array();
		if(!empty($data)){
			if($data['query'] != ""){
				$query['Sale.ref_number LIKE'] = '%'.$data['query'].'%';
			}
		}else{
			$data = null;
		}
		$this->set('searchData', $data);
		$this->Paginator->settings = array(
			'limit' => 5,
			'conditions' => array($query,'AND'=>array('Sale.status_active'=>'active')),
			'order' => array(
				'Sale.created_date' => 'desc'
				)
		);

		$this->set('data', $this->Paginator->paginate('Sale'));
	}

	public function add(){
		//check if there is customer birthday 
		if ($this->request->is('post')) {
			$data = $this->request->data;

			$admin_id = $this->Auth->user('id');
			$data['Sale']['ref_number'] = $this->SaleCounter->getRefNumber();
			$data['Sale']['id'] = $data['Sale']['ref_number'];
			if(isset($data['Payment']['payment_purpose']) && !empty($data['Payment']['payment_purpose'])){
				$data['Sale']['status_payment'] = $data['Payment']['payment_purpose'];
			}
			$data['Sale']['admin_id'] = $admin_id;
			$ds = $this->Sale->getDataSource();
			$ds->begin();
			
			try {
				if (count($data['SaleItem']) < 1) {
					$errors['custom'][] = 'Belum memilih produk yang terjual.';
					$this->renderAdd($data, $errors);
					return;
				}
				$this->Sale->create();
				if(!$this->Sale->save($data)){
					$ds->rollback();
					$this->renderAdd($data,$this->Sale->validationErrors);
					return;
				}

				$sale_id = $this->Sale->getLastInsertID();
				if (isset($data['Sale']['is_paid']) && !empty($data['Sale']['is_paid']) && $data['Sale']['is_paid']=='on') {
					$data['Sale']['status_sales'] = 'paid';
					$data['Payment']['sale_id'] = $sale_id;
					$data['Payment']['admin_id'] = $admin_id;
					$data['Payment']['payment_journal_type'] = 'income';
					$data['Payment']['payment_date'] = date('Y-m-d H:i:s',strtotime($data['Payment']['payment_date']));
					//KALAU ADA KEMBALIAN
					$remaining_offline_payment = $data['Sale']['total_nominal']-$data['Payment']['debit_nominal']-$data['Payment']['transfer_nominal'];
                	$charge = $data['Payment']['cash_nominal'] - $remaining_offline_payment;

                	if ($charge > 0 ) {
                		$data['Payment']['charge'] = $charge;
                	}

                	$this->Payment->create();
					if(!$this->Payment->save($data)){
						$ds->rollback();
						$this->renderAdd($data,$this->Payment->validationErrors);
						return;
					}

				}
				
				//debug()
				
				foreach ($data['SaleItem'] as $key => $value) {
					$sale_item = array(
						'SaleItem' => array(
							'product_id' => $value['Product']['id'],
							'sale_id' => $sale_id,
							'qty' => $value['qty'],
							'unit_price' => $value['unit_price'],
							'total_price' => $value['total_price'],
							'admin_id' => $admin_id,
							'employee_id' => $value['Employee']['id'],
							'division_id' => $value['Division']['id']
						)
					);
					$this->SaleItem->create();
					if (!$this->SaleItem->save($sale_item)) {
						$ds->rollback();
						$this->renderAdd($data,$this->SaleItem->validationErrors);
						return;
					}
				}

				//SESSION TRANSACTION

				if (isset($data['Sale']['is_paid']) && !empty($data['Sale']['is_paid']) && $data['Sale']['is_paid']=='on') {
					
					$session_id = array();
					$session = $this->TransactionSession->find('first', array(
		                'conditions' => array('end_date' => null, 'admin_id' => $admin_id)
		            ));
					if ($this->Session->check('session_id') && !empty($session)) {
	                    $session_id = $this->Session->read('session_id');
	                    //TAMBAHKAN SESSION HISTORY

	                    if ($data['Payment']['cash_nominal'] != '' && $data['Payment']['cash_nominal'] != 0) {
							//KALAU ADA CASH
							$this->addTransactionSessionHistory($session_id,$sale_id,'cash','income',$data['Payment']['cash_nominal']);

							$session['TransactionSession']['cr_balance'] += $data['Payment']['cash_nominal'];
							
							//KALAU ADA KEMBALIAN
							$remaining_offline_payment = $data['Sale']['total_nominal']-$data['Payment']['debit_nominal']-$data['Payment']['transfer_nominal'];
	                    	$charge = $data['Payment']['cash_nominal'] - $remaining_offline_payment;

	                    	
	                    	if ($charge > 0 ) {
	                    		$session['TransactionSession']['cr_balance'] -= $charge;
	                    		$this->addTransactionSessionHistory($session_id,$sale_id,'cash','outcome',(string)$charge);
	                    	}
	                    	$session['TransactionSession']['cr_balance'] -= $charge;
	                    	//SIMPAN CR BALANCE
	                    	if (!$this->TransactionSession->save($session)) {
								$ds->rollback();
								$this->renderAdd($data,$this->TransactionSession->validationErrors);
								return;
							}
						}
						//KALAU ADA TRANSFER
	                    if ($data['Payment']['transfer_nominal'] != '' && $data['Payment']['transfer_nominal'] != 0) {
	                    	$this->addTransactionSessionHistory($session_id,$sale_id,'transfer','income',$data['Payment']['transfer_nominal']);	
						}
						//KALAU ADA DEBET
	                    if ($data['Payment']['debit_nominal'] != '' && $data['Payment']['debit_nominal'] != 0) {
							$this->addTransactionSessionHistory($session_id,$sale_id,'debit','income',$data['Payment']['debit_nominal']);
						}
	                }
				}
				

				$ds->commit();
				$this->setFlash('Data penjualan berhasil disimpan.', 'success', '/admin/sales/add');
			} catch (Exception $e) {
				$ds->rollback();
				$errors['custom'][] = 'Terjadi kesalahan saat akan melakukan penyimpanan.';

				$this->renderAdd($data, $errors);
				return;
			}
			
		}
		$this->renderAdd();
	}

	

	private function renderAdd($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('Penjualan', '/admin/sales', 'fa-list'),
			array('Tambah Penjualan', '', 'fa-plus-circle')
		);	

		$products = $this->Product->find('all',array(
			'conditions' => array(
				'status_active' => 'active'
			)
		));

		$customers = $this->Customer->find('all',array(
			'conditions' => array(
				'status_active' => 'active'
			)
		));
		$this->Division->recursive = 2;
		$divisions = $this->Division->find('all');
		$this->set(compact('customers'));
		$this->set(compact('divisions'));
		//UNTUK ELEMENT PRODUCT PICKER 
		$this->set(compact('products'));
		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	private function addTransactionSessionHistory($session_id,$sale_id,$type,$status,$nominal){
		$admin_id = $this->Auth->user('id');
		$transaction_session_history = array(
			'TransactionSessionHistory' => array(
				'admin_id' => $admin_id,
				'status' => $status,
				'total' => $nominal,
				'type' => $type,
				'session_id' => $session_id,
				'sale_id' => $sale_id
			)
		);
		
		$this->TransactionSessionHistory->create();
		if (!$this->TransactionSessionHistory->save($transaction_session_history)) {
			$ds->rollback();
			$this->renderAdd($data,$this->TransactionSessionHistory->validationErrors);
			return;
		}
	}

	public function edit($id = null){
		$id = $this->params['id'];
		if (empty($id)) {
			$this->invalidRequest();
		}
		$this->Sale->recursive = 2;
		$sale = $this->Sale->findById($id);
		if (empty($sale)) {
			$this->invalidRequest();
		}
		//check if there is customer birthday 
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$admin_id = $this->Auth->user('id');
			$sale['Sale']['status_sales'] = 'unpaid';
			$sale['Sale']['customer_id'] = $data['Sale']['customer_id'];
			$sale['Sale']['total_nominal'] = $data['Sale']['total_nominal'];
			$sale['Sale']['sales_type'] = $data['Sale']['sales_type'];
			$sale['Sale']['admin_id'] = $admin_id;
			$sale['Sale']['last_modified_date'] = date('Y-m-d H:i:s');

			if (isset($data['Sale']['is_paid']) && !empty($data['Sale']['is_paid']) && $data['Sale']['is_paid']=='on') {
				//MENGAMBIL DATA PEMBAYARAN
				$sale['Sale']['status_sales'] = 'paid';
				$payment = $this->Payment->find('first',array(
					'conditions' => array(
						'Payment.sale_id' => $sale['Sale']['id']
					)
				));

				if (empty($payment)) {
					$this->setFlash('Database tidak konsisten.', 'danger', '/admin/sales/edit/'.$sale['Sale']['id']);
					return;
				}

				$payment['Payment']['cash_nominal'] = $data['Payment']['cash_nominal'];
				$payment['Payment']['transfer_nominal'] = $data['Payment']['transfer_nominal'];
				$payment['Payment']['transfer_bank_name'] = $data['Payment']['transfer_bank_name'];
				$payment['Payment']['transfer_bank_account'] = $data['Payment']['transfer_bank_account'];
				$payment['Payment']['transfer_name'] = $data['Payment']['transfer_name'];
				$payment['Payment']['total_nominal'] = $data['Payment']['total_nominal'];
				$payment['Payment']['payment_purpose'] = $data['Payment']['payment_purpose'];

				$payment['Sale']['status_sales'] = 'paid';
				$payment['Payment']['admin_id'] = $admin_id;
				$payment['Payment']['payment_journal_type'] = 'income';
				$payment['Payment']['payment_date'] = date('Y-m-d H:i:s',strtotime($data['Payment']['payment_date']));
				$payment['Payment']['last_modified_date'] = date('Y-m-d H:i:s');

				$remaining_offline_payment = $data['Sale']['total_nominal']-$data['Payment']['debit_nominal']-$data['Payment']['transfer_nominal'];
            	$charge = $data['Payment']['cash_nominal'] - $remaining_offline_payment;

            	if ($charge > 0 ) {
            		$payment['Payment']['charge'] = $charge;
            	}

				if (!$this->Payment->save($payment)) {
					$ds->rollback();
					$this->renderEdit($data,$this->Payment->validationErrors);
					return;
				}
			}
			if(isset($data['Payment']['payment_purpose']) && !empty($data['Payment']['payment_purpose'])){
				$sale['Sale']['status_payment'] = $data['Payment']['payment_purpose'];
			}
			$ds = $this->Sale->getDataSource();
			$ds->begin();
			
			try {
				if (count($data['SaleItem']) < 1) {
					$errors['custom'][] = 'Belum memilih produk yang terjual.';
					$this->renderEdit($data, $errors);
					return;
				}
				if(!$this->Sale->save($sale)){
					$ds->rollback();
					$this->renderEdit($data,$this->Sale->validationErrors);
					return;
				}
				//HAPUS SALE ITEM YANG LAMA
				if (!$this->SaleItem->deleteAll(array('SaleItem.sale_id'=>$sale['Sale']['id']))) {
					$ds->rollback();
					$this->renderEdit($data,$this->SaleItem->validationErrors);
					return;
				}
				//TAMBAHKAN SALE ITEM YANG BARU
				$sale_id = $sale['Sale']['id'];
				foreach ($data['SaleItem'] as $key => $value) {
					$sale_item = array(
						'SaleItem' => array(
							'product_id' => $value['Product']['id'],
							'sale_id' => $sale_id,
							'qty' => $value['qty'],
							'unit_price' => $value['unit_price'],
							'total_price' => $value['total_price'],
							'admin_id' => $admin_id,
							'employee_id' => $value['Employee']['id'],
							'division_id' => $value['Division']['id']
						)
					);
					$this->SaleItem->create();
					if (!$this->SaleItem->save($sale_item)) {
						$ds->rollback();
						$this->renderEdit($data,$this->SaleItem->validationErrors);
						return;
					}
				}
				$ds->commit();
				$this->setFlash('Data penjualan berhasil diedit.', 'success', '/admin/sales/edit/'.$sale['Sale']['id']);
			} catch (Exception $e) {
				$ds->rollback();
				$errors['custom'][] = 'Terjadi kesalahan saat akan melakukan perubahan data.';
				$this->renderEdit($data, $errors);
				return;
			}

			
		}
		$this->renderEdit($sale);
	}

	private function renderEdit($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('Penjualan', '/admin/sales', 'fa-list'),
			array('Edit Penjualan', '', 'fa-plus-circle')
		);	

		$products = $this->Product->find('all',array(
			'conditions' => array(
				'status_active' => 'active'
			)
		));

		//MENGAMBIL DATA PEMBAYARAN PERTAMA KALAU ADA.
		if ($data['Sale']['status_sales'] == 'paid') {
			$payment = $this->Payment->find('first',array(
				'conditions'=> array(
					'Payment.sale_id' => $data['Sale']['id']
				)
			));

			$data['Payment'] = $payment['Payment'];

			//MENGAKTIFKAN CHECKBOX
			$data['Sale']['is_paid'] = 'on';
		}

		$customers = $this->Customer->find('all',array(
			'conditions' => array(
				'status_active' => 'active'
			)
		));
		$this->Division->recursive = 2;
		$divisions = $this->Division->find('all');
		$this->set(compact('customers'));
		$this->set(compact('divisions'));
		//UNTUK ELEMENT PRODUCT PICKER 
		$this->set(compact('products'));
		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function payoff($id = array()){
		
		$id = $this->params['id'];
		if (empty($id)) {
			$this->invalidRequest();
		}

		$this->Sale->recursive = 2;
		$sale = $this->Sale->findById($id);
		if (empty($sale)) {
			$this->invalidRequest();
		}
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$admin_id = $this->Auth->user('id');
			$data['Sale'] = $sale['Sale'];
			if (isset($data['Payment']['payment_purpose']) && !empty($data['Payment']['payment_purpose']) && $data['Payment']['payment_purpose']=='paid_off') {
				$sale['Sale']['status_sales'] = 'paid';
				$sale['Sale']['status_payment'] = 'paid_off';
				$data['Payment']['admin_id'] = $admin_id;
				$data['Payment']['payment_journal_type'] = 'income';
				$data['Payment']['sale_id'] = $data['Sale']['id'];
				$data['Payment']['payment_date'] = date('Y-m-d H:i:s',strtotime($data['Payment']['payment_date']));
			}
			$ds = $this->Sale->getDataSource();
			$ds->begin();
			try {
				$this->Payment->create();
				if (!$this->Payment->save($data)) {
					$ds->rollback();
					$this->renderPayoff($data,$this->Payment->validationErrors);
					debug($this->Payment->validationErrors);die;
					return;
				}

				if (!$this->Sale->save($sale)) {
					$ds->rollback();
					$this->renderPayoff($data,$this->Sale->validationErrors);
					debug($this->Sale->validationErrors);die;
					return;
				}
				$ds->commit();
				$this->setFlash('Pelunasan penjualan berhasil lakukan.', 'success', '/admin/sales');
			} catch (Exception $e) {
				$ds->rollback();
				$errors['custom'][] = 'Terjadi kesalahan saat akan melakukan penyimpanan.';
				$this->renderPayoff($data, $errors);
				return;
			}
		}
		$this->renderPayoff($sale);
	}

	private function renderPayoff($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('Penjualan', '/admin/sales', 'fa-list'),
			array('Pelunasan', '', 'fa-money')
		);

		$total_payment = 0;
		if ($data['Sale']['status_sales'] == 'paid') {
			$payments = $this->Payment->find('all',array(
				'conditions'=> array(
					'Payment.sale_id' => $data['Sale']['id'],
					'Payment.payment_journal_type' => 'income'
				)
			));

			
			foreach ($payments as $key => $value) {
				if (isset($value['Payment']['total_nominal']) && !empty($value['Payment']['total_nominal'])) {
					$total_payment += $value['Payment']['total_nominal'];
				}
			}

			$this->set(compact('payments'));

		}

		$remaining_payment = $data['Sale']['total_nominal'] - $total_payment;
		$this->set(compact('remaining_payment'));
		$this->set(compact('total_payment'));
		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function detail($id = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('Penjualan', '/admin/sales', 'fa-list'),
			array('Detil', '', 'fa-list')
		);

		$id = $this->params['id'];
		if (empty($id)) {
			$this->invalidRequest();
		}
		$this->Sale->recursive = 2;
		$sale = $this->Sale->findById($id);
		if (empty($sale)) {
			$this->invalidRequest();
		}

		$this->set('data',$sale);

		if (isset($sale['Sale']['status_sales']) && !empty($sale['Sale']['status_sales']) && $sale['Sale']['status_sales']=='paid') {
			$payments = $this->Payment->find('all',array(
				'conditions' => array(
					'Payment.payment_journal_type' => 'income',
					'Payment.sale_id' => $sale['Sale']['id']
				)
			));
			$this->set('payments',$payments);

			$total_payment = 0;
			foreach ($payments as $key => $value) {
				$total_payment += $value['Payment']['total_nominal'];
			}
			$this->set('total_payment',$total_payment);
		}

	}

	public function delete($id = null){

		$id = $this->params['id'];
		if (empty($id)) {
			$this->invalidRequest();
		}
		$this->Sale->recursive = 2;
		$sale = $this->Sale->findById($id);
		if (empty($sale)) {
			$this->invalidRequest();
		}

		$ds = $this->Sale->getDataSource();
		$ds->begin();
		$sale['Sale']['status_active']= 'inactive';
		$sale['Sale']['last_modified_date']= date('Y-m-d H:i:s');
		try {
			if (!$this->SaleItem->deleteAll(array('SaleItem.sale_id'=>$id))) {
				$ds->rollback();
				$this->setFlash('Terjadi kesalahan saat menghapus data penjualan.', 'danger', '/admin/sales');
				return;
			}
			if (!$this->Payment->deleteAll(array('Payment.sale_id'=>$id))) {
				$ds->rollback();
				$this->setFlash('Terjadi kesalahan saat menghapus data penjualan.', 'danger', '/admin/sales');
				return;
			}
			if (!$this->Sale->save($sale)) {
				$ds->rollback();
				$this->setFlash('Terjadi kesalahan saat menghapus data penjualan.', 'danger', '/admin/sales');
				return;
			}
			$ds->commit();
			$this->setFlash('Data penjualan telah terhapus.', 'warning', '/admin/sales');
		} catch (Exception $e) {
			$ds->rollback();
			$this->setFlash('Terjadi kesalahan saat menghapus data penjualan4.', 'danger', '/admin/sales');
			return;
		}
		
		

	}
}

