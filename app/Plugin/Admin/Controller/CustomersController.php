<?php
App::uses('PosAppController', 'Admin.Controller');
/**
 * PosDashboards Controller
 *
 */
class CustomersController extends AdminAppController {

	public $uses = array('Admin.Customer');

	public function isAuthorized($user){
		$this->parent = 'customers';
		$this->module = 'admin';

		return true;
	}

	public function index(){
		if(!$this->checkPrivilege(1)) $this->notAuthorized();
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '', 'fa-list')
		);
		$data = $this->request->query;	
		$query = array();
		$sort = array();
		if(!empty($data)){
			if($data['query'] != ""){
				$query['OR']['Customer.name LIKE'] = '%'.$data['query'].'%';
				$query['OR']['Customer.city LIKE'] = '%'.$data['query'].'%';
				$query['OR']['Customer.phone LIKE'] = '%'.$data['query'].'%';
				$query['OR']['Customer.email LIKE'] = '%'.$data['query'].'%';
			}

			if($data['sort'] != "") {
				if($data['sort'] == 'created_date') $sort['Customer.created_date'] = 'asc';
				elseif($data['sort'] == 'name') $sort['Customer.name'] = 'asc';
				elseif($data['sort'] == 'phone') $sort['Customer.phone'] = 'asc';
				elseif($data['sort'] == 'email') $sort['Customer.email'] = 'asc';
				elseif($data['sort'] == 'city') $sort['Customer.city'] = 'asc'; 
			}

		}else{
			$data = null;
		}

		$this->set('searchData', $data);
		$this->renderIndexSortBy($data['sort']);

		$query['AND']['Customer.status_active'] = 'active';
		// debug($query); 
		// debug($sort); die();

		$this->Paginator->settings = array(
			'limit' => 20,
			'conditions' => $query,
			'order' => $sort
		);
		
		$this->set('customers', $this->Paginator->paginate('Customer'));
		// die();
	}

	private function renderIndexSortBy($sortBy) {
		$setSelectSort = "";

		$setSelectSort .= "<option selected=true> Urut Berdasarkan </option>";
		
		if($sortBy == 'created_date') {
			$setSelectSort .= "<option value='created_date' selected=true> Tanggal Dibuat </option>";
		}
		else {
			$setSelectSort .= "<option value='created_date'> Tanggal Dibuat </option>";
		}
		if($sortBy == 'name') {
			$setSelectSort .= "<option value='name' selected=true> Nama </option>";
		}
		else {
			$setSelectSort .= "<option value='name'> Nama </option>";
		}
		if($sortBy == 'phone') {
			$setSelectSort .= "<option value='phone' selected=true> No. HP </option>";	
		}
		else {
			$setSelectSort .= "<option value='phone'> No. HP </option>";
		}
		if($sortBy == 'email') {
			$setSelectSort .= "<option value='email' selected=true> Email </option>";
		}
		else {
			$setSelectSort .= "<option value='email'> Email </option>";
		}
		if($sortBy == 'city') {
			$setSelectSort .= "<option value='city' selected=true> Kota </option>";
		}
		else {
			$setSelectSort .= "<option value='city'> Kota </option>";
		}

		$this->set('sortData', $setSelectSort);
	}

	public function add() {
		if($this->request->is('post')){
			$data = $this->request->data;
			$data['Customer']['admin_id'] = $this->Auth->user('id');
			$data['Customer']['birthday'] = date('Y-m-d h:i:s', strtotime($data['Customer']['birthday']));
			$ds = $this->Customer->getDataSource();
			$ds->begin();
			try {
				$this->Customer->create();
				if(!$this->Customer->save($data)) {
					$ds->rollback();
					$this->renderAdd($data, $this->Customer->validationErrors);
					return;
				}
				$ds->commit();
				$this->setFlash('Pelanggan Berhasil Ditambahkan.', 'success', '/admin/customers/add');
			} catch (Exception $e) {
				$ds->rollback();
				$errors['custom'][] = $e->getMessage();
				$this->renderAdd($data, $errors);
				return;
			}
		}
		$this->renderAdd();
	}

	private function renderAdd($data = array(), $validationErrors = array()){	
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/customers', 'fa-list'),
			array('Add', '', 'fa-plus-circle')
		);
		$this->set(compact('data'));
		$this->set(compact('validationErrors'));

		$cities = $this->Customer->find('list', array(
			'fields' => array('Customer.city'),
			'group' => array('Customer.city')));
		$this->set('cities', $cities);
	}

	public function edit(){
		$id = $this->params['id'];
		if($id == null) $this->invalidRequest();

		$customers = $this->Customer->findById($id);
		if(empty($customers)) $this->invalidRequest();
		else {
			$birthday = $customers['Customer']['birthday'];
			$customers['Customer']['birthday'] = date('m/d/Y', strtotime($birthday));
		}
		if($this->request->is('post')){
			$data = $this->request->data;
			$customers['Customer']['name'] = $data['Customer']['name'];
			$customers['Customer']['email'] = $data['Customer']['email'];
			$customers['Customer']['city'] = $data['Customer']['city'];
			$customers['Customer']['phone'] = $data['Customer']['phone'];
			$customers['Customer']['address'] = $data['Customer']['address'];
			$customers['Customer']['birthday'] = date('Y-m-d H:i:s', strtotime($data['Customer']['birthday']));
			//$customers['Customer']['last_modified_date'] = date('Y-m-d h:i:s');
			if(!$this->Customer->save($customers)){
				$this->renderAdd($data, $this->Customer->validationErrors);
				return;
			}
			$this->setFlash('Data pelanggan berhasil diubah.', 'success', '/admin/customers/edit/'.$customers['Customer']['id']);
		}
		$this->renderEdit($customers);
	}

	private function renderEdit($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/customers', 'fa-list'),
			array('Edit', '', 'fa-pencil')
		);

		$this->set(compact('data'));
		$this->set(compact('validationErrors'));

		$cities = $this->Customer->find('list', array(
			'fields' => array('Customer.city'),
			'group' => array('Customer.city')));
		$this->set('cities', $cities);
	}

	public function delete($id = null){
		if($id == null) $this->invalidRequest();
		$customers = $this->Customer->findById($id);
		if(empty($customers)) $this->invalidRequest();

		$this->Customer->id = $id;
		if(!$this->Customer->saveField('status_active', 'inactive')){
			$this->setFlash('Terjadi kesalahan sistem.', 'danger', '/admin/customers');
			return;
		}
		$this->setFlash('Pelanggan berhasil dihapus.', 'warning', '/admin/customers');
	}

	public function detail($id = null) {
		$id = $this->params['id'];
		if($id == null) $this->invalidRequest();

		$customers = $this->Customer->findById($id);
		if(empty($customers)) $this->invalidRequest();
		
		$this->renderDetail($customers);
	}

	private function renderDetail($data = array(), $validationErrors = array()) {
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/customers', 'fa-list'),
			array('Detail', '', 'fa-list')
			);

		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function get_city_typeahead(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			
			$datas = $this->Customer->find('all', array(
				'conditions' => array('Customer.city <>' => null),
				'group' => array('Customer.city')

			));
			$return = array();
			foreach ($datas as $key => $data) {
				$tmp = array(
					'id' => $data['Customer']['id'],
					'name' => $data['Customer']['city']
					);
				$return[] = $tmp;
			}
			echo json_encode($return);
			return;
		}
		echo json_encode(array('code'=>500, 'message'=>'Invalid Request!'));
		return;
	}
}
