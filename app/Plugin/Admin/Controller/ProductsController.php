<?php
App::uses('AdminAppController', 'Admin.Controller');
/**
 * Products Controller
 *
 */
class ProductsController extends AdminAppController {

	public $uses = array('Admin.Product', 'Admin.Category');

	public function isAuthorized($user){
		/* sesuaikan privilege */
		$this->parent = 'products';
		$this->module = 'admin';
		return true;
	}

	public function index(){
		if(!$this->checkPrivilege(1)) $this->notAuthorized();
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '', 'fa-list')
		);
		$data = $this->request->query;	
		$query = array();
		if(!empty($data)){
			if($data['query'] != ""){
				$query['OR']['Product.name LIKE'] = '%'.$data['query'].'%';
			}
		}else{
			$data = null;
		}
		$this->set('searchData', $data);

		$query['AND']['Product.status_active'] = 'active'; 
		$this->Paginator->settings = array(
			'limit' => 20,
			'conditions' => $query,
		);

		$this->set('products', $this->Paginator->paginate('Product'));
	}

	public function add(){
		if($this->request->is('post')){
			$data = $this->request->data;
			$this->request->data['Product']['admin_id'] = $this->Auth->user('id');
			if(!$this->Product->save($this->request->data)){
				$this->renderAdd($data, $this->Product->validationErrors);
				return;
			}
			$this->setFlash('Product has been added.', 'success', '/admin/products/add');
		}
		$this->renderAdd();
	}
	private function renderAdd($data = array(), $validationErrors = array()){	
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/products', 'fa-list'),
			array('Add', '', 'fa-plus-circle')
		);
		$categories = $this->Category->find('all');
		$this->set(compact('categories'));
		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function edit(){
		$id = $this->params['id'];
		if($id == null) $this->invalidRequest();

		$products = $this->Product->findById($id);
		if(empty($products)) $this->invalidRequest();
		if($this->request->is('post')){
			$data = $this->request->data;
			$this->request->data['Product']['id'] = $products['Product']['id'];
			$this->request->data['Product']['admin_id'] = $this->Auth->user('id');
			if(!$this->Product->save($this->request->data)){
				$this->renderAdd($data, $this->Product->validationErrors);
				return;
			}
			$this->setFlash('Data produk berhasil diubah.', 'success', '/admin/products/edit/'.$products['Product']['id']);
		}
		$this->renderEdit($products);
	}
	private function renderEdit($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/products', 'fa-list'),
			array('Edit', '', 'fa-pencil')
		);
		$categories = $this->Category->find('all');
		$this->set(compact('categories'));

		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function delete($id = null){
		if($id == null) $this->invalidRequest();
		$products = $this->Product->findById($id);
		if(empty($products)) $this->invalidRequest();

		$this->Product->id = $id;
		if(!$this->Product->saveField('status_active', 'inactive')){
			$this->setFlash('Terjadi kesalahan sistem.', 'danger', '/admin/products');
			return;
		}
		$this->setFlash('Product berhasil dihapus.', 'warning', '/admin/products');
	}
}
