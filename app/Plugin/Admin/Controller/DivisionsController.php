<?php
App::uses('AdminAppController', 'Admin.Controller');
/**
 * Divisions Controller
 *
 */
class DivisionsController extends AdminAppController {

	public $uses = array('Admin.Division', 'Admin.Employee', 'Admin.DivisionEmployee');

	public function isAuthorized($user){
		/* sesuaikan privilege */
		$this->parent = 'divisions';
		$this->module = 'admin';
		return true;
	}

	public function index(){
		if(!$this->checkPrivilege(1)) $this->notAuthorized();
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '', 'fa-list')
		);
		$data = $this->request->query;	
		$query = array();
		$sort = array();
		if(!empty($data)){
			if($data['query'] != ""){
				$query['OR']['Division.name LIKE'] = '%'.$data['query'].'%';
			}

			if($data['sort'] != "") {
				if($data['sort'] == 'created_date') $sort['Customer.created_date'] = 'asc';
				elseif($data['sort'] == 'name') $sort['Customer.name'] = 'asc';
			}
		}else{
			$data = null;
		}
		$this->set('searchData', $data);
		$this->renderIndexSortBy($data['sort']);

		// $query['AND']['Division.status_active'] = 'active'; 
		$this->Paginator->settings = array(
			'limit' => 20,
			'conditions' => $query,
			'order' => $sort
		);

		$this->set('divisions', $this->Paginator->paginate('Division'));
	}

	private function renderIndexSortBy($sortBy) {
		$setSelectSort = "";

		$setSelectSort .= "<option selected=true> Urut Berdasarkan </option>";
		
		if($sortBy == 'created_date') {
			$setSelectSort .= "<option value='created_date' selected=true> Tanggal Dibuat </option>";
		}
		else {
			$setSelectSort .= "<option value='created_date'> Tanggal Dibuat </option>";
		}
		if($sortBy == 'name') {
			$setSelectSort .= "<option value='name' selected=true> Nama </option>";
		}
		else {
			$setSelectSort .= "<option value='name'> Nama </option>";
		}

		$this->set('sortData', $setSelectSort);
	}

	public function add(){
		if($this->request->is('post')){
			$data = $this->request->data;
			$data['Division']['admin_id'] = $this->Auth->user('id');
			$data['Division']['created_date'] = date('Y-m-d h:i:s');
			$data['Division']['last_modified_date'] = date('Y-m-d h:i:s');
			$ds = $this->Division->getDataSource();
			$ds->begin();

			try {
				$this->Division->create();
				if(!$this->Division->save($data)) {
					$ds->rollback();
					$this->renderAdd($data, $this->Division->validationErrors);
					return;
				}

				$division_id = $this->Division->getLastInsertId();
				foreach ($data['DivisionEmployee'] as $key => $division_employee) {
					// debug($division_employee); 
					$this->DivisionEmployee->create();
					$division_employee['division_id'] = $division_id;
					$division_employee['admin_id'] = $this->Auth->user('id');
					if(!$this->DivisionEmployee->save($division_employee)) {
						$ds->rollback();
						$this->renderAdd($data, $this->DivisionEmployee->validationErrors);
						return;
					}
				}
				$ds->commit();
				$this->setFlash('Divisi Berhasil Ditambahkan.', 'success', '/admin/divisions/add');
			} catch (Exception $e) {
				$ds->rollback();
				$errors['custom'][] = $e->getMessage();
				$this->renderAdd($data, $errors);
				return;
			}
		}
		$this->renderAdd();
	}

	private function renderAdd($data = array(), $validationErrors = array()){	
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/divisions', 'fa-list'),
			array('Add', '', 'fa-plus-circle')
		);
		$employees = $this->Employee->find('all', array(
			'conditions' => array(
				'status_active' => 'active'
			)
		));

		$this->set('employees', $employees);
		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function edit(){
		$id = $this->params['id'];
		if($id == null) $this->invalidRequest();

		$divisions = $this->Division->findById($id);
		if(empty($divisions)) $this->invalidRequest();
		
		$employeelist = $this->DivisionEmployee->find('list', array(
			'fields' => 'DivisionEmployee.employee_id',
			'conditions' => array('DivisionEmployee.division_id' => $id)));

		$divisions['DivisionEmployee'] = array();
		$number = 0;
		foreach($employeelist as $key => $employee_id) {
			$divisions['DivisionEmployee'][$number] = array();
			$employee = $this->Employee->findById($employee_id);
			$employee_name = $employee['Employee']['name'];
			array_push($divisions['DivisionEmployee'][$number], $employee_id);
			array_push($divisions['DivisionEmployee'][$number], $employee_name);
			$number++;
		}

		$divisions['row_number'] = sizeof($divisions['DivisionEmployee']) + 1;
		if($this->request->is('post')){
			$data = $this->request->data;

			$divisions['Division']['name'] = $data['Division']['name'];
			if(!$this->Division->save($divisions)){
				$this->renderAdd($data, $this->Division->validationErrors);
				return;
			}
			
			if(isset($data['DivisionEmployee'])) {
				$employee_ids = $data['DivisionEmployee'];
				foreach($employee_ids as $key => $division_employee_id) {
					foreach($employeelist as $key => $employee_id) {
						if($division_employee_id['employee_id'] != $employee_id) {
							$this->DivisionEmployee->deleteAll(
								[
									'division_id' => $id,
									'employee_id' => $employee_id
								]							
								);
						}
					}
				}

				foreach($employee_ids as $key => $division_employee) {
					$employee_id = $division_employee;

					$conditions = array(
						'employee_id' => $employee_id,
						'division_id' => $id
						);
					if(!$this->DivisionEmployee->hasAny($conditions)) {
						$this->DivisionEmployee->create();
						$division_employee['division_id'] = $id;
						$division_employee['admin_id'] = $this->Auth->user('id');
						if(!$this->DivisionEmployee->save($division_employee)) {
							// $ds->rollback();
							$this->renderEdit($data, $this->DivisionEmployee->validationErrors);
							return;
						}
					}
				}
			}
			else {
				foreach($employeelist as $key => $employee_id) {
					$this->DivisionEmployee->deleteAll(
						[
							'division_id' => $id,
							'employee_id' => $employee_id
						]							
					);
				}
			}

			$this->setFlash('Data divisi berhasil diubah.', 'success', '/admin/divisions/edit/'.$divisions['Division']['id']);
		}
		$this->renderEdit($divisions);
	}

	private function renderEdit($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/divisions', 'fa-list'),
			array('Edit', '', 'fa-pencil')
		);
		$employees = $this->Employee->find('all', array(
			'conditions' => array(
				'status_active' => 'active'
			)
		));

		$this->set('employees', $employees);
		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function delete($id = null){
		if($id == null) $this->invalidRequest();
		$divisions = $this->Division->findById($id);
		if(empty($divisions)) $this->invalidRequest();

		$this->Division->delete($id);
		$this->DivisionEmployee->deleteAll([
									'division_id' => $id
								]							
								);
		// $this->Division->id = $id;
		// if(!$this->Division->saveField('status_active', 'inactive')){
		// 	$this->setFlash('Terjadi kesalahan sistem.', 'danger', '/admin/divisions');
		// 	return;
		// }
		$this->setFlash('Divisi berhasil dihapus.', 'warning', '/admin/divisions');
	}

	public function detail($id = null) {
		$id = $this->params['id'];
		if($id == null) $this->invalidRequest();

		$divisions = $this->Division->findById($id);
		if(empty($divisions)) $this->invalidRequest();

		$employeelist = $this->DivisionEmployee->find('list', array(
			'fields' => 'DivisionEmployee.employee_id',
			'conditions' => array('DivisionEmployee.division_id' => $id)));

		$divisions['DivisionEmployee'] = array();
		$number = 0;
		foreach($employeelist as $key => $employee_id) {
			$divisions['DivisionEmployee'][$number] = array();
			$employee = $this->Employee->findById($employee_id);
			$employee_name = $employee['Employee']['name'];
			array_push($divisions['DivisionEmployee'][$number], $employee_id);
			array_push($divisions['DivisionEmployee'][$number], $employee_name);
			$number++;
		}

		$this->renderDetail($divisions);
	}

	private function renderDetail($data = array(), $validationErrors = array()) {
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/divisions', 'fa-list'),
			array('Detail', '', 'fa-list')
			);
		$employees = $this->Employee->find('all', array(
			'conditions' => array(
				'status_active' => 'active'
			)
		));

		$this->set('employees', $employees);
		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}
}
