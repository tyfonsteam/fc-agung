<?php
App::uses('AdminAppController', 'Admin.Controller');
/**
 * Admins Controller
 *
 */
class AdminsController extends AdminAppController {

    public $uses = array('Admin.Admin', 'Admin.TransactionSession');

    public function beforeFilter(){
        parent::beforeFilter();
    }

    public function beforeRender(){
        parent::beforeRender();
        $this->layout='login';
    }
    public function login(){
        
        $this->set('title_for_layout', 'Login');
        
        //if already logged-in, redirect
        if($this->Session->check('Auth.User')){
            //check jika masih aktif session
            $admin_id = $this->Session->read('Auth.User');
            $session = $this->TransactionSession->find('first', array(
                'conditions' => array('end_date' => null, 'admin_id' => $admin_id)
            ));

            if (!$this->Session->check('session_id') && !empty($session)) {
                $this->Session->write('session_id', $session['TransactionSession']['id']);
            }
            return $this->redirect($this->Auth->redirectUrl());
        }
        // if we get the post information, try to authenticate
        if ($this->request->is('post')) {  
            
            //check status invalid
            $admin = $this->Admin->findByUsername($this->request->data['Admin']['username']);
            if(empty($admin)){
                $this->Session->setFlash(__('Invalid username or password'), 'sessionmessage',array('class'=>'danger'));
                $this->redirect(array('action'=>'login'));
                return;
            }
            if($admin['Admin']['status'] == 'inactive'){
                $this->Session->setFlash(__('You do not have permission to access this'), 'sessionmessage',array('class'=>'danger'));
                $this->redirect(array('action'=>'login'));
                return;
            }
            if ($this->Auth->login()) {
                
                if (isset($this->request->data['Admin']['remember_me']) && $this->request->data['Admin']['remember_me'] == 1) {
                    // remove "remember me checkbox"
                    unset($this->request->data['Admin']['remember_me']);
                    
                    // hash the admin's password
                    $passwordHasher = new BlowfishPasswordHasher();
                    $this->request->data['Admin']['password'] =
                        $passwordHasher->hash($this->request->data['Admin']['password']);
                    
                    // write the cookie
                    $this->Cookie->write('its_me_cookie', $this->request->data['Admin'], true, '2 weeks');
                }
                $this->Session->setFlash(__('Welcome, '. $this->Auth->user('name')),'sessionmessage',array('class'=>'success'));
                
                //check jika masih aktif session
                $session = $this->TransactionSession->find('first', array(
                    'conditions' => array('end_date' => null, 'admin_id' => $admin['Admin']['id'])
                ));
                

                if (!$this->Session->check('session_id') && !empty($session)) {
                    $this->Session->write('session_id', $session['TransactionSession']['id']);
                    $this->redirect('/admin/sales/add');
                } else {
                    $priv = json_decode($admin['Admin']['privilege'],true);
                    $userPrivParent = $priv['admin'];
                    if(in_array(2, $userPrivParent['dashboard'])){
                        //langsung arah kan ke kasir / buka sesi
                        $this->redirect('/admin/sessions/open');
                        return;
                    }   
                }

               
                $this->redirect('/admin/dashboard');
            } else {
                $this->Session->setFlash(__('Invalid username or password'), 'sessionmessage',array('class'=>'danger'));
            }
        } 
    }

    public function logout(){
        if (!$this->Session->check('session_id')) {
         
            $this->Session->destroy();
            $this->Cookie->delete('its_me_cookie');
            
            return $this->redirect($this->Auth->logout());
        } else {
            $this->Session->setFlash('Anda harus mengakhiri sesi terlebih dahulu sebelum bisa keluar.','sessionmessage',array('class'=>'danger') );
            return $this->redirect("/admin");               
        }        
    }
}
