<?php

App::uses('AppController', 'Controller');
App::uses('CakeNumber', 'Utility');
App::uses('MyLibs', 'Admin.Lib');
App::uses('Upload', 'Admin.Lib');

class AdminAppController extends AppController {
	protected $global_settings = array();
	protected $noCurrency = 'NOIDR';
	protected $currency = 'IDR';
	protected $module = '';
	protected $parent = '';
	protected $breadcrumbs = array();
	public $components = array('Session',
				'Auth' => array(
					'loginAction' => array(
			            'controller' => 'admins',
			            'action' => 'login',
			            'plugin' => 'admin'
			        ),
					'logoutRedirect' => array(
						'plugin' => 'admin',
						'controller' => 'admins',
						'action' => 'login'
					),
					'authenticate' => array(
		                'Form' => array(
		                    'passwordHasher' => 'Blowfish',
		                    'userModel' => 'Admin.Admin'
		                )
		            ),
		            'authorize'=>array('Controller')
				),
				'Cookie',
				'Paginator'
			);

	public $uses = array('Admin.Admin');
	public $paginate = array();
	protected $myLibs;
	public $helper = array('Presentation');
	public function beforeFilter(){

		$this->layout = 'admin';
	   
	    if($this->name == 'CakeError') {
	    	if(Configure::read('debug') == 0){
		        $this->layout = 'error';
		        return;
	    	}else{
		        $this->layout = 'default';
	    	}
	    }
	   
		$this->Cookie->key = 'ca299d67d305bbb4d700d3f5ed3376d6#$%)asGb$@11~_+!@#HKis~#^';
		$this->Cookie->httpOnly = true;
		if (!$this->Auth->loggedIn() && $this->Cookie->read('its_me_cookie')) {
			$cookie = $this->Cookie->read('its_me_cookie');

			$Admin = $this->Admin->find('first', array(
				'conditions' => array(
					'Admin.username' => $cookie['username'],
					'Admin.password' => $cookie['password']
				)
			));

			if ($Admin && !$this->Auth->login($Admin['Admin'])) {

				$this->redirect('/users/logout'); // destroy session & cookie
			}
		}
		$this->myLibs = new MyLibs();
		CakeNumber::addFormat('IDR', array('before' => 'Rp. ', 'thousands' => '.', 'decimals' => ',', 'places'=>'0'));
		CakeNumber::addFormat('NOIDR', array('thousands' => '.', 'decimals' => ',', 'places'=>'0'));
	}

	public function beforeRender(){
	
		$this->set('breadcrumbs', $this->breadcrumbs);
		$user_data = $this->Admin->findById($this->Auth->user('id'));
		$this->set(compact('user_data'));
	
		if($this->module != '' && $this->parent != ''){
			$user = json_decode($this->Auth->user('privilege'),TRUE);
			$this->set('userPrivs', $user[$this->module][$this->parent]);
			$this->set('userPrivParent', $user[$this->module]);
		}else{
			$user = json_decode($this->Auth->user('privilege'),TRUE);
			$this->set('userPrivs', array());
			$this->set('userPrivParent', $user[$this->module]);
		}
		$this->global_settings = array(
			'company_name' => 'Agung',
			'company_detail' => 'Digital Printing | Copy Center',
			'address' => 'Jl. Babarsari No. 5, Jogja',
			'phone' => '0274-488612',
			'website' => 'www.agungprinting.com'
		);
		$this->set('global_settings', $this->global_settings);
		$this->set('breadcrumbs', $this->breadcrumbs);
		$this->set('currency', $this->currency);
		$this->set('noCurrency', $this->noCurrency);

		$this->set('auth_user', $this->Auth->user());
		$this->set('user_login_id', $this->Auth->user('id'));
		$photo = $this->Admin->find('first',array(
			'conditions'=> array('Admin.id'=>$this->Auth->user('id')),
			'fields' => 'Admin.photo'
		));
		if(isset($photo['Admin']['photo']) && !empty($photo['Admin']['photo'])){
			$this->set('user_profile_photo', $photo['Admin']['photo']);
		}

	}

	public function checkModule($module = ''){
		$user = json_decode($this->Auth->user('privilege'), TRUE);
		if(array_key_exists($module, $user)){
			return true;
		}
		return false;
	}

	public function checkParentModule(){
		$user = json_decode($this->Auth->user('privilege'), TRUE);

		if(array_key_exists($this->module, $user)){
			if(array_key_exists($this->parent, $user[$this->module])){

				return true;
			}
		}

		return false;
	}

	public function checkPrivilege($code=""){
		$user = json_decode($this->Auth->user('privilege'), TRUE);

		if(array_key_exists($this->module, $user)){
			if(array_key_exists($this->parent, $user[$this->module])){
				if(in_array($code, $user[$this->module][$this->parent])){
					return true;
				}
			}
		}
		return false;
	}

	public function isAuthorized($user) {
		return true;
	}

	
	public function notAuthorized(){

		$this->Session->setFlash(__('Anda tidak diijinkan mengakses halaman ini.'),
			'sessionmessage',
			array('class'=>'danger'));
		$this->redirect('/admin');

	}
}
