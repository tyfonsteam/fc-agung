<?php
App::uses('AdminAppController', 'Admin.Controller');
/**
 * Products Controller
 *
 */
class TransactionSessionsController extends AdminAppController {

	public $uses = array('Admin.Admin','Admin.TransactionSession','Admin.TransactionSessionHistory');

	public function isAuthorized($user){
		/* sesuaikan privilege */
		$this->parent = '';//transaction_sessions
		$this->module = 'admin';
		return true;
	}

	public function open(){
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$admin_id = $this->Auth->user('id');
			$data['TransactionSession']['admin_id'] = $admin_id;
			$data['TransactionSession']['start_date'] = date('Y-m-d H:i:s');
			$ds = $this->TransactionSession->getDataSource();
			$ds->begin();
			try {
				$this->TransactionSession->create();
				if (!$this->TransactionSession->save($data)) {
					$ds->rollback();
					$this->renderOpen($data,$this->TransactionSession->validationErrors);
					return;
				}
				$ds->commit();
				if (!$this->Session->check('session_id')) {
                    $this->Session->write('session_id', $this->TransactionSession->getLastInsertID());
                }
                $this->setFlash('Sesi Anda berhasil dibuka.', 'success', '/admin/sales/add');
			} catch (Exception $e) {
				$ds->rollback();
				$errors['custom'][] = $e->getMessage();
				$this->renderOpen($data, $errors);
				return;
			}

		}
		$this->renderOpen();
	}

	private function renderOpen($data = array(),$validationErrors = array()){
		$admin_id = $this->Auth->user('id');

		$admin  = $this->Admin->findById($admin_id);

		$this->set('data',$data);
		$this->set('admin',$admin);
		$this->set('validationErrors',$validationErrors);
	}

	public function close(){
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$admin_id = $this->Auth->user('id');

			if (!$this->Session->check('session_id')) {
            	$ds->rollback();
				$this->setFlash('Terjadi kesalahan, sesi sebelumnya tidak ditemukan.', 'danger', '/admin/sessions/open');
            }
            $session_id = $this->Session->read('session_id');
            $session = $this->TransactionSession->findById($session_id);
            if (empty($session)) {
            	$this->setFlash('Terjadi kesalahan, sesi sebelumnya tidak ditemukan.', 'danger', '/admin/sessions/open');
            }
            $session['TransactionSession']['end_date'] = date('Y-m-d H:i:s');
            $session['TransactionSession']['next_admin_id'] = $data['TransactionSession']['next_admin_id'];
            $session['TransactionSession']['closed_balance'] = $data['TransactionSession']['closed_balance'];
            $session['TransactionSession']['last_modified_date'] = date('Y-m-d H:i:s');

            if ($session['TransactionSession']['cr_balance']!=$data['TransactionSession']['closed_balance']) {
            	$session['TransactionSession']['status'] = 'unclear';
            } else {
            	$session['TransactionSession']['status'] = 'clear';
            }
			$ds = $this->TransactionSession->getDataSource();
			$ds->begin();
			try {
				if (!$this->TransactionSession->save($session)) {
					$ds->rollback();
					$this->renderClose($data,$this->TransactionSession->validationErrors);
					return;
				}
				$ds->commit();
				$this->Session->delete('session_id');
                $this->setFlash('Sesi telah ditutup.', 'success', '/admin/sessions/open');
			} catch (Exception $e) {
				$ds->rollback();
				$errors['custom'][] = $e->getMessage();
				$this->renderClose($data, $errors);
				return;
			}
		}
		$this->renderClose();
	}

	private function renderClose($data = array(),$validationErrors = array()){
		$admin_id = $this->Auth->user('id');

		$admin  = $this->Admin->findById($admin_id);

		$admins = $this->Admin->find('all',array(
			'conditions'=> array(
				'Admin.status' => 'active'
			)

		));
		$this->set('data',$data);
		$this->set('admin',$admin);
		$this->set('admins',$admins);
		$this->set('validationErrors',$validationErrors);
	}
	
}
