<?php
App::uses('AdminAppController', 'Admin.Controller');
class PrivilegesController extends AdminAppController{
	public $uses = array('Admin.Privilege');
	public $helper = array('Presentation');
	
	public function isAuthorized($user){
		$this->parent = 'privileges';
		$this->module = 'admin';
		return true;
	}

	public function index(){

		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List Module', '', 'fa-list')
		);	

		$this->set('modules', $this->Privilege->find('all', array(
			'fields' => 'DISTINCT Privilege.module'
			)));
	}

	public function lists(){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List Module', '/admin/privileges', 'fa-list'),
			array('Module', '', 'fa-lock')
		);	

		$data = $this->request->query;	
		$query = array();
		$query['Privilege.module'] = $this->params['module'];
		if(!empty($data)){
			if($data['query'] != ""){
				$query['Privilege.username LIKE'] = '%'.$data['query'].'%';
			}
		}else{
			$data = null;
		}
		$this->set('searchData', $data);
		$privileges = $this->Privilege->find('all', array(
			'conditions' => $query,
			'order'=>array('Privilege.parent_id' => 'asc', 'Privilege.code'=>'asc')
			));
		$privsChild = array();
		foreach ($privileges as $key => $privs) {
			if($privs['Privilege']['parent_id'] == 0) continue;
			$privsChild[$privs['Privilege']['parent_id']][] = $privs;
		}
		$this->set('privsChild', $privsChild);

		$this->set('module', $this->params['module']);
		$this->set('privileges', $privileges);
	}

	public function add(){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List Module', '/admin/privileges', 'fa-list'),
			array('Module '.ucfirst($this->params['module']), '/admin/privileges/lists/'.$this->params['module'], 'fa-lock'),
			array('Privilege', '', 'fa-key')
		);	
		if($this->request->is('post')){
			if(!$this->Privilege->save($this->request->data)){
				 $this->Session->setFlash(__('There was an error while saving privileges'),
	                'sessionmessage',
	                array('class'=>'danger'));
	            $this->redirect(array('action'=>'add'));
			}
			 $this->Session->setFlash(__('Your privileges has been saved'),
                'sessionmessage',
                array('class'=>'success'));
            $this->redirect('/admin/privileges/add/'.$this->params['parent'].'/'.$this->params['module'].'/'.$this->params['parent_id']);
		}
		$this->set('parent',$this->params['parent']);
		$this->set('module',$this->params['module']);
		$this->set('parent_id', $this->params['parent_id']);
	}

	public function addModule(){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List Module', '/admin/privileges', 'fa-list'),
			array('Create Module', '', 'fa-plus-circle')
		);	
		if($this->request->is('post')){
			if(!$this->Privilege->save($this->request->data)){
				 $this->Session->setFlash(__('There was an error while saving privileges'),
	                'sessionmessage',
	                array('class'=>'danger'));
	            $this->redirect(array('action'=>'index'));
			}
			 $this->Session->setFlash(__('Your privileges has been saved'),
                'sessionmessage',
                array('class'=>'success'));
            $this->redirect(array('action'=>'index'));
		}
	}
	
	public function addParent(){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List Module', '/admin/privileges', 'fa-list'),
			array('Module '.ucfirst($this->params['module']), '/admin/privileges/lists/'.$this->params['module'], 'fa-lock'),
			array('Create Parent', '', 'fa-key')
		);	
		if($this->request->is('post')){
			if(!$this->Privilege->save($this->request->data)){
				 $this->Session->setFlash(__('There was an error while saving privileges'),
	                'sessionmessage',
	                array('class'=>'danger'));
	            $this->redirect(array('action'=>'add'));
			}
			 $this->Session->setFlash(__('Your privileges has been saved'),
                'sessionmessage',
                array('class'=>'success'));
            $this->redirect('/admin/privileges/add/'.$this->params['parent'].'/'.$this->params['module'].'/'.$this->params['parent_id']);
		}
		$this->set('module', $this->params['module']);
	}

	public function edit(){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List Module', '/admin/privileges', 'fa-list'),
			array('Module '.ucfirst($this->params['module']), '/admin/privileges/lists/'.$this->params['module'], 'fa-lock'),
			array('Edit Parent', '', 'fa-key')
		);	
		$privileges = $this->Privilege->findById($this->params['id']);
		if(empty($privileges)){
			$this->Session->setFlash(__('There was an error while saving privileges'),
                'sessionmessage',
                array('class'=>'danger'));
            $this->redirect(array('action'=>'index'));
		}
		if($this->request->is('post')){
			if(!$this->Privilege->save($this->request->data)){
				 $this->Session->setFlash(__('There was an error while saving privileges'),
	                'sessionmessage',
	                array('class'=>'danger'));
	            $this->redirect(array('action'=>'index'));
			}
			 $this->Session->setFlash(__('Your privileges has been saved'),
                'sessionmessage',
                array('class'=>'success'));
            $this->redirect('/admin/privileges/edit/'.$privileges['Privilege']['id']);
		}
		$this->set('name', $privileges['Privilege']['name']);
		$this->set('id', $privileges['Privilege']['id']);
		$this->set('module', $privileges['Privilege']['module']);
	}
	
	public function delete($id = null){
		if($id == null){
			$this->Session->setFlash(__('Invalid Request'),
                'sessionmessage',
                array('class'=>'danger'));
            $this->redirect(array('action'=>'index'));
		}

		$privileges = $this->Privilege->findById($id);
		if(empty($privileges)){
			$this->Session->setFlash(__('There was an error while deleting privileges'),
                'sessionmessage',
                array('class'=>'danger'));
            $this->redirect(array('action'=>'index'));
		}
		if(!$this->Privilege->delete($id)){
			$this->Session->setFlash(__('There was an error while deleting privileges'),
                'sessionmessage',
                array('class'=>'danger'));
            $this->redirect(array('action'=>'index'));
		}

		if($privileges['Privilege']['parent_id'] == 0){
			if(!$this->Privilege->deleteAll(array('Privilege.parent_id'=>$privileges['Privilege']['id']))){
				$this->Session->setFlash(__('There was an error while deleting childs privileges'),
	                'sessionmessage',
	                array('class'=>'danger'));
	            $this->redirect(array('action'=>'index'));
			}
		}

		$this->Session->setFlash(__('Your privileges has been deleted'),
            'sessionmessage',
            array('class'=>'warning'));
        $this->redirect('/admin/privileges/lists/'.$privileges['Privilege']['module']);
	}
}

?>