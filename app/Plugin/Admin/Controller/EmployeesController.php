<?php
App::uses('AdminAppController', 'Admin.Controller');
/**
 * Employees Controller
 *
 */
class EmployeesController extends AdminAppController {

	public $uses = array('Admin.Employee');

	public function isAuthorized($user){
		/* sesuaikan privilege */
		$this->parent = 'employees';
		$this->module = 'admin';
		return true;
	}

	public function index(){
		if(!$this->checkPrivilege(1)) $this->notAuthorized();
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '', 'fa-list')
		);
		$data = $this->request->query;	
		$query = array();
		$sort = array();
		if(!empty($data)){
			if($data['query'] != ""){
				$query['OR']['Employee.name LIKE'] = '%'.$data['query'].'%';
				$query['OR']['Employee.phone LIKE'] = '%'.$data['query'].'%';
			}

			if($data['sort'] != "") {
				if($data['sort'] == 'created_date') $sort['Customer.created_date'] = 'asc';
				elseif($data['sort'] == 'name') $sort['Customer.name'] = 'asc';
				elseif($data['sort'] == 'phone') $sort['Customer.phone'] = 'asc';
			}
		}else{
			$data = null;
		}
		$this->set('searchData', $data);
		$this->renderIndexSortBy($data['sort']);

		$query['AND']['Employee.status_active'] = 'active'; 
		$this->Paginator->settings = array(
			'limit' => 20,
			'conditions' => $query,
			'order' => $sort
		);

		$this->set('employees', $this->Paginator->paginate('Employee'));
	}

	private function renderIndexSortBy($sortBy) {
		$setSelectSort = "";

		$setSelectSort .= "<option selected=true> Urut Berdasarkan </option>";
		
		if($sortBy == 'created_date') {
			$setSelectSort .= "<option value='created_date' selected=true> Tanggal Dibuat </option>";
		}
		else {
			$setSelectSort .= "<option value='created_date'> Tanggal Dibuat </option>";
		}
		if($sortBy == 'name') {
			$setSelectSort .= "<option value='name' selected=true> Nama </option>";
		}
		else {
			$setSelectSort .= "<option value='name'> Nama </option>";
		}
		if($sortBy == 'phone') {
			$setSelectSort .= "<option value='phone' selected=true> No. HP </option>";	
		}
		else {
			$setSelectSort .= "<option value='phone'> No. HP </option>";
		}

		$this->set('sortData', $setSelectSort);
	}

	public function add(){
		if($this->request->is('post')){
			$data = $this->request->data;
			$data['Employee']['admin_id'] = $this->Auth->user('id');
			$data['Employee']['created_date'] = date('Y-m-d h:i:s');
			$data['Employee']['join_date'] = date('Y-m-d h:i:s', strtotime($data['Employee']['join_date']));
			//$data['Employee']['resign_date'] = date('Y-m-d h:i:s', strtotime($data['Employee']['resign_date']));
			// debug($data); die();
			$ds = $this->Employee->getDataSource();
			$ds->begin();
			try {
				$this->Employee->create();
				if(!$this->Employee->save($data)) {
					$ds->rollback();
					$this->renderAdd($data, $this->Employee->validationErrors);
					return;
				}

				$ds->commit();
				$this->setFlash('Pegawai Berhasil Ditambahkan.', 'success', '/admin/employees/add');
			} catch (Exception $e) {
				$ds->rollback();
				$errors['custom'][] = $e->getMessage();
				$this->renderAdd($data, $errors);
				return;
			}
		}
		$this->renderAdd();
	}

	private function renderAdd($data = array(), $validationErrors = array()){	
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/employees', 'fa-list'),
			array('Add', '', 'fa-plus-circle')
		);

		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function edit(){
		$id = $this->params['id'];
		if($id == null) $this->invalidRequest();

		$employees = $this->Employee->findById($id);
		if(empty($employees)) $this->invalidRequest();
		else {
			$join_date = $employees['Employee']['join_date'];
			$employees['Employee']['join_date'] = date('m/d/Y', strtotime($join_date));

			$resign_date = $employees['Employee']['resign_date'];
			if($resign_date != "0000-00-00 00:00:00" && $resign_date != NULL) {
				$employees['Employee']['resign_date'] = date('m/d/Y', strtotime($resign_date));
			}
			else {
				$employees['Employee']['resign_date'] = '';
			}
		}
		
		if($this->request->is('post')){
			$data = $this->request->data;
			$employees['Employee']['name'] = $data['Employee']['name'];
			$employees['Employee']['phone'] = $data['Employee']['phone'];
			$employees['Employee']['address'] = $data['Employee']['address'];
			$employees['Employee']['base_salary'] = $data['Employee']['base_salary'];
			if(!$this->Employee->save($employees)){
				$this->renderAdd($data, $this->Employee->validationErrors);
				return;
			}
			$this->setFlash('Data pegawai berhasil diubah.', 'success', '/admin/employees/edit/'.$employees['Employee']['id']);
		}
		$this->renderEdit($employees);
	}

	private function renderEdit($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/employees', 'fa-list'),
			array('Edit', '', 'fa-pencil')
		);

		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function delete($id = null){
		if($id == null) $this->invalidRequest();
		$employees = $this->Employee->findById($id);
		if(empty($employees)) $this->invalidRequest();

		$this->Employee->id = $id;
		if(!$this->Employee->saveField('status_active', 'inactive')){
			$this->setFlash('Terjadi kesalahan sistem.', 'danger', '/admin/employees');
			return;
		}
		$this->setFlash('Pegawai berhasil dihapus.', 'warning', '/admin/employees');
	}

	public function detail($id = null) {
		$id = $this->params['id'];
		if($id == null) $this->invalidRequest();

		$employees = $this->Employee->findById($id);
		if(empty($employees)) $this->invalidRequest();
		else {
			$join_date = $employees['Employee']['join_date'];
			$employees['Employee']['join_date'] = date('m/d/Y', strtotime($join_date));
			
			$resign_date = $employees['Employee']['resign_date'];
			if($resign_date != "0000-00-00 00:00:00" && $resign_date != NULL) {
				$employees['Employee']['resign_date'] = date('m/d/Y', strtotime($resign_date));
			}
			else {
				$employees['Employee']['resign_date'] = '';
			}
		}
		
		$this->renderDetail($employees);
	}

	private function renderDetail($data = array(), $validationErrors = array()) {
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/employees', 'fa-list'),
			array('Detail', '', 'fa-list')
			);

		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}
}
