<?php
App::uses('AdminAppController', 'Admin.Controller');

/**
 * Users Controller
 *
 */
class UsersController extends AdminAppController {

	public $uses = array('Admin.Admin', 'Admin.Privilege');

	public function isAuthorized($user){
		$this->parent = 'users';
		$this->module = 'admin';

		return true;
	}

	public function profile(){
		$admin = $this->Admin->findById($this->Auth->user('id'));

		if($this->request->is('post')){
			$data = $this->request->data;
			$photo = $_FILES['photo'];
			
			$admin['Admin']['name'] = $this->request->data['Admin']['name'];
			$admin['Admin']['phone'] = $this->request->data['Admin']['phone'];
			$admin['Admin']['email'] = $this->request->data['Admin']['email'];
			$admin['Admin']['address'] = $this->request->data['Admin']['address'];
			
			if($this->request->data['Admin']['password'] != '' && $this->request->data['Admin']['password'] != $this->request->data['Admin']['re_password']){
				$errors['custom'][] = 'Kata Sandi harus sama dengan Kolom "Ulang Kata Sandi"';
				$this->renderProfile($data, $errors);	
			}
			if($this->request->data['Admin']['password'] == '') $admin['Admin']['flag'] = 1;

			if($this->request->data['Admin']['password'] != '' && $this->request->data['Admin']['password'] == $this->request->data['Admin']['re_password']){
				$admin['Admin']['password'] = $this->request->data['Admin']['password'];
			}

			$ds = $this->Admin->getDataSource();
			$ds->begin();
			try{

				if(!$this->Admin->save($admin)){
					$ds->rollback();
					$this->renderProfile($data, $this->Admin->validationErrors);
					return;
				}
				if($photo['error'] != 4){
					$tmp = $this->myLibs->uploadImage($photo, "users".DS.'profile'.DS.$admin['Admin']['id'].DS);
					
					if($tmp == FALSE){
						$ds->rollback();
						$errors['custom'][] = 'There was an error while do request 2';
						$this->renderProfile($data, $errors);
						return;
					}
					//delete file
					if($admin['Admin']['photo'] != null && $admin['Admin']['photo'] != ""){
						$photo_url = explode("/", $admin['Admin']['photo']);
						$photo_url = $photo_url[count($photo_url) - 2].DS.$photo_url[count($photo_url) - 1];
						if(is_file(APP.'Plugin'.DS.'Admin'.DS.'webroot'.DS.'img'.DS.'users'.DS.'profile'.DS.$photo_url)){
							if(!unlink(APP.'Plugin'.DS.'Admin'.DS.'webroot'.DS.'img'.DS.'users'.DS.'profile'.DS.$photo_url)){
								$ds->rollback();
								$this->Session->setFlash(__("Terjadi kesalahan saat meng-upload gambar"),
									'sessionmessage',
									array('class'=>'danger'));
								$this->renderEdit($request);
								return;
							}
						}
						$this->Admin->id = $admin['Admin']['id'];
						
						$this->Admin->saveField('photo', $tmp);
					}
					
				}

				$ds->commit();
				$this->Session->setFlash(__('Data profile Anda berhasil diubah'),
					'sessionmessage',
					array('class'=>'success'));
				$this->redirect('/admin/users/profile');
			}catch(Exception $e){
				$ds->rollback();
				$errors['custom'][] = 'There was an error while do request';
				$this->renderProfile($data, $errors);
				return;
			}
			
		}
		$this->renderProfile($admin);
	}

	public function renderProfile($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('User Profile', '', 'fa-user')
		);	

		$this->set(compact('data'));
		$this->set(compact('validationErrors'));
	}

	public function index(){
		//if(!$this->checkPrivilege(1)) $this->notAuthorized();
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('User Profile', '', 'fa-user')
		);	

		$data = $this->request->query;	
		$query = array();
		if(!empty($data)){
			if($data['query'] != ""){
				$query['Admin.username LIKE'] = '%'.$data['query'].'%';
			}
		}else{
			$data = null;
		}
		$this->set('searchData', $data);
		$this->Paginator->settings = array(
			'limit' => 5,
			'conditions' => $query,
			'order' => array(
				'Admin.status' => 'asc',
				'Admin.created_date' => 'desc'
				)
		);

		$this->set('admins', $this->Paginator->paginate('Admin'));
	}

	public function add(){
		//if(!$this->checkPrivilege(2)) $this->notAuthorized();	
		if($this->request->is('post')){
			$data = $this->request->data;
			if(isset($data['privs']) == null) $data['privs'] = array();
			$data['Admin']['privilege'] = json_encode($data['privs']);

			if($data['Admin']['password'] != $data['Admin']['re_password']){
				$this->renderAdd($data, array("password"=>array("Password still different with Re-Password")));
                return;
			}
            $this->Admin->create();
            if(!$this->Admin->save($data)){
                $this->renderAdd($data, $this->Admin->validationErrors);
                return;
            }
            $this->Session->setFlash(__('Admin berhasil disimpan.'),
                'sessionmessage',
                array('class'=>'success'));
            $this->redirect(array('action'=>'add'));
		}
		$this->renderAdd();
	}

	private function renderAdd($data = array(), $validationErrors = array()){
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('Admin', '/admin/users', 'fa-users'),
			array('Add', '/admin/users/add', 'fa-plus-circle')
		);
		$privileges = array();
		$module = array();
		$privs = $this->Privilege->find('all', array(
			'order' => array('Privilege.parent_id' => 'asc', 'Privilege.module' => 'asc')
			));
		foreach ($privs as $key => $priv) {
			if($priv['Privilege']['parent_id'] == 0){
				$privileges[$priv['Privilege']['module']][$priv['Privilege']['id']] = array('name'=>$priv['Privilege']['name']);
			}else{
				$privileges[$priv['Privilege']['module']][$priv['Privilege']['parent_id']][] = $priv;
			}
		}
		$this->set('privileges', $privileges);
		$this->set('data', $data);
		$this->set('validationErrors', $validationErrors);
	}

	public function edit(){
		//if(!$this->checkPrivilege(4)) $this->notAuthorized();
		if($this->request->is('post')){
			$data = $this->request->data;
			$admin = $this->Admin->findById($data['Admin']['id']);
			if(empty($admin)){
				$this->Session->setFlash(__('Invalid Request asdf'),
	                'sessionmessage',
	                array('class'=>'danger'));
	            $this->redirect(array('action'=>'index'));
			}

			if($data['Admin']['password'] != ''){
				if($data['Admin']['password'] != $data['Admin']['re_password']){
					$this->renderEdit($admin, array("password"=>array("Password still different with Re-Password")));
	                return;
				}else{
					$admin['Admin']['password'] = $data['Admin']['password'];
				}
			}else{
				$admin['Admin']['flag'] = 1;
			}
			

			$admin['Admin']['status'] = $data['Admin']['status'];

			$admin['Admin']['privilege'] = json_encode($data['privs']);
			$admin['Admin']['name'] = $data['Admin']['name'];
			$admin['Admin']['phone'] = $data['Admin']['phone'];
			$admin['Admin']['address'] = $data['Admin']['address'];
			if(!$this->Admin->save($admin)){
				$this->renderEdit($admin, $this->Admin->validationErrors);
				return;
			}

			//update privileges current user login
			if($admin['Admin']['id'] == $this->Auth->user('id')){
				$this->Session->write('Auth.User', $admin['Admin']);
			}

			$this->Session->setFlash(__('Data admin berhasil diubah.'),
                'sessionmessage',
                array('class'=>'success'));
            $this->redirect("/admin/users/edit/" . $admin['Admin']['id']);

            return;
		}

		$id = $this->params['id'];
		
		if($id == null){
			$this->Session->setFlash(__('Invalid Request'),
                'sessionmessage',
                array('class'=>'danger'));
            $this->redirect(array('action'=>'index'));
		}

		$admin = $this->Admin->findById($id);
		if(empty($admin)){
			$this->Session->setFlash(__('Invalid Request'),
                'sessionmessage',
                array('class'=>'danger'));
            $this->redirect(array('action'=>'index'));
		}

		
		$this->renderEdit($admin);
	}

	private function renderEdit($data = array(), $validationErrors = array()){
		$this->set('data', $data);
		$this->set('validationErrors', $validationErrors);
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('Admin', '/admin/users', 'fa-users'),
			array('Edit', '/admin/users/edit', 'fa-pencil')
		);
		$adminPrivs = json_decode($data['Admin']['privilege'], TRUE);
		$privileges = array();
		$module = array();
		$privs = $this->Privilege->find('all', array(
			'order' => array('Privilege.parent_id' => 'asc', 'Privilege.module' => 'asc')
			));
		foreach ($privs as $key => $priv) {
			if($priv['Privilege']['parent_id'] == 0){
				$privileges[$priv['Privilege']['module']][$priv['Privilege']['id']] = array('name'=>$priv['Privilege']['name']);
			}else{
				$parent_name = $privileges[$priv['Privilege']['module']][$priv['Privilege']['parent_id']]['name'];
				if(isset($adminPrivs[$priv['Privilege']['module']][$parent_name])){
					if(in_array($priv['Privilege']['code'], $adminPrivs[$priv['Privilege']['module']][$parent_name]))
						$priv['Privilege']['isChecked'] = true;
					else 
						$priv['Privilege']['isChecked'] = false;
				}else{
					$priv['Privilege']['isChecked'] = false;
				}
				$privileges[$priv['Privilege']['module']][$priv['Privilege']['parent_id']][] = $priv;
			}
		}
		$this->set('privileges', $privileges);

	}

	public function delete($id = null){
		if(!$this->checkPrivilege(8)) $this->notAuthorized();
		if($id == null){
			$this->Session->setFlash(__('Invalid Request'),
                'sessionmessage',
                array('class'=>'danger'));
            $this->redirect(array('action'=>'index'));
		}

		$admin = $this->Admin->findById($id);
		$admin['Admin']['status'] = 'inactive';
		$admin['Admin']['flag'] = 1;
		if(!$this->Admin->save($admin)){
			$this->Session->setFlash(__('There was an error while deactivate admin account'),
                'sessionmessage',
                array('class'=>'danger'));
            $this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Your admin has been deactivated'),
            'sessionmessage',
            array('class'=>'warning'));
        $this->redirect(array('action'=>'index'));
	}
}
