<?php
App::uses('PosAppController', 'Admin.Controller');
/**
 * PosDashboards Controller
 *
 */
class DashboardsController extends AdminAppController {

	public $uses = array('Admin.TransactionSession');

	public function isAuthorized($user){
		$this->parent = '';
		$this->module = 'admin';

		return true;
	}

	public function index(){
		//check if there is customer birthday 
		$admin_id = $this->Auth->user('id');
		$session = $this->TransactionSession->find('first', array(
            'conditions' => array('end_date' => null, 'admin_id' => $admin_id)
        ));

        if (!$this->Session->check('session_id') && !empty($session)) {
            $this->Session->write('session_id', $session['TransactionSession']['id']);
            $this->redirect('/admin/sales/add');
        }
	}
}
