<?php
App::uses('PosAppController', 'Admin.Controller');
/**
 * PosDashboards Controller
 *
 */
class ReportsController extends AdminAppController {

	public $uses = array('Admin.Customer', 'Admin.Sale', 'Admin.Payment');

	public function isAuthorized($user){
		$this->parent = 'reports';
		$this->module = 'admin';

		return true;
	}

	public function reports_by_customer(){
		if(!$this->checkPrivilege(1)) $this->notAuthorized();
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '', 'fa-list')
		);

		$customers = $this->Sale->find('all', array(
			'limit' => 20,
			'fields' => array(
				'Sale.created_date' => 'MAX(Sale.created_date)',
				'Sale.id', 'Customer.id', 'Customer.name', 'Sale.created_date',
				'Sale.total' => 'COUNT(Sale.id) AS total', 'Admin.name'
				),
			'group' => 'Customer.id',
			'order' => 'Customer.name ASC',
			'conditions' => array(
				'Sale.status_active' => 'active',
				'Customer.status_active' => 'active'
				)
			)
		);

		// debug($customers); die();
		
		$this->Paginator->settings = array(
			'limit' => 20,
			'fields' => array(
				'Sale.created_date' => 'MAX(Sale.created_date)',
				'Sale.id', 'Customer.id', 'Customer.name', 'Sale.created_date',
				'COUNT(Sale.id) AS total'
				),
			'group' => 'Customer.id',
			'order' => 'Customer.name ASC',
			'conditions' => array(
				'Sale.status_active' => 'active',
				'Customer.status_active' => 'active'
				)
		);

		$this->set('customers', $this->Paginator->paginate('Sale'));
	}

	private function renderIndexSortBy($sortBy) {
		$setSelectSort = "";

		$setSelectSort .= "<option selected=true> Urut Berdasarkan </option>";
		
		if($sortBy == 'created_date') {
			$setSelectSort .= "<option value='created_date' selected=true> Tanggal Dibuat </option>";
		}
		else {
			$setSelectSort .= "<option value='created_date'> Tanggal Dibuat </option>";
		}
		if($sortBy == 'name') {
			$setSelectSort .= "<option value='name' selected=true> Nama </option>";
		}
		else {
			$setSelectSort .= "<option value='name'> Nama </option>";
		}
		if($sortBy == 'phone') {
			$setSelectSort .= "<option value='phone' selected=true> No. HP </option>";	
		}
		else {
			$setSelectSort .= "<option value='phone'> No. HP </option>";
		}
		if($sortBy == 'email') {
			$setSelectSort .= "<option value='email' selected=true> Email </option>";
		}
		else {
			$setSelectSort .= "<option value='email'> Email </option>";
		}
		if($sortBy == 'city') {
			$setSelectSort .= "<option value='city' selected=true> Kota </option>";
		}
		else {
			$setSelectSort .= "<option value='city'> Kota </option>";
		}

		$this->set('sortData', $setSelectSort);
	}

	public function reports_by_customer_detail($id = array()) {
		$this->breadcrumbs = array(
			array('Dashboard', '/pos', 'fa-dashboard'),
			array('Laporan Pelanggan', '/pos/report/customers', 'si si-user'),
			array('Detail', '', 'fa-folder-open')
		);

		$id = $this->params['id'];
		if($id == null) $this->invalidReqeust();
		$from = '1970-01-01 00:00:00';
		$to = new DateTime();
		$to = $to->format('Y-m-d H:i:s');

		if($this->request->is('post')){
			$data = $this->request->data;
			$from = $this->myLibs->formatDate($data['from'] . " 00:00:00");
			$to = $this->myLibs->formatDate($data['to'] . " 23:59:59");
			$this->set(compact('from'));
			$this->set(compact('to'));
		}


		$customer = $this->Customer->findById($id);
		$sales = $this->Sale->find('all', array(
			'conditions' => array(
				'Sale.customer_id' => $customer['Customer']['id'],
				'Sale.created_date BETWEEN ? AND ?' => array($from, $to)
				),
			'order' => array(
				'Sale.created_date' => 'ASC'
				)
			));
		$sales_aggregate = $this->Sale->find('all', array(
			'fields' => array(
				'COUNT(Sale.id) as count',
				'SUM(Sale.total_nominal) as total',
				),
			'conditions' => array(
				'Sale.customer_id' => $customer['Customer']['id'],
				'Sale.created_date BETWEEN ? AND ?' => array($from, $to)
				)
			));

		$chart_sales = array();
		$chart_sales_category = array();
		$customer_sales_product_categories = $this->Customer->get_sales_by_product_category($id, $from, $to);

		foreach ($customer_sales_product_categories as $key => $category) {
			$color = $this->myLibs->getColor($key+20);
			$rgba = 'rgba('.$color[0].', '.$color[1].', '.$color[2].', 1)';
			$chart_sales_category[$key]['value'] = (int)$category[0]['tc'];
			$chart_sales_category[$key]['color'] = $rgba;
			$rgba = 'rgba('.$color[0].', '.$color[1].', '.$color[2].', .75)';
			$chart_sales_category[$key]['highlight'] = $rgba;
			$chart_sales_category[$key]['label'] = $category['pr']['category'];
		}
		$sales_aggregate_bar_chart_payment_type = $this->Payment->find('all', array(
			'fields' => array(
				'SUM(Payment.cash_nominal) as cash',
				'SUM(Payment.transfer_nominal) as transfer',
				'SUM(Payment.debit_nominal) as debit',
				),
			'conditions' => array(
				'Sale.customer_id' => $customer['Customer']['id'],
				'Sale.created_date BETWEEN ? AND ?' => array($from, $to)
				)
			));
		$bar_chart_data_payment_type = array();
		$bar_chart_data_payment_type[0]['label'] = '';
		$bar_chart_data_payment_type[0]['fillColor'] = 'rgba(171, 227, 125, .3)';
		$bar_chart_data_payment_type[0]['strokeColor'] = 'rgba(171, 227, 125, 1)';
		$bar_chart_data_payment_type[0]['pointColor'] = 'rgba(171, 227, 125, 1)';
		$bar_chart_data_payment_type[0]['pointStrokeColor'] = '#fff';
		$bar_chart_data_payment_type[0]['pointHighlightFill'] = '#fff';
		$bar_chart_data_payment_type[0]['pointHighlightStroke'] = 'rgba(171, 227, 125, 1)';
		$bar_chart_data_payment_type[0]['data'] = [$sales_aggregate_bar_chart_payment_type[0][0]['cash'], $sales_aggregate_bar_chart_payment_type[0][0]['debit'], $sales_aggregate_bar_chart_payment_type[0][0]['transfer']];

		$line_chart_pos_sales = array();
		$line_chart_pos_sales['labels'] = array();
		$line_chart_pos_sales['datasets'][0]['label'] = 'Penjualan';
		$line_chart_pos_sales['datasets'][0]['fillColor'] = 'rgba(171, 227, 125, .3)';
		$line_chart_pos_sales['datasets'][0]['strokeColor'] = 'rgba(171, 227, 125, 1)';
		$line_chart_pos_sales['datasets'][0]['pointColor'] = 'rgba(171, 227, 125, 1)';
		$line_chart_pos_sales['datasets'][0]['pointStrokeColor'] = '#fff';
		$line_chart_pos_sales['datasets'][0]['pointHighlightFill'] = '#fff';
		$line_chart_pos_sales['datasets'][0]['pointHighlightStroke'] = 'rgba(171, 227, 125, 1)';
		$line_chart_pos_sales['datasets'][0]['data'] = array();
		/* Chart Lines */
		foreach($sales as $key => $sale){
			$d = date_parse_from_format("Y-m-d", $sale['Sale']['created_date']);
			$label = $d['day'] . '-' . $d['month'];
			if(!in_array($label, $line_chart_pos_sales['labels'])){
				$line_chart_pos_sales['labels'][] = $label;
				$line_chart_pos_sales['datasets'][0]['data'][] = (int)$sale['Sale']['total_nominal'];
			}else{
				$idx = array_search($label,$line_chart_pos_sales['labels']);
				$line_chart_pos_sales['datasets'][0]['data'][$idx] += $sale['Sale']['total_nominal'];
			}
		}
		/* END */
		//debug(json_encode($bar_chart_data_payment_type));die;
		$this->set(compact('line_chart_pos_sales'));
		$this->set('bar_chart_data_payment_type', $bar_chart_data_payment_type);
		$this->set('pie_chart_data', $chart_sales_category);
		$this->set(compact('sales_aggregate'));
		$this->set(compact('sales'));
	}

	private function renderDetail($sales = array(), $validationErrors = array()) {
		$this->breadcrumbs = array(
			array('Dashboard', '/admin', 'fa-dashboard'),
			array('List', '/admin/reports/customers', 'fa-list'),
			array('Detail', '', 'fa-list')
			);

		$this->set(compact('sales'));
		$this->set(compact('validationErrors'));
	}
}
