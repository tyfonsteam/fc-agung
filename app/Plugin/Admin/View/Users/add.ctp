<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Admin <small>Create Admin</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>

<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
	<ul>
	<?php foreach($validationErrors as $key => $errors): ?>
		<?php foreach($errors as $error): ?>
			<li>
			<?php echo $error; ?>
			</li>
		<?php endforeach; ?>
		
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<div class='content'>
<div class="row">
	<div class="col-md-12">
		<form autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data">
			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Data Admin </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="username" name="data[Admin][username]" value="<?php echo empty($data) ? '': $data['Admin']['username']?>">
		                                <label for="username">Username</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<select class='form-control' name="data[Admin][status]" class="form-control">
											<option value="active"> Active </option>
											<option value="inactive"> Inactive </option>
										</select>
		                                <label for="status">Status</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="password" id="password" name="data[Admin][password]" value="<?php echo empty($data) ? '': $data['Admin']['password']?>">
		                                <label for="password">Password</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<input class="form-control" type="password" id="re-password" name="data[Admin][re_password]" value="<?php echo empty($data) ? '': $data['Admin']['re_password']?>">
		                                <label for="re-password">Ulangi Password</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="name" name="data[Admin][name]" value="<?php echo empty($data) ? '': $data['Admin']['name']?>">
		                                <label for="name">Nama</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<input class="form-control" type="text" id="phone" name="data[Admin][phone]" value="<?php echo empty($data) ? '': $data['Admin']['phone']?>">
		                                <label for="phone">No. Telp</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
                    	<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
			                    <div class="col-sm-12">
			                        <div class="form-material floating">
			                            <textarea id="address" name="data[Admin][address]" class="form-control"><?php echo isset($data['Admin']['address']) ? $data['Admin']['address'] : '' ;?></textarea>
			                            <label for="address">Alamat</label>
			                        </div>
			                    </div>
			                </div>
			            </div>
                    </div>
				</div>
			</div>
			
			<h3> Lists Privileges <a class="btn btn-default btn-sm" id="btn-check" data-state="check">Check All</a></h3>
			<hr/>
			<?php foreach($privileges as $key => $module): ?>
				<div class='block block-themed'>
					<div class='block-header bg-primary-dark'>
						<ul class='block-options'>
							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle">
									<i class="si si-arrow-up">
									</i>
								</button>
							</li>
						</ul>
						<h3 class='block-title'> <?php echo ucfirst($key); ?></h3>
					</div>
					<div class='block-content'>
					<?php foreach($module as $key2 => $parent): ?>
						
						<div class='block-header bg-success-light'>
						<ul class='block-options'>
							<li>
								<a class='btn btn-default btn-sm' id='btn-check-one-module' data-state='check' data-parent-name="<?php echo $key.$parent['name'];?>"> Check All</a>
							</li>
						</ul>
							<h3 class='block-title'><?php echo ucfirst($parent['name']); ?></h3>
						</div>
						
						<div id="<?php echo $key.$parent['name']?>" style="clear:both; overflow:hidden; padding-left:30px">
						<?php foreach($parent as $key3 => $privs): if(!is_array($privs)) continue; ?>

							<div class="col-md-3">
								<label class="css-input css-checkbox css-checkbox-success">
                                    <input type="checkbox" name="privs[<?php echo $key?>][<?php echo $parent['name']?>][]" class="privs-check" value="<?php echo $privs['Privilege']['code']?>"/><span></span> <?php echo $privs['Privilege']['name'];?>
                                </label>
							</div> 
						<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
					</div>
				</div>
			<?php endforeach; ?>
			
			<div class='block block-themed'>
				<div class='block-header bg-flat-dark'>
					<h3 class='block-title'> Confirm</h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class="col-md-12">
							<label> Are you sure ? </label>

							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check-circle-o"></i> Save </button>

							<a style="margin-right:10px;" href="<?php echo $this->Html->url('/admin/users');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>