<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Admin <small>List of Admin</small>
	        </h1>
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<div class="block">
<div class="block-header">
	<div class="row">
		<form role="form" class="form">
			<div class="col-md-4">
				<div class="input-group">
					<input name="query" type="text" class="form-control" placeholder="Search for...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
			</div>
		</form>

	</div>
	
	<?php if($searchData != null && $searchData['query'] != ''):?>
		<div class="row">
			<div class="col-md-12">
				<label class="label label-default">
					Search result : 
					<?php echo $searchData['query']; ?>
				</label>
			</div>
		</div>
	<?php endif; ?>
</div>

<div class='block-content'>
	
	<div class="row">
		<div class="col-lg-12 table-responsive">
			<table class="table table-striped table-bordered table-header-bg">
				<thead>
					<th class="text-center"> No </th>
					<th class="text-center"> Created Date </th>
					<th class="text-center"> Username </th>
					<th class="text-center"> Status </th>
					<th class="text-center"> Action </th>
				</thead>
				<tbody>
					<?php foreach($admins as $key => $admin): ?>
						<tr> 
							<td class="text-center"> <?php echo ($key + 1) ?> </td>
							<td class="text-center"> <?php echo $this->Presentation->dateFormat($admin['Admin']['created_date'], 'd F Y'); ?> </td>
							<td class="text-center"> <?php echo $admin['Admin']['username']; ?> </td>
							<td class="text-center"> 
								<label class="label label-<?php echo ($admin['Admin']['status'] == 'active')? 'success':'danger';?>">
									<?php echo ucfirst($admin['Admin']['status']); ?> 
								</label>
							</td>
							<td class="text-center">
								
								<a data-toggle="tooltip" title="Edit Admin" data-placement="left" href="<?php echo $this->Html->url('/admin/users/edit/'.$admin['Admin']['id']);?>" class="btn btn-primary btn-xs"> <i class="fa fa-pencil"></i> </a>
								

								
								<a data-toggle="tooltip" title="Delete Admin" data-placement="left" href="<?php echo $this->Html->url(array('plugin'=>'admin', 'controller'=>'Users', 'action'=>'delete', $admin['Admin']['id']));?>" class="btn btn-danger btn-xs btn-delete"> <i class="fa fa-trash"></i> </a>
								
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>

				
			</table>
			<?php if($this->Paginator->hasNext() || $this->Paginator->hasPrev()): ?>
			<div class="paginate pull-right">
				<ul class="pagination">
			<?php
				echo $this->Paginator->prev(__('<< Previous'), array('tag' => 'li', 'disabledTag'=>'a'),null, array('tag'=>'li','disabledTag'=>'a', 'class'=>'disabled'));
				echo $this->Paginator->numbers(array(
				    'separator' => '',
				    'currentClass' => 'active',
				    'currentTag' => 'a',
				    'tag' => 'li'
				));					
				echo $this->Paginator->next(__('Next >>'), array('tag' => 'li', 'disabledTag'=>'a'),null, array('tag'=>'li','disabledTag'=>'a', 'class'=>'disabled'));
			?>
				</ul>	
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
</div>
