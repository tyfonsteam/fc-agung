<!-- Page Heading -->
<div class='content bg-gray-lighter'>
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                User <small>Edit Profile</small>
            </h1>
        
        </div>
        <div class='col-sm-5 text-right hidden-xs'>
            <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
        </div>
    </div>
</div>


<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
    <ul>
    <?php foreach($validationErrors as $key => $errors): ?>
        <?php foreach($errors as $error): ?>
            <li>
            <?php echo $error; ?>
            </li>
        <?php endforeach; ?>
        
    <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>

<div class="content">

<form action="<?php echo $this->Html->url('/pos/users/profile'); ?>" method="POST" enctype="multipart/form-data">
		
	<div class="block block-themed">

	<div class="block-header bg-primary">
		<h3 class="block-title">User Profile</h3>
	</div>

	<div class="block-content">
		<div class='row items-push'>
			<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
				<div class="form-group">
                    <div class="col-sm-12">
                        <div class="form-material floating">
                            <input class="form-control" type="password" id="password" name="data[Admin][password]" value="">
                            <label for="password">Password Baru</label>
                        </div>
                        <label class="label label-danger">Biarkan kosong jika tidak ingin mengganti password!</label>
                    </div>
                </div>
            </div>
            <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="form-material floating">
                        	<input type="password" class="form-control" id="re-password" name="data[Admin][re_password]" value="">
                            <label for="re-password">Ulangi Password</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
        <div class="divider-hr"></div>
        <div class='row items-push'>
			<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
				<div class="form-group">
                    <div class="col-sm-12">
                        <div class="form-material floating">
                            <input class="form-control" type="text" id="username" name="data[Admin][username]" value="<?php echo isset($data['Admin']['username']) ? $data['Admin']['username'] : '';?>" readonly>
                            <label for="username">Username</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="form-material floating">
                        	<input type="text" class="form-control" id="name" name="data[Admin][name]" value="<?php echo isset($data['Admin']['name']) ? $data['Admin']['name'] : '';?>">
                            <label for="name">Nama</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
        <div class='row items-push'>
			<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
				<div class="form-group">
                    <div class="col-sm-12">
                        <div class="form-material floating">
                            <input class="form-control" type="text" id="phone" name="data[Admin][phone]" value="<?php echo isset($data['Admin']['phone']) ? $data['Admin']['phone'] : '';?>">
                            <label for="phone">No. Telp</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="form-material floating">
                        	<input type="text" class="form-control" id="email" name="data[Admin][email]" value="<?php echo isset($data['Admin']['email']) ? $data['Admin']['email'] : '';?>">
                            <label for="email">Email</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
        <div class='row items-push'>
			<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
				<div class="form-group">
                    <div class="col-sm-12">
                        <div class="form-material floating">
                            <textarea id="address" name="data[Admin][address]" class="form-control"><?php echo isset($data['Admin']['address']) ? $data['Admin']['address'] : '' ;?></textarea>
                            <label for="address">Alamat</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
                <div class="form-group">
                    <div class="col-sm-6">
                       <?php if(!empty($data['Admin']['photo'])) : ?>
                        <img class="img-responsive"  src="<?php echo Router::url('/').$data['Admin']['photo'];?>" alt="Avatar">
                        <?php else :?>
                        <img class="img-responsive" src="<?php echo Router::url('/').'pos/img/dummy-avatar.jpg';?>" alt="Avatar">
                        <?php endif;?>
                    </div>
                    <div class="col-sm-6">
                        <input type='file' name="photo"/>
                        <!-- <div id="dZUpload" class="dropzone">
						      <div class="dz-default dz-message"><span>Drop files here to upload new avatar of yours</span></div>
						</div> -->
                    </div>
                </div>
            </div>
        </div>	

        <div class="row items-push">
            <div class="col-md-12 col-lg-12">
                <button type="submit" class="btn btn-success btn-small btn-icon"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>
	</div>

	</div>


</form>

</div>