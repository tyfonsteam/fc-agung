<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <?php echo $this->Html->charset(); ?>

        <title><?php echo $title_for_layout; ?> - Tyfons Photocopy System</title>

        <meta name="description" content="Point Of Sales For Ti-e-nette Boutique">
        <meta name="author" content="tyfons">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <?php echo $this->Html->meta('favicon.ico', '/admin/favicon.png', array('type' => 'icon') );?>

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- OneUI CSS framework -->
        <?php echo $this->Html->css(array('/admin/css/oneui','/admin/css/styles.css'));?>

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <div class='fixed-top-notification'>
            <?php echo $this->Session->flash();?>
        </div>

        <?php echo $this->fetch('content'); ?>

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <?php echo $this->Html->script(array('/admin/js/core/jquery.min.js', '/admin/js/core/bootstrap.min.js', '/admin/js/core/jquery.slimscroll.min.js','/admin/js/core/jquery.scrollLock.min.js', '/admin/js/core/jquery.appear.min.js','/admin/js/core/jquery.countTo.min.js','/admin/js/core/jquery.placeholder.min.js', '/admin/js/core/js.cookie.min.js', '/admin/js/app.js')); ?>


        <!-- Page JS Plugins -->
        <?php echo $this->Html->script(array('/admin/js/plugins/jquery-validation/jquery.validate.min.js')); ?>
        <!-- <script src="assets/js/plugins/jquery-validation/jquery.validate.min.js"></script> -->

        <!-- Page JS Code -->
        <?php echo $this->Html->script(array('/admin/js/pages/base_pages_login.js')); ?>
        <!-- <script src="assets/js/pages/base_pages_login.js"></script> -->

        <script type="text/javascript">
            $(function(){
            if($(".fixed-top-notification").html().trim() != ""){
                    $(".fixed-top-notification .alert").slideDown(500);
                    $(".fixed-top-notification .alert").delay(2000).slideUp(500, function(e){
                        $(this).html("");
                    });   
            }
            $("[data-toggle='tooltip']").tooltip();     
          });
        </script>
    </body>
</html>