<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Fc Agung - Administration System</title>

        <meta name="description" content="Point Of Sales For Ti-e-nette Boutique">
        <meta name="author" content="tyfons">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <?php echo $this->Html->meta('favicon.ico', '/admin/favicon.png', array('type' => 'icon') );?>
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700"> -->

        <!-- Page JS Plugins CSS -->
        <?php echo $this->Html->css(array('/admin/css/plugins/select2/select2.min.css','/admin/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css','/admin/css/plugins/slick/slick.min.css', '/admin/css/plugins/slick/slick-theme.min.css', '/admin/css/plugins/dropzonejs/dropzone.min.css')); ?>

        <!-- OneUI CSS framework -->
       <?php echo $this->Html->css(array('/admin/css/oneui', '/admin/css/themes/flat.min.css','/admin/css/styles.css'));?>

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Error Content -->
        <div class="content bg-white text-center pulldown overflow-hidden">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <!-- Error Titles -->
                    <h1 class="font-s128 font-w300 text-modern animated zoomInDown">500</h1>
                    <h2 class="h3 font-w300 push-50 animated fadeInUp">We are sorry but our server encountered an internal error..</h2>
                    <!-- END Error Titles -->

                    <!-- Search Form -->
                    <form class="form-horizontal push-50" action="base_pages_search.html" method="admint">
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="input-group input-group-lg">
                                    <input class="form-control" type="text" placeholder="Search application..">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END Search Form -->
                </div>
            </div>
        </div>
        <!-- END Error Content -->

        <!-- Error Footer -->
        <div class="content pulldown text-muted text-center">
            Would you like to let us know about it?<br>
            <a class="link-effect" href="javascript:void(0)">Report it</a> or <a class="link-effect" href="<?php $this->Html->url('/admin'); ?>">Go Back to Dashboard</a>
        </div>
        <!-- END Error Footer -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <?php echo $this->Html->script(array('/admin/js/core/jquery.min.js', '/admin/js/core/bootstrap.min.js', '/admin/js/moment.min.js','/admin/js/core/jquery.slimscroll.min.js','/admin/js/core/jquery.scrollLock.min.js', '/admin/js/plugins/dropzonejs/dropzone.min.js','/admin/js/core/jquery.appear.min.js','/admin/js/core/jquery.countTo.min.js','/admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js','/admin/js/core/jquery.placeholder.min.js', '/admin/js/core/js.cookie.min.js', '/admin/js/app.js','/admin/js/bootbox.js','/admin/js/custom.js')); ?>
    </body>
</html>