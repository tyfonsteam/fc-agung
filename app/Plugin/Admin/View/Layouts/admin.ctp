<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <?php echo $this->Html->charset(); ?>

        <title><?php echo $title_for_layout; ?> - Fotocopy Agung</title>

        <meta name="description" content="Point Of Sales For Ti-e-nette Boutique">
        <meta name="author" content="tyfons">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <?php echo $this->Html->meta('favicon.ico', '/admin/favicon.png', array('type' => 'icon') );?>
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Page JS Plugins CSS -->
        <?php echo $this->Html->css(array('/admin/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css','/admin/css/plugins/select2/select2.min.css','/admin/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css','/admin/css/plugins/slick/slick.min.css', '/admin/css/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css','/admin/css/plugins/slick/slick-theme.min.css', '/admin/css/plugins/dropzonejs/dropzone.min.css','/admin/css/plugins/datatables/jquery.dataTables.min.css', '/admin/css/custom.css', '/admin/js/plugins/magnific-popup/magnific-popup.min.css')); ?>

        <!-- OneUI CSS framework -->
       <?php echo $this->Html->css(array('/admin/css/oneui', '/admin/css/styles.css'));?>

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <div class="fixed-top-notification">
          <?php echo $this->Session->flash();?>
        </div>
            <?php
            $show_sidebar = 'sidebar-o';
            $show_cashier =false;
            if(in_array(2, $userPrivParent['dashboard'])){
                $show_sidebar = '';
                $show_cashier =true;
            }
            ?>
        <div id="page-container" class="sidebar-l <?php echo $show_sidebar; ?> side-scroll header-navbar-fixed">
            <?php if(!$show_cashier): ?>
                <?php echo $this->element('sidebar'); ?>
            <?php endif ?>
            <?php echo $this->element('header');?>
            <!-- Main Container -->
            <main id="main-container">
                <?php echo $this->fetch('content');?>
            </main>
            <!-- END Main Container -->

            <?php echo $this->element('footer');?>
        </div>
        <!-- END Page Container -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <?php echo $this->Html->script(array('/admin/js/core/jquery.min.js', '/admin/js/core/bootstrap.min.js', '/admin/js/moment.min.js','/admin/js/core/jquery.slimscroll.min.js','/admin/js/core/jquery.scrollLock.min.js', '/admin/js/plugins/dropzonejs/dropzone.min.js','/admin/js/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js','/admin/js/plugins/select2/select2.full.min.js','/admin/js/plugins/masked-inputs/jquery.maskedinput.min.js','/admin/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js','/admin/js/core/jquery.appear.min.js','/admin/js/core/jquery.countTo.min.js','/admin/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js','/admin/js/plugins/flot/jquery.flot.min.js','/admin/js/core/jquery.placeholder.min.js', '/admin/js/core/js.cookie.min.js', '/admin/js/app.js','/admin/js/bootbox.js','/admin/js/custom.js', '/admin/js/plugins/magnific-popup/magnific-popup.min.js', '/admin/js/plugins/bootstrap-typeahead/bootstrap-typeahead.js')); ?>

        <?php echo $this->Html->script(array('/admin/js/plugins/chartjs/Chart.min.js'));?>

        <!-- Validation JS Code -->
        <?php echo $this->Html->script(array('/admin/js/plugins/jquery-validation/jquery.validate.min.js')); ?>
        <?php if($this->params['controller'] == 'Users'): ?>   
            <?php if($this->params['action'] == 'add'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Users/add.js');?>
            <?php endif; ?>
            <?php if($this->params['action'] == 'edit'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Users/edit.js');?>
            <?php endif; ?>
        <?php endif;?>

        <?php if($this->params['controller'] == 'Products'): ?>   
            <?php if($this->params['action'] == 'add'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Products/add.js');?>
            <?php endif; ?>
            <?php if($this->params['action'] == 'edit'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Products/edit.js');?>
            <?php endif; ?>
        <?php endif;?>
        
        <?php if($this->params['controller'] == 'Customers'): ?>   
            <?php if($this->params['action'] == 'add'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Customers/add.js');?>
            <?php endif; ?>
            <?php if($this->params['action'] == 'edit'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Customers/edit.js');?>
            <?php endif; ?>
        <?php endif;?>

        <?php if($this->params['controller'] == 'Employees'): ?>   
            <?php if($this->params['action'] == 'add'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Employees/add.js');?>
            <?php endif; ?>
            <?php if($this->params['action'] == 'edit'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Employees/edit.js');?>
            <?php endif; ?>
        <?php endif;?>

        <?php if($this->params['controller'] == 'Divisions'): ?>   
            <?php if($this->params['action'] == 'add'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Divisions/add.js');?>
            <?php endif; ?>
            <?php if($this->params['action'] == 'edit'): ?>
                <?php echo $this->Html->script('/admin/js/validations/Divisions/edit.js');?>
            <?php endif; ?>
        <?php endif;?>

        <?php if($this->params['controller'] == 'Dashboards'):?>
        <!-- Page Plugins -->
        <?php echo $this->Html->script(array('/admin/css/plugins/slick/slick.min.js','/admin/js/plugins/chartjs/Chart.min.js')); ?>

        <!-- Page JS Code -->
        <?php echo $this->Html->script(array('/admin/js/pages/base_comp_animations.js'));?>
        

        <script>
            $(function () {
                // Init page helpers (Slick Slider plugin)
                App.initHelpers('slick');

            });
            
        </script>
        <?php endif; ?>

        <?php if($this->params['controller'] == 'Reports' && ($this->params['action'] == 'sales' || $this->params['action'] == 'customer_details')): ?>
            <?php echo $this->Html->script(array('/admin/js/plugins/datatables/jquery.dataTables.min.js'));?>
            <?php echo $this->Html->script(array('/admin/js/pages/admin_report/report.js'));?>
            <?php echo $this->Html->script(array('/admin/js/plugins/chartjs/Chart.min.js'));?>
        <?php endif; ?>

        <script>
            $(function(e){
                if($(".fixed-top-notification").html().trim() != ""){
                        $(".fixed-top-notification .alert").slideDown(500);
                        $(".fixed-top-notification .alert").delay(2000).slideUp(500, function(e){
                        $(this).html("");
                    });   
                }
                $("[data-toggle='tooltip']").tooltip();
                App.initHelpers(['slick', 'masked-inputs','datepicker', 'select2', 'colorpicker', 'magnific-popup']);
                $(document).on('click', '.btn-delete', function(e){
                    e.preventDefault();
                    showDeleteConfirmationDialog($(this).data('msg'), $(this).attr('href'));
                });

                <?php
                if($this->params['controller'] == 'Privileges'){
                    echo $this->element('Users/privileges_js');
                }
                if($this->params['controller'] == 'Users'){
                    echo $this->element('Users/profile_js');
                    echo $this->element('Users/crud_users_js');
                }
                if($this->params['controller'] == 'Sales'){
                    if ($this->params['action']=='add') {
                        echo $this->element('js-product-picker');
                        echo $this->element('js-payment');
                        echo $this->element('Sales/js-sales-add');    
                    } else  if ($this->params['action']=='edit') {
                        echo $this->element('js-product-picker');
                        echo $this->element('js-payment');
                        echo $this->element('Sales/js-sales-edit');    
                    } else  if ($this->params['action']=='payoff') {
                        echo $this->element('js-product-picker');
                        echo $this->element('js-payment');
                        echo $this->element('Sales/js-sales-payoff');    
                    }
                }

                if($this->params['controller'] == 'Customers'){
                    if ($this->params['action']=='add') {
                        echo $this->element('Customers/js-customer-add');    
                    } else  if ($this->params['action']=='edit') {
                        echo $this->element('Customers/js-customer-edit');    
                    }
                }

                if($this->params['controller'] == 'Employees'){
                    if ($this->params['action']=='add') {
                        echo $this->element('Employees/js-employee-add');    
                    } else  if ($this->params['action']=='edit') {
                        echo $this->element('Employees/js-employee-edit');    
                    }
                }

                if($this->params['controller'] == 'Divisions'){
                    if ($this->params['action']=='add') {
                        echo $this->element('Divisions/js-division-add');    
                    } else  if ($this->params['action']=='edit') {
                        echo $this->element('Divisions/js-division-edit');    
                    }
                }

                if($this->params['controller'] == 'TransactionSessions'){
                    if ($this->params['action']=='open') {
                        echo $this->element('TransactionSessions/js-transaction-sessions-open');    
                    } else if ($this->params['action']=='close') {
                        echo $this->element('TransactionSessions/js-transaction-sessions-close');    
                    } 
                }

                if($this->params['controller'] == 'Reports'){
                    echo $this->element('Reports/Customers/js_chart_customer');
                }
                ?> 
            });
        </script>
    </body>
</html>