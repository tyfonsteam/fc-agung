<div class='block block-themed'>
	<div class="block-header bg-primary">
		<div class = 'row'>
			<div class = 'col-lg-6'>
    			<h3 class="block-title"> Informasi Pembayaran </h3>
    		</div>
    		<div class = 'col-lg-6 text-right'>
                <h3 class="block-title"> Total : <span id="total-payment-label"><?php echo (isset($data['Payment']['total_nominal'])&& !empty($data['Payment']['total_nominal'])) ? $this->Number->currency( $data['Payment']['total_nominal'], $currency)  : 'Rp. 0'; ?></span> </h3>
    		
				<input type="hidden" id="total-payment" value = '<?php echo (isset($data['Payment']['total_nominal'])&& !empty($data['Payment']['total_nominal'])) ? $data['Payment']['total_nominal']  : '0'; ?>' name="data[Payment][total_nominal]">
                <input type="hidden" id="remaining-payment" value = '<?php echo (isset($remaining_payment)&& !empty($remaining_payment)) ? $remaining_payment  : '0'; ?>'>
    		</div>
		</div>						
	</div>
    
	<div class="block-content">
		<div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class = 'row'>
                            <div class = 'col-lg-12'>
                                <div style = 'display:none' id = 'alert-payment-error' class="alert alert-warning">
                                    <a id = 'btn-close-alert-payment-error' href = 'javascript:void(0)' id = '' type="button" class="close" >×</a>
                                    <i class="fa fa-warning"></i> Error <p id = 'p-alert-payment-error'></p>
                                </div>
                            </div>
                        </div>
                    	<div class = 'row'>
                    		<div class="col-xs-4">
                    			<div class = 'col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                    				<div class="form-group">
                                        <?php 
                                        $dp = '';
                                        $paid_off = 'checked';
                                        $receivable = '';
                                        if (isset($data['Payment']['payment_purpose']) && !empty($data['Payment']['payment_purpose'])) {
                                            switch ($data['Payment']['payment_purpose']) {
                                                case 'dp':
                                                    $dp = 'checked';
                                                    break;
                                                
                                                case 'paid_off':
                                                   $paid_off = 'checked';
                                                    break;

                                                case 'receivable':
                                                   $receivable = 'checked';
                                                    break;
                                            }
                                        } ?>
                                        <label  style = '<?php echo (isset($payment_type['paid_off']) && !empty($payment_type['paid_off']) && $payment_type['paid_off'] == true ) ? '' : 'display:none' ?>' class="css-input css-radio css-radio-success css-radio-lg push-10-r">
                                            <input id = 'input-check-payment-paid-off' type="radio" value = 'paid_off' name="data[Payment][payment_purpose]" <?php echo $paid_off; ?>><span></span> Lunas
                                        </label>
                                        <label style = '<?php echo (isset($payment_type['dp']) && !empty($payment_type['dp']) && $payment_type['dp'] == true ) ? '' : 'display:none' ?>' class="css-input css-radio css-radio-success css-radio-lg push-10-r">
                                            <input  id = 'input-check-payment-dp' type="radio" value = 'dp' name="data[Payment][payment_purpose]" <?php echo $dp; ?>><span></span> DP
                                        </label>
                                         <label style = '<?php echo (isset($payment_type['receivable']) && !empty($payment_type['receivable']) && $payment_type['receivable'] == true ) ? '' : 'display:none' ?>' id = 'input-check-payment-receivable' class="css-input css-radio css-radio-success css-radio-lg">
                                            <input type="radio" value = 'receivable' name="data[Payment][payment_purpose]" <?php echo $receivable; ?>><span></span> Cicilan
                                        </label>
                                    </div>
                    			</div>
                    			
                            </div>
                            <div class="col-xs-4">
                				<div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div  class="form-material form-material-primary">
                                            <input id = 'input-payment-date' class="js-datepicker form-control" type="text"  name="data[Payment][payment_date]" value = '<?php echo (isset($data['Payment']['payment_date'])&& !empty($data['Payment']['payment_date'])) ? date('m/d/Y' , strtotime($data['Payment']['payment_date']))  : date('m/d/Y'); ?>'>
                                            <label >Tanggal Pembayaran</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    	<div class = 'row'>
                    		
                    		<div class = 'col-lg-4 '>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                 	<p class="content-mini content-mini-full bg-success text-white">Pembayaran Tunai</p>
                                </div>
                    			<div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-material form-material-primary">
                                            <input id = 'input-payment-cash-nominal' class="numericText form-control" type="text"  placeholder="0" name="data[Payment][cash_nominal]" value = '<?php echo (isset($data['Payment']['cash_nominal'])&& !empty($data['Payment']['cash_nominal'])) ? $data['Payment']['cash_nominal']  : ''; ?>'>
                                            <label >Nominal Tunai</label>
                                        </div>
                                    </div>
                                </div>
                    		</div>
                    		<div class = 'col-lg-4 '>
                    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <p class="content-mini content-mini-full bg-success text-white">Pembayaran Transfer</p>
                                </div>
                    			<div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-material form-material-primary">
                                            <input id = 'input-payment-transfer-nominal' class="numericText form-control" type="text"  placeholder="0" name="data[Payment][transfer_nominal]" value = '<?php echo (isset($data['Payment']['transfer_nominal'])&& !empty($data['Payment']['transfer_nominal'])) ? $data['Payment']['transfer_nominal']  : ''; ?>'>
                                            <label >Nominal Transfer</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-material form-material-primary">
                                            <input id = 'input-payment-transfer-bank-name' class="form-control" type="text"  placeholder="0" name="data[Payment][transfer_bank_name]" value = '<?php echo (isset($data['Payment']['transfer_bank_name'])&& !empty($data['Payment']['transfer_bank_name'])) ? $data['Payment']['transfer_bank_name']  : ''; ?>'>
                                            <label >Nama Bank Transfer</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-material form-material-primary input-group">
                                             <input id = 'input-payment-transfer-name' class="form-control" type="text"  placeholder="Masukkan nama transfer.." name="data[Payment][transfer_name]" value = '<?php echo (isset($data['Payment']['transfer_name'])&& !empty($data['Payment']['transfer_name'])) ? $data['Payment']['transfer_name']  : ''; ?>'>
                                            <label>Nama Transfer</label>
                                            <span class="input-group-addon"><i class="si si-user"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-material form-material-primary input-group">
                                             <input id = 'input-payment-transfer-bank-account' class="form-control" type="text"  placeholder="Masukkan nomor rekening.." name="data[Payment][transfer_bank_account]" value = '<?php echo (isset($data['Payment']['transfer_bank_account'])&& !empty($data['Payment']['transfer_bank_account'])) ? $data['Payment']['transfer_bank_account']  : ''; ?>'>
                                            <label>Nomor Rekening</label>
                                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                        </div>
                                    </div>
                                </div>
                    		</div>
                            <div class = 'col-lg-4 '>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <p class="content-mini content-mini-full bg-success text-white">Pembayaran Kartu Debet</p>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-material form-material-primary">
                                            <input id = 'input-payment-debit-nominal' class="numericText form-control" type="text"  placeholder="0" name="data[Payment][debit_nominal]" value = '<?php echo (isset($data['Payment']['debit_nominal'])&& !empty($data['Payment']['debit_nominal'])) ? $data['Payment']['debit_nominal']  : ''; ?>'>
                                            <label >Nominal Debet</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-material form-material-primary">
                                            <input id = 'input-payment-debit-bank-name' class="form-control" type="text"  placeholder="0" name="data[Payment][debit_bank_name]" value = '<?php echo (isset($data['Payment']['debit_bank_name'])&& !empty($data['Payment']['debit_bank_name'])) ? $data['Payment']['debit_bank_name']  : ''; ?>'>
                                            <label >Nama Bank Debet</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-material form-material-primary input-group">
                                             <input id = 'input-payment-debit-bank-account' class="form-control" type="text"  placeholder="Masukkan nomor kartu.." name="data[Payment][dabit_bank_account]" value = '<?php echo (isset($data['Payment']['dabit_bank_account'])&& !empty($data['Payment']['dabit_bank_account'])) ? $data['Payment']['dabit_bank_account']  : ''; ?>'>
                                            <label>Nomor Kartu Debet</label>
                                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    	</div>
                        
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>