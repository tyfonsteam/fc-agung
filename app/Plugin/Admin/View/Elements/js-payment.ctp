function updateRemainingPayment(nominal){
	$('#remaining-payment').val(nominal);
}

function isPaymentNominalCorrect(){
	pass = true;
	var error_html = '';
	var total_payment = 0;
	if($('#input-check-payment-paid-off').is(':checked')){
		var remaining_payment = parseInt($('#remaining-payment').val());
		var total_payment = parseInt($('#total-payment').val());
		if(total_payment < remaining_payment){
			error_html += "- Nominal pelunasan kurang. <br/>";
			pass = false;
		}
	} else if($('#input-check-payment-dp').is(':checked')){
		var remaining_payment = parseInt($('#remaining-payment').val());
		var total_payment = parseInt($('#total-payment').val());
		if(total_payment >= remaining_payment){
			error_html += "- Nominal uang muka terlalu banyak. <br/>";
			pass = false;
		}
	}

	if(!pass){
		$('#p-alert-payment-error').html(error_html);
		$('#alert-payment-error').slideDown();	
	} 
	return pass;
}

function isPaymentFormValid(){
	$('#p-alert-payment-error').html('');
	$('#alert-payment-error').hide();
	pass = true;
	var error_html = '';
	var total_payment = 0;

	if($('#input-payment-date').val()==''){
		error_html += "- Tanggal pembayaran belum diisi. <br/>";
		pass = false;
	} 

	if($('#input-payment-cash-nominal').val()=='' && $('#input-payment-transfer-nominal').val()=='' && $('#input-payment-debit-nominal').val() == ''){
		error_html += "- Nominal pembayaran belum diisi. <br/>";
		pass = false;
	} else {
		total_payment += ($('#input-payment-cash-nominal').val()=='') ? 0 : parseInt($('#input-payment-cash-nominal').val());
		total_payment += ($('#input-payment-transfer-nominal').val()=='') ? 0 : parseInt($('#input-payment-transfer-nominal').val());
		total_payment += ($('#input-payment-debit-nominal').val()=='') ? 0 : parseInt($('#input-payment-debit-nominal').val());
	}

	if($('#input-payment-cash-nominal').val() < 1 && $('#input-payment-cash-nominal').val()!= ''){
		error_html += "- Nominal pembayaran tidak valid. <br/>";
		pass = false;
	} else if($('#input-payment-debit-nominal').val() < 1 && $('#input-payment-debit-nominal').val()!= ''){
		error_html += "- Nominal pembayaran tidak valid. <br/>";
		pass = false;
	} else if($('#input-payment-transfer-nominal').val() < 1 && $('#input-payment-transfer-nominal').val()!= ''){
		error_html += "- Nominal pembayaran tidak valid. <br/>";
		pass = false;
	}

	if($('#input-payment-transfer-nominal').val()!= ''){
		if($('#input-payment-transfer-bank-name').val() == ''){

			error_html += "- Nama bank transfer belum dipilih. <br/>";
			pass = false;
		}
		if($('#input-payment-transfer-name').val() == ''){

			error_html += "- Nama transfer uang tidak boleh kosong. <br/>";
			pass = false;
		}
		if($('#input-payment-transfer-bank-account').val() == ''){

			error_html += "- Nomor rekening transfer tidak boleh kosong. <br/>";
			pass = false;
		}
	}

	if($('#input-payment-debit-nominal').val()!= ''){
		if($('#input-payment-debit-bank-name').val() == ''){

			error_html += "- Nama bank debet belum dipilih. <br/>";
			pass = false;
		}
		if($('#input-payment-debit-bank-account').val() == ''){

			error_html += "- Nomor kartu debet tidak boleh kosong. <br/>";
			pass = false;
		}
	}

	if(!pass){
		$('#p-alert-payment-error').html(error_html);
		$('#alert-payment-error').slideDown();	
	} 
	return pass;
}

$('#input-payment-cash-nominal').on('input',function(e){
	var transfer_nominal = 0;
	var cash_nominal = 0;
	if($(this).val() != ''){
		cash_nominal = $(this).val();
	}
	
	if($('#input-payment-transfer-nominal').val() != ''){
		transfer_nominal = $('#input-payment-transfer-nominal').val();
	}
		
	var total_nominal = parseInt(transfer_nominal) + parseInt(cash_nominal);

	$('#total-payment').val(total_nominal);
	$('#total-payment-label').html("Rp " + parseInt(total_nominal).formatMoney(0,".",","));
});

$('#input-payment-transfer-nominal').on('input',function(e){
	var transfer_nominal = 0;
	var cash_nominal = 0;
	if($(this).val() != ''){
		transfer_nominal = $(this).val();
	}
	
	if($('#input-payment-cash-nominal').val() != ''){
		cash_nominal = $('#input-payment-cash-nominal').val();
	}
		
	var total_nominal = parseInt(transfer_nominal) + parseInt(cash_nominal);

	$('#total-payment').val(total_nominal);
	$('#total-payment-label').html("Rp " + parseInt(total_nominal).formatMoney(0,".",","));
});



$('#btn-close-alert-payment-error').on('click',function(e){
	$('#alert-payment-error').slideUp();	
})
