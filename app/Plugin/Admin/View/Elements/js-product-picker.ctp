function getProductTotalNominal(){
	return $('#total-price').val();
}

function addTotalSale(nominal){
	var current_total = $('#total-price').val();
	var after_add_total = parseInt(current_total)+parseInt(nominal);
	$('#total-price').val(after_add_total);
	$('#total-price-label').html("Rp " + parseInt(after_add_total).formatMoney(0,".",","));
}

function substractTotalSale(nominal){
	var current_total = $('#total-price').val();
	var after_substract_total = parseInt(current_total)-parseInt(nominal);
	$('#total-price').val(after_substract_total);
	$('#total-price-label').html("Rp " + parseInt(after_substract_total).formatMoney(0,".",","));
}

function isSaleDetailFormValid(){
	$('#p-alert-sale-detail-error').html('');
	$('#alert-sale-detail-error').hide();
	var pass = true;

	if($('#input-sales-detail-product-id').val() == ''){
		html = '- Nama produk tidak boleh kosong.<br/>';
		$('#p-alert-sale-detail-error').append(html);
		pass = false;
	}
	if($('#input-sales-detail-product-division-name').val() == ''){
		html = '- Nama divisi tidak boleh kosong.<br/>';
		$('#p-alert-sale-detail-error').append(html);
		pass = false;
	}
	if($('#input-sales-detail-product-qty').val() == ''){
		html = '- Jumlah produk tidak boleh kosong.<br/>';
		$('#p-alert-sale-detail-error').append(html);
		pass = false;
	}
	if($('#input-sales-detail-product-unit-price').val() == ''){
		html = '- Harga produk tidak boleh kosong.<br/>';
		$('#p-alert-sale-detail-error').append(html);
		pass = false;
	}
	return pass;
}

function resetSaleDetailForm(){
	//KOSONGKAN FORM
	$('#input-sales-detail-product-qty').val('');
	$('#input-sales-detail-product-unit-price').val('');
	$('#input-sales-detail-product-division-name').select2('val','');
	$('#input-sales-detail-product-id').select2('val','');
	$('#input-sales-detail-product-id option:selected').attr('data-product-name','');
	$('#alert-table-product-error').hide();
}

$('#btn-add-sales-detail').on('click',function(e){
	$('#alert-table-product-error').hide();
	if(!isSaleDetailFormValid()){
		$('#alert-sale-detail-error').slideDown();
		$('html, body').animate({
	        scrollTop: $("#anchor-error-sale-detail-form").offset().top-100
	    }, 200);
		return;
	}

	var qty = $('#input-sales-detail-product-qty').val();
	var unit_price = $('#input-sales-detail-product-unit-price').val();
	var product_division_name = $('#input-sales-detail-product-division-name').val();
	var product_id = $('#input-sales-detail-product-id').val();
	var product_name = $('#input-sales-detail-product-id option:selected').attr('data-product-name');
	var total_price = parseInt(qty)*parseInt(unit_price);
	var division_id = $('#input-sales-detail-product-division-name option:selected').attr('data-division-id');
	var employee_id = $('#input-sales-detail-product-division-name option:selected').attr('data-employee-id');

	if($('#sales-detail-list tr').length == 0){
		sales_detail_index = 0;
	} else {
		sales_detail_index = $('#sales-detail-list tr:last-child').data('sales-detail-index');
		sales_detail_index = parseInt(sales_detail_index) + 1;
	}

	var row_number = $('#sales-detail-list tr').length + 1;

	html = '';
	html += "<tr data-sales-detail-index = '"+sales_detail_index+"'>";
	html += "<input class = 'input-tr-sale-detail-product-id' type = 'hidden' name = 'data[SaleItem]["+sales_detail_index+"][Product][id]' value = '"+product_id+"' />";
	html += "<input class = 'input-tr-sale-detail-division-id' type = 'hidden' name = 'data[SaleItem]["+sales_detail_index+"][Division][id]' value = '"+division_id+"' />";
	html += "<input class = 'input-tr-sale-detail-employee-id' type = 'hidden' name = 'data[SaleItem]["+sales_detail_index+"][Employee][id]' value = '"+employee_id+"' />";
	html += "<input class = 'input-tr-sale-detail-product-name' type = 'hidden' name = 'data[SaleItem]["+sales_detail_index+"][Product][name]' value = '"+product_name+"' />";
	html += "<input class = 'input-tr-sale-detail-product-division-name' type = 'hidden' name = 'data[SaleItem]["+sales_detail_index+"][Division][name]' value = '"+product_division_name+"' />";
	html += "<input class = 'input-tr-sale-detail-unit-price' type = 'hidden' name = 'data[SaleItem]["+sales_detail_index+"][unit_price]' value = '"+unit_price+"' />";
	html += "<input class = 'input-tr-sale-detail-qty' type = 'hidden' name = 'data[SaleItem]["+sales_detail_index+"][qty]' value = '"+qty+"' />";
	html += "<input class = 'input-tr-sale-detail-total-price' type = 'hidden' name = 'data[SaleItem]["+sales_detail_index+"][total_price]' value = '"+total_price+"' />";
	html += "<td class = 'row-number text-center' data-row-number = '"+row_number+"'>"+row_number+"</td>";
	html += "<td class = 'text-center td-product-name'  >"+product_name+"</td>";
	html += "<td class = 'text-center td-product-division-name'  >"+product_division_name+"</td>";
	html += "<td class = 'text-center td-qty'  >"+qty+"</td>";
	html += "<td class = 'text-center td-unit-price'  >"+"Rp " + parseInt(unit_price).formatMoney(0,".",",")+"</td>";
	html += "<td class = 'text-center td-total-price'  >"+"Rp " + parseInt(total_price).formatMoney(0,".",",")+"</td>";
	html += "<td class = 'text-center'  >";
	html += "<a style = 'margin-right : 0.25em;' href='javascript:void(0)' class = 'btn btn-sale-detail-edit btn-primary btn-xs'><i class = 'fa fa-pencil'></i></a>";
	html += "<a href='javascript:void(0)' class = 'btn btn-sale-detail-delete btn-danger btn-xs'><i class = 'fa fa-trash'></i></a>";
	html += "</td>";

	html += "</tr>";
	$('#sales-detail-list').append(html);
	addTotalSale(total_price);
	resetSaleDetailForm();
	
});

$('#btn-reset-sales-detail').on('click',function(e){
	resetSaleDetailForm();
})

$('#btn-close-alert-sale-detail-error').on('click',function(e){
	$('#alert-sale-detail-error').slideUp();
})


$('body').on('click','.btn-sale-detail-delete',function(e){
	
	resetSaleDetailForm();
	var tr_element = $(this).closest('tr');
	$('#sales-detail-list tr.edited').removeClass('edited');
	$('#sales-detail-list tr.edited').removeClass('danger');
	var before_edit_total_price = tr_element.find('.input-tr-sale-detail-total-price').val();
	substractTotalSale(before_edit_total_price);
	tr_element.remove();
	//UPDATE PENOMORAN TABEL

	var row_number = 1;
	$.each($('#sales-detail-list tr'),function(idx){
		$(this).find('td.row-number').html(row_number);
		$(this).find('td.row-number').attr('data-row-number',row_number);
		row_number++;
	});

	$('#btn-save-edit-sales-detail').hide();
	$('#btn-add-sales-detail').show();

});

//EDIT ITEM

$('body').on('click','.btn-sale-detail-edit',function(e){
	$('#alert-table-product-error').hide();
	$('#alert-sale-detail-error').hide();
	var tr_element = $(this).closest('tr');
	tr_element.addClass('danger');
	tr_element.addClass('edited');

	var edited_product_id = tr_element.find('.input-tr-sale-detail-product-id').val();
	var edited_product_name = tr_element.find('.input-tr-sale-detail-product-name').val();
	var edited_qty = tr_element.find('.input-tr-sale-detail-qty').val();
	var edited_division_name = tr_element.find('.input-tr-sale-detail-product-division-name').val();
	var edited_unit_price = tr_element.find('.input-tr-sale-detail-unit-price').val();

	$('#input-sales-detail-product-qty').val(edited_qty);
	$('#input-sales-detail-product-unit-price').val(edited_unit_price);
	$('#input-sales-detail-product-division-name').select2('val',edited_division_name);
	$('#input-sales-detail-product-id').select2('val',edited_product_id);
	$('#input-sales-detail-product-id option:selected').attr('data-product-name',edited_product_name);

	$('#btn-save-edit-sales-detail').show();
	$('#btn-add-sales-detail').hide();

	$('html, body').animate({
        scrollTop: $("#anchor-error-sale-detail-form").offset().top-100
    }, 200);
	return;
});

$('#btn-save-edit-sales-detail').on('click',function(e){
	if(!isSaleDetailFormValid()){
		$('#alert-sale-detail-error').slideDown();
		$('html, body').animate({
	        scrollTop: $("#anchor-error-sale-detail-form").offset().top-100
	    }, 200);
		return;
	}
	var tr_element = $('#sales-detail-list tr.edited');
	var before_edit_total_price = tr_element.find('.input-tr-sale-detail-total-price').val();
	substractTotalSale(before_edit_total_price);

	var qty = $('#input-sales-detail-product-qty').val();
	var unit_price = $('#input-sales-detail-product-unit-price').val();
	var product_division_name = $('#input-sales-detail-product-division-name').val();
	var product_id = $('#input-sales-detail-product-id').val();
	var product_name = $('#input-sales-detail-product-id option:selected').attr('data-product-name');
	var total_price = parseInt(qty)*parseInt(unit_price);
	var product_division_id = $('#input-sales-detail-product-division-name  option:selected').attr('data-division-id');
	var product_employee_id = $('#input-sales-detail-product-division-name  option:selected').attr('data-employee-id');

	tr_element.find('.input-tr-sale-detail-product-id').val(product_id);
	tr_element.find('.input-tr-sale-detail-product-name').val(product_name);
	tr_element.find('.input-tr-sale-detail-qty').val(qty);
	tr_element.find('.input-tr-sale-detail-product-division-name').val(product_division_name);
	tr_element.find('.input-tr-sale-detail-product-employee-id').val(product_employee_id);
	tr_element.find('.input-tr-sale-detail-product-division-id').val(product_division_id);
	tr_element.find('.input-tr-sale-detail-unit-price').val(unit_price);
	tr_element.find('.input-tr-sale-detail-total-price').val(total_price);

	tr_element.find('.td-product-name').html(product_name);
	tr_element.find('.td-qty').html(qty);
	tr_element.find('.td-product-division-name').html(product_division_name);
	tr_element.find('.td-unit-price').html("Rp " + parseInt(unit_price).formatMoney(0,".",","));
	tr_element.find('.td-total-price').html("Rp " + parseInt(total_price).formatMoney(0,".",","));

	tr_element.removeClass('edited');
	tr_element.removeClass('danger');

	addTotalSale(total_price);

	resetSaleDetailForm();
	$('#btn-save-edit-sales-detail').hide();
	$('#btn-add-sales-detail').show();
});

//CEK KALAU PRODUCT PICKER KOSONG / ADA ISINYA

function isExistSaleDetail(){
	return $('#sales-detail-list tr').length;
}

$('#btn-close-alert-table-product-error').on('click',function(e){
	$('#alert-table-product-error').slideUp();
});