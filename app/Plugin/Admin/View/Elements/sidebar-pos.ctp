<div class='col-lg-3 col-md-4' style="background-color:white; position: fixed;  top: 60px;  bottom: 0; overflow-y: auto;">
	<div class="row" style="margin-top:20px;">	
		<div class="col-md-12 font-w600">
			<div id="list-items-table">
				<div style="max-height:20em; overflow:auto">
					<table class="table-pos" id="products-table">

					</table>
				</div>
				<p class="text-right" style="padding-top:10px; border-top:1px solid black; padding-right:10px;padding-bottom:10px">Total: Rp. <span id="total-text">0</span></p>
				<input type="hidden" value="0" id="total-value" name="data[Sale][total]"/>
				<input type="hidden" value="0" id="total-discounts" name="data[Sale][total_discount]"/>
			</div>
		</div>
	</div>

	<div class="row" style="margin-top:20px;">
		<div class="col-md-9">
			<div class="form-group">
               	
                <div class="form-material">
					<input type="text" id="name-product-active" class="form-control" readonly="" value=""/>
                    <label for="name-product-active">Nama Produk</label>
                </div>
                
            </div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
            
                <div class="form-material">
                    <input type="text" class="form-control" value="" id="qty-product-active"/>
                    <label for="qty-product-active">Jumlah</label>
                </div>
               
            </div>
		</div>

		

		<div class="col-md-6">
			<a class="btn btn-danger btn-block" id="btn-delete-product-row"> Hapus </a>
		</div>
		<div class="col-md-6">
			<a class="btn btn-success btn-block" id="btn-edit-product-row"> Ubah </a>
		</div>
	</div>
	<div class="row" style="position:absolute; bottom:10px; width:100%">
		
		<div class="col-md-12">
			<a class="btn btn-primary btn-block" id="btn-payment">Bayar</a>
		</div>
	</div>
</div>
