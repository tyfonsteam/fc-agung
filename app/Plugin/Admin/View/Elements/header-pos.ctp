<!-- Header -->
<header id="header-navbar" class="content-mini content-mini-full">
    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">
        <li>
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <a href="<?php echo $this->Html->url('/pos/session/stop'); ?>" class="btn btn-default" data-toggle="layout" data-action="side_overlay_toggle" type="button">
                <i class="fa fa-times"></i> Tutup Sesi
            </a>
        </li>
    </ul>
    <!-- END Header Navigation Right -->

    <!-- Header Navigation Left -->
    <ul class="nav-header pull-left">
        <li class="hidden-md hidden-lg">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                <i class="fa fa-navicon"></i>
            </button>
        </li>

        <li>
        <a href="<?php echo $this->Html->url('/pos'); ?>"><?php echo $this->Html->image('/pos/img/tyfons-logo.png',array('id' => 'brand-image'));?> <span class='text-primary hidden-sm hidden-xs'>Ti-e-nette Boutique</span></a>
        </li>
    </ul>
    <!-- END Header Navigation Left -->

</header>
<!-- END Header -->