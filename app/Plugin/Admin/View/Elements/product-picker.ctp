<!-- MEMBUTUHKAN PRODUCTS DAN DIVISIS SAAT AKAN MENGGUNAKAN ELEMENT INI
AKTIFKAN JUGA ELEMENT js-product-picker UNTUK INTERAKSI -->
<div class='row'>
	<div class='col-md-12 col-lg-12 col-sm-12 col-xs-12'>
		<div class='block block-themed'>
			<div class='block-header bg-primary'>
				<h3 class='block-title'> Produk </h3>
				<a id = 'anchor-error-sale-detail-form' href="javascript:void(0)"></a>
			</div>
			<div class='block-content'>
				<div class = 'row'>
                	<div class = 'col-lg-12'>
                		<div style = 'display:none' id = 'alert-sale-detail-error' class="alert alert-warning">
                            <a id = 'btn-close-alert-sale-detail-error' href = 'javascript:void(0)' id = '' type="button" class="close" >×</a>
                            <i class="fa fa-warning"></i> Error <p id = 'p-alert-sale-detail-error'></p>
                        </div>
                	</div>
                </div>
				<div class='row '>
					<div class='col-md-12 col-lg-12 col-sm-12 col-xs-12'>
						<div class="form-group">
	                        <div class="form-material">
                                <select id = 'input-sales-detail-product-id'  class='js-select2 form-control' name="data[Product][category]" data-allow-clear="true" data-placeholder="Pilih produk.." style="width:100%">
                            		<option></option>
                            		<?php foreach($products as $key => $product): 
                            			if ($product['Product']['id']) :?>
										<option data-product-name = '<?php echo $product['Product']['name']; ?>' value="<?php echo $product['Product']['id'];?>"> <?php echo $product['Product']['name']; ?> </option>
										<?php endif; ?>
									<?php endforeach; ?>
								</select>
                                <label for="name">Nama/Kode Produk</label>
                            </div>
                        </div>
	                </div>
                </div>
                <?php //debug($divisions) ?>
                <div class='row '>
					<div class='col-md-8 col-lg-8 col-sm-12 col-xs-12'>
						<div class="form-group">
	                        <div class="form-material">
                                <select id = 'input-sales-detail-product-division-name'  class='js-select2 form-control' data-allow-clear="true" data-placeholder="Pilih divisi.." style="width:100%">
                            		<option></option>
                            		<?php if (isset($divisions) && !empty($divisions)): ?>
                            			<?php foreach ($divisions as $key => $division): ?>
                            				<?php if (isset($division['Division']['id']) && !empty($division['Division']['id'])): ?>
                            				
	                            				<?php foreach ($division['DivisionEmployee'] as $key => $value): ?>
	                            					<?php if (isset($value['Employee']['id']) && !empty($value['Employee']['id'])): ?>
	                            					<option data-division-id = '<?php echo $division['Division']['id'] ?>' data-employee-id = '<?php echo $value['Employee']['id'] ?>' value = "<?php  echo $division['Division']['name'].' - '.$value['Employee']['name'] ?>"><?php  echo $division['Division']['name'].' - '.$value['Employee']['name'] ?></option>
	                            					<?php endif ?>
	                            				<?php endforeach ?>
	                            			
                            				<?php endif ?>
                            			<?php endforeach ?>
                            		<?php endif ?>
                            		

								</select>
                                <label for="name">Divisi</label>
                            </div>
                        </div>
	                </div>
                </div>
                <div class='row '>
	                <div class='col-md-8 col-lg-8 col-sm-12 col-xs-12'>
	                    <div class="form-group">
                            <div class="form-material">
                            	<input id = 'input-sales-detail-product-unit-price' class = 'form-control numericText' placeholder = '0' />
                                <label for="divisi">Harga</label>
                            </div>
	                    </div>
	                </div>
	                <div class='col-md-4 col-lg-4 col-sm-12 col-xs-12'>
	                    <div class="form-group">
	                        <div class="form-material">
	                            <input id = 'input-sales-detail-product-qty' class = 'form-control numericText' placeholder = '0' />
	                            <label for="divisi">Jumlah</label>
                            </div>
                        </div>
	                </div>
                </div>
               
                <div class = 'row'>
                	<div class = 'col-lg-12'>
                		<div class = 'form-group'>
                			<div class = 'form-material'>
                				<a id = 'btn-add-sales-detail' href="javascript:void(0)" class = 'btn btn-success btn-md btn-block'><i class = 'fa fa-plus-circle'></i> Tambahkan</a>
                				<a style = 'display:none' id = 'btn-save-edit-sales-detail' href="javascript:void(0)" class = 'btn btn-primary btn-md btn-block'><i class = 'fa fa-pencil'></i> Edit</a>
                				<a id = 'btn-reset-sales-detail' href="javascript:void(0)" class = 'btn btn-default btn-md btn-block'><i class = 'fa fa-history'></i> Kosongkan Form</a>
                			</div>
                		</div>
                	</div>
                </div>
			</div>
		</div>
    </div>
</div>
<div class='block block-themed'>
	<div class="block-header bg-primary">
		<a id = 'anchor-sale-detail-table' href="javascript:void(0)"></a>
		<div class = 'row'>
			<div class = 'col-lg-6'>
    			<h3 class="block-title"> Daftar Penjualan Produk </h3>
    		</div>
    		<div class = 'col-lg-6 text-right'>
    			Total : <span id="total-price-label"><?php echo (isset($data['Sale']['total_nominal']) && !empty($data['Sale']['total_nominal']))? $this->Number->currency( $data['Sale']['total_nominal'], $currency) : 'Rp. 0' ?></span> 
				<input type="hidden" id="total-price" value="<?php echo (isset($data['Sale']['total_nominal']) && !empty($data['Sale']['total_nominal']))? $data['Sale']['total_nominal'] : '0' ?>" name="data[Sale][total_nominal]">
    		</div>
		</div>						
	</div>
	<div class="block-content">
		<div style = 'display:none' id = 'alert-table-product-error' class="alert alert-warning">
            <a id = 'btn-close-alert-table-product-error' href = 'javascript:void(0)' id = '' type="button" class="close" >×</a>
            <i class="fa fa-warning"></i> Error <p id = 'p-alert-table-product-error'></p>
        </div>
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th class="text-center">ID</th>
						<th class="text-center">Nama Produk</th>
						<th class="text-center">Divisi</th>
						<th class="text-center">Jumlah</th>
						<th class="text-center">Harga Satuan</th>
						<th class="text-center">Total</th>
						<th class="text-center">Aksi</th>
					</tr>
				</thead>
				<tbody id="sales-detail-list">
					<?php if (isset($data['SaleItem']) && !empty($data['SaleItem'])):
						$row_number = 1;
					 ?>
					<?php foreach ($data['SaleItem'] as $key => $value): ?>
						<tr data-sales-detail-index = '<?php echo $key; ?>'>
						<input class = 'input-tr-sale-detail-product-id' type = 'hidden' name = 'data[SaleItem][<?php echo $key; ?>][Product][id]' value = '<?php echo (isset($value['Product']['id']) && !empty($value['Product']['id'])) ? $value['Product']['id'] : '' ; ?>' />
						<input class = 'input-tr-sale-detail-division-id' type = 'hidden' name = 'data[SaleItem][<?php echo $key; ?>][Division][id]' value = '<?php echo (isset($value['Division']['id']) && !empty($value['Division']['id'])) ? $value['Division']['id'] : '' ; ?>' />
						<input class = 'input-tr-sale-detail-employee-id' type = 'hidden' name = 'data[SaleItem][<?php echo $key; ?>][Employee][id]' value = '<?php echo (isset($value['Employee']['id']) && !empty($value['Employee']['id'])) ? $value['Employee']['id'] : '' ; ?>' />
						<input class = 'input-tr-sale-detail-product-name' type = 'hidden' name = 'data[SaleItem][<?php echo $key; ?>][Product][name]' value = '<?php echo (isset($value['Product']['name']) && !empty($value['Product']['name'])) ? $value['Product']['name'] : '' ; ?>' />
						<input class = 'input-tr-sale-detail-product-division-name' type = 'hidden' name = 'data[SaleItem][<?php echo $key; ?>][Division][name]' value = '<?php echo (isset($value['Division']['name']) && !empty($value['Division']['name'])) ? $value['Division']['name'] : '' ; ?>' />
						<input class = 'input-tr-sale-detail-unit-price' type = 'hidden' name = 'data[SaleItem][<?php echo $key; ?>][unit_price]' value = '<?php echo (isset($value['unit_price']) && !empty($value['unit_price'])) ? $value['unit_price'] : '' ; ?>' />
						<input class = 'input-tr-sale-detail-qty' type = 'hidden' name = 'data[SaleItem][<?php echo $key; ?>][qty]' value = '<?php echo (isset($value['qty']) && !empty($value['qty'])) ? $value['qty'] : '' ; ?>' />
						<input class = 'input-tr-sale-detail-total-price' type = 'hidden' name = 'data[SaleItem][<?php echo $key; ?>][total_price]' value = '<?php echo (isset($value['total_price']) && !empty($value['total_price'])) ? $value['total_price'] : '' ; ?>' />
						<td class = 'row-number text-center' data-row-number = '<?php echo $row_number;  ?>'><?php echo $row_number; $row_number++; ?></td>
						<td class = 'text-center td-product-name'  ><?php echo (isset($value['Product']['name']) && !empty($value['Product']['name'])) ? $value['Product']['name'] : '' ; ?></td>
						<td class = 'text-center td-product-division-name'  ><?php echo (isset($value['Division']['name']) && !empty($value['Division']['name'])) ? $value['Division']['name'] : '' ; ?></td>
						<td class = 'text-center td-qty'  ><?php echo (isset($value['qty']) && !empty($value['qty'])) ? $value['qty'] : '' ; ?></td>
						<td class = 'text-center td-unit-price'  ><?php echo (isset($value['unit_price']) && !empty($value['unit_price'])) ?$this->Number->currency( $value['unit_price'], $currency) : '' ; ?></td>
						<td class = 'text-center td-total-price'  ><?php echo (isset($value['total_price']) && !empty($value['total_price'])) ?$this->Number->currency( $value['total_price'], $currency) : '' ; ?></td>
						<td class = 'text-center'  >
						<a style = 'margin-right : 0.25em;' href='javascript:void(0)' class = 'btn btn-sale-detail-edit btn-primary btn-xs'><i class = 'fa fa-pencil'></i></a>
						<a href='javascript:void(0)' class = 'btn btn-sale-detail-delete btn-danger btn-xs'><i class = 'fa fa-trash'></i></a>
						</td>

						</tr>
					<?php endforeach ?>
					<?php endif ?>
					
				</tbody>
			</table>
		</div>
	</div>
</div>