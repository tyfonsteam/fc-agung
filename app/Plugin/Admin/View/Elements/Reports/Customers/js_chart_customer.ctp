
var BaseCompCharts = function() {
    // Chart.js Charts, for more examples you can check out http://www.chartjs.org/docs
    var initChartsChartJS = function () {
        // Get Chart Containers
        var $chartLinesCon  = jQuery('.js-chartjs-lines')[0].getContext('2d');
        var $chartPieCon    = jQuery('.js-chartjs-pie')[0].getContext('2d');
        var $chartBarsCon   = jQuery('.js-chartjs-bars')[0].getContext('2d');

        // Set Chart and Chart Data variables
        var $chartBars, $chartPie, $chartLines;
        var $chartBarsData, $chartPolarPieDonutData;

        // Set global chart options
        var $globalOptions = {
            scaleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
            scaleFontColor: '#999',
            scaleFontStyle: '600',
            tooltipTitleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
            tooltipCornerRadius: 3,
            maintainAspectRatio: false,
            responsive: true
        };

        var $chartBarsData = {
            labels: ['Cash', 'Debit', 'Transfer'],
            datasets: <?php echo json_encode($bar_chart_data_payment_type, TRUE); ?>
        };
        var $chartLinesData = <?php echo json_encode($line_chart_pos_sales, TRUE); ?>;

        // Polar/Pie/Donut Data
        var $chartPolarPieDonutData = <?php echo json_encode($pie_chart_data,TRUE);?>;

        // Init Charts
        $chartBars  = new Chart($chartBarsCon).Bar($chartBarsData, $globalOptions);
        $chartPie   = new Chart($chartPieCon).Pie($chartPolarPieDonutData, $globalOptions);
        $chartLines   = new Chart($chartLinesCon).Line($chartLinesData, $globalOptions);
    };


    

    return {
        init: function () {
            // Init all charts
            initChartsChartJS();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ BaseCompCharts.init(); });