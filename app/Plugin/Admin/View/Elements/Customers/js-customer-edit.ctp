$('#btn-submit-edit-customer').on('click', function(e) {
	e.preventDefault();
	showConfirmDialog("Apakah anda yakin untuk mengubah data pelanggan anda ?",function(result) {
        if(result){
            $("#form-edit-customer").submit();
        }
    });
});

// var substringMatcher = function(strs) {
//   return function findMatches(q, cb) {
//     var matches, substringRegex;

//     // an array that will be populated with substring matches
//     matches = [];

//     // regex used to determine if a string contains the substring `q`
//     substrRegex = new RegExp(q, 'i');

//     // iterate through the pool of strings and for any string that
//     // contains the substring `q`, add it to the `matches` array
//     $.each(strs, function(i, str) {
//       if (substrRegex.test(str)) {
//         matches.push(str);
//       }
//     });

//     cb(matches);
//   };
// };

// var cities = [];
// <?php foreach ($cities as $key => $value): ?>
// 	cities.push("<?php echo $value; ?>");
// <?php endforeach; ?>

// $('#a').typeahead({
  
//   minLength: 1
// },
// {
//   name: 'states',
//   source: substringMatcher(cities)
// });


$('#city-typeahead').typeahead({
    ajax: { 
        url: '<?php echo $this->Html->url(array("plugin"=>"admin", "controller"=>"Customers", "action"=>"get_city_typeahead")); ?>',
        triggerLength: 1 
      },     
      scrollBar :true
});
