$('#input-transaction-session-init-balance').on('input',function(e){
	var nominal = $(this).val();
	$('#span-transaction-session-init-balance').html("Rp " + parseInt(nominal).formatMoney(0,".",","));
});
	
$('#btn-submit-transaction-session-open').on('click',function(e){
	e.preventDefault();
	$('#alert-transaction-sessions-open-error').slideUp(200);
	$('#p-alert-transaction-sessions-open-error').html('');
	var pass = true;
	if($('#input-transaction-session-init-balance').val() == ''){
		pass = false;
		error_html = '- Nominal saldo awal belum diisi.';
		$('#p-alert-transaction-sessions-open-error').append(error_html);
	}
	if(!pass){
		$('#alert-transaction-sessions-open-error').slideDown(200);
		return;
	}

	showConfirmDialog("Apakah Anda yakin akan <strong>Membuka Sesi</strong> ?",function(result) {
        if(result){
            $('#form-transaction-session-open').submit();
        }
    });
	
})

$('#btn-close-alert-transaction-sessions-open-error').on('click',function(e){
	$('#alert-transaction-sessions-open-error').slideUp(200);
})

$(document).ready(function()
{	
	
   setInterval(function(e){
   		var currentTime = new Date ();
		var currentHours = currentTime.getHours ( );
		var currentMinutes = currentTime.getMinutes ( );

		// Pad the minutes and seconds with leading zeros, if required
		currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;

		// Convert the hours component to 12-hour format if needed
		currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

		// Convert an hours component of "0" to "12"
		currentHours = ( currentHours == 0 ) ? 12 : currentHours;

		// Compose the string for display
		var currentTimeString = currentHours + ":" + currentMinutes;
		
		
		$("#span-transaction-session-time").html(currentTimeString);
   }, 1000);
});