$('#input-transaction-session-closed-balance').on('input',function(e){
	var nominal = $(this).val();
	$('#span-transaction-session-closed-balance').html("Rp " + parseInt(nominal).formatMoney(0,".",","));
});
	
$('#btn-submit-transaction-session-close').on('click',function(e){
	e.preventDefault();
	$('#alert-transaction-sessions-close-error').slideUp(200);
	$('#p-alert-transaction-sessions-close-error').html('');
	var pass = true;
	if($('#input-transaction-session-closed-balance').val() == ''){
		pass = false;
		error_html = '- Nominal saldo awal belum diisi.';
		$('#p-alert-transaction-sessions-close-error').append(error_html);
	}
	if(!pass){
		$('#alert-transaction-sessions-close-error').slideDown(200);
		return;
	}

	showConfirmDialog("Apakah Anda yakin akan <strong>Menutup Sesi</strong> ?",function(result) {
        if(result){
            $('#form-transaction-session-close').submit();
        }
    });
	
})

$('#btn-close-alert-transaction-sessions-close-error').on('click',function(e){
	$('#alert-transaction-sessions-close-error').slideUp(200);
})

$(document).ready(function()
{	
	
   setInterval(function(e){
   		var currentTime = new Date ();
		var currentHours = currentTime.getHours ( );
		var currentMinutes = currentTime.getMinutes ( );

		// Pad the minutes and seconds with leading zeros, if required
		currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;

		// Convert the hours component to 12-hour format if needed
		currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

		// Convert an hours component of "0" to "12"
		currentHours = ( currentHours == 0 ) ? 12 : currentHours;

		// Compose the string for display
		var currentTimeString = currentHours + ":" + currentMinutes;
		
		
		$("#span-transaction-session-time").html(currentTimeString);
   }, 1000);
});