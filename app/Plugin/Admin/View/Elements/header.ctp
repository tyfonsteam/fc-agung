<!-- Header -->
<header id="header-navbar" class="content-mini content-mini-full">
    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">
        <li>
            <div class="btn-group">
                <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                    <?php if(!empty($user_data['Admin']['photo'])) : ?>
                    <img src="<?php echo Router::url('/').$user_data['Admin']['photo'];?>" alt="Avatar">
                    <?php else :?>
                    <img src="<?php echo Router::url('/').'admin/img/dummy-avatar.jpg';?>" alt="Avatar">
                    <?php endif;?>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-header">Actions</li>
                    <li>
                        <?php 
                            if (!$this->Session->check('session_id')) { ?>
                                <a tabindex="-1" href="<?php echo $this->Html->url('/admin/session/start');?>">
                                    <i class="fa fa-play pull-right"></i>
                                    Buka Sesi
                                </a>
                        <?php } else {?>
                            <a tabindex="-1" href="<?php echo $this->Html->url('/admin/sales/add');?>">
                                <i class="si si-basket-loaded pull-right"></i>
                                Penjualan
                            </a>
                            <a tabindex="-1" href="<?php echo $this->Html->url('/admin/sessions/close');?>">
                                <i class="fa fa-stop pull-right"></i>
                                Tutup Sesi
                            </a>
                        <?php } ?>
                    </li>
                     <?php 
                            if ($this->Session->check('session_id')) : ?>
                        <li>
                        
                                <a tabindex="-1" href="<?php echo $this->Html->url('/admin/sales');?>">
                                    <i class="fa fa-shopping-cart pull-right"></i>
                                    Daftar Penjualan
                                </a>
                        </li>
                    <?php endif ?>
                    <li>                        
                        <a tabindex="-1" href="<?php echo $this->Html->url('/admin/Admins/logout');?>">
                            <i class="si si-logout pull-right"></i>Keluar
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        
    </ul>
    <!-- END Header Navigation Right -->

    <!-- Header Navigation Left -->
    <ul class="nav-header pull-left">
        <li class="hidden-md hidden-lg">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                <i class="fa fa-navicon"></i>
            </button>
        </li>

        <li>
        <a href="<?php echo $this->Html->url('/admin'); ?>"><?php echo $this->Html->image('/admin/img/tyfons-logo.png',array('id' => 'brand-image'));?> <span class='text-primary hidden-sm hidden-xs'>Fotokopi Agung</span></a>
        </li>
    </ul>
    <!-- END Header Navigation Left -->

</header>
<!-- END Header -->