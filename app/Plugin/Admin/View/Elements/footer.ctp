<!-- Footer -->
<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
    <div class="pull-right">
        Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="http://www.tyfons.com" target="_blank">Tyfons</a>
    </div>
    <div class="pull-left hidden-xs visible-sm">
        <a class="font-w600" href="javascript:void(0);" target="_blank">Tyfons Boutique POS 1.0</a> &copy; <span class="js-year-copy"></span>
    </div>
</footer>
<!-- END Footer -->