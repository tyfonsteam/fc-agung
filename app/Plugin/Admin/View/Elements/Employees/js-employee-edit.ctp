$('#btn-submit-edit-employee').on('click', function(e) {
	e.preventDefault();
	showConfirmDialog("Apakah anda yakin untuk mengubah data pegawai anda ?",function(result) {
        if(result){
            $("#form-edit-employee").submit();
        }
    });
});
