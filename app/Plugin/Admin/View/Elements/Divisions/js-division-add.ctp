function resetEmployeeDetailForm() {
	// $("#select-division-employee-id").prop('selectedIndex', 0);
}

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

$(document).ready(function(e) {
	$("#form-division-employee-warning").hide();
});

$("#btn-division-employee-warning-close").on('click', function(e) {
	$("#form-division-employee-warning").slideUp();
});

var employee_ids = [];
$("#btn-add-employee").on('click', function(e) {
	$("#form-division-employee-warning").slideUp();
	var employee_id = $("#select-division-employee-id").val();

	if(employee_id == 0) {
		$("#label-division-employee-warning").html("Pilih Nama Pegawai Terlebih Dahulu");
		$("#form-division-employee-warning").slideDown();
		return;
	}

	var dups = false;
	if(employee_ids.length > 0) {
		for(var i=0; i<employee_ids.length; i++) {
			console.log(employee_ids[i]);
			console.log(employee_id);
			if(employee_ids[i] == employee_id) {
				dups = true;
			}
		}
	}

	if(dups == false) {
		employee_ids.push(employee_id);
		var employee_name = $("#select-division-employee-id option:selected").attr("data-employee-name");
		var row = $("#tbody-division-employee-list").attr("data-employee-row");

		var html = "";
		html += "<tr id=" + employee_id + " class='tr-employe-row'>";
		html += ("<input name='data[DivisionEmployee][" + (row - 1) + "][employee_id]' type='hidden' value='" + employee_id + "'>");
		html += ("<td class='text-center row-number'>" + row + "</td>");
		html += ("<td>" + employee_name + "</td>");
		html += "<td class='text-center'>";
		html += "<div class='btn-group'>";
		html += "<a data-msg='Pegawai' href='javascript:void(0)' class='btn btn-xs btn-employee-detail-delete btn-danger'><i class='fa fa-trash'></i></a>";
		html += "</div>";
		html += "</td>"; 
		html += "</tr>";

		$("#tbody-division-employee-list").append(html);
		row++;
		$("#tbody-division-employee-list").attr("data-employee-row", row);

		resetEmployeeDetailForm();
	}
	else {
		$("#label-division-employee-warning").html("Pegawai sudah dimasukkan ke dalam divisi ini");
		$("#form-division-employee-warning").slideDown();
		return;
	}
});

$('body').on('click', '.btn-employee-detail-delete', function(e) {
	$("#form-division-employee-warning").slideUp();
	resetEmployeeDetailForm();
	var tr_element = $(this).closest('tr');
	var tr_id = tr_element.attr("id");
	employee_ids.remove(tr_id);
	tr_element.remove();
	console.log(employee_ids);
	//update penomoran tabel
	var n = $("tr.tr-employe-row").size();
	var row_number = 1;
	$.each($("#tbody-division-employee-list tr"), function(idx) {
		$(this).find("td.row-number").html(row_number);
		row_number++;
	});
	$("#tbody-division-employee-list").attr("data-employee-row", n + 1);
});

$('#btn-submit-new-division').on('click', function(e) {
	e.preventDefault();
	showConfirmDialog("Apakah anda yakin untuk menambah data divisi ?",function(result) {
        if(result){
            $("#form-new-division").submit();
        }
    });
});
