<?php 
$dashboard = '';

$settings = '';
$setting_bank = '';
$setting_religion = '';
$privileges = '';

$users = '';
$user_lists = '';
$user_add = '';

$products = '';
$product_add = '';
$product_lists = '';

$sales = '';
$sales_add = '';
$sales_lists = '';

$customers = '';
$customers_add = '';
$customers_lists = '';

$employees = '';
$employees_add = '';
$employees_lists = '';

$divisions = '';
$divisions_add = '';
$divisions_lists = '';

$reports = '';
$reports_lists = '';

$purchases = '';
$purchases_add = '';
$purchases_lists = '';

switch ($this->params['controller']) {
    case 'Products':
        $products = 'open';
        switch ($this->params['action']) {
            case 'add':
                $product_add = 'active';
                break;
            case 'index':
                $product_lists = 'active';
                break;
        }
        break;
    case 'Dashboards':
        $dashboard = 'active';
        break;
    case 'Users':
        $settings = 'open';
        $users = 'open';
        switch ($this->params['action']) {
            case 'index':
                $user_lists = 'active';
                break;
            case 'add': 
                $user_add = 'active';
                break;
            default:
                # code...
                break;
        }
        break;
    case 'Sales':
        $sales = 'open';
        switch ($this->params['action']) {
            case 'detail':
            case 'edit':
            case 'index':
                $sales_lists = 'active';
                break;
            case 'add': 
                $sales_add = 'active';
                break;
        }
        break;
    case 'Customers':
        $customers = 'open';
        switch ($this->params['action']) {
            case 'detail':
            case 'edit':
            case 'index':
                $customers_lists = 'active';
                break;
            case 'add': 
                $customers_add = 'active';
                break;
        }
        break;
    case 'Divisions':
        $divisions = 'open';
        switch ($this->params['action']) {
            case 'detail':
            case 'edit':
            case 'index':
                $divisions_lists = 'active';
                break;
            case 'add': 
                $divisions_add = 'active';
                break;
        }
        break;
    case 'Employees':
        $employees = 'open';
        switch ($this->params['action']) {
            case 'detail':
            case 'edit':
            case 'index':
                $employees_lists = 'active';
                break;
            case 'add': 
                $employees_add = 'active';
                break;
        }
        break;
    case 'Reports':
        $reports = 'open';
        switch ($this->params['action']) {
            case 'detail':
            case 'edit':
            case 'add':
            case 'index':
                $reports_lists = 'active';
                break;
        }
        break;
    case 'Purchases':
        $purchases = 'open';
        switch ($this->params['action']) {
            case 'detail':
            case 'edit':
            case 'add':
                $purchases_add = 'active';
                break;
            case 'index':
                $purchases_lists = 'active';
        }
        break;
    case 'Privileges':
        $privileges = 'active';
        $settings = 'open';
        break;
}
?>
<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->

            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <a class="h5 text-white" href="<?php echo $this->Html->url('/admin');?>">
                    <i class="fa fa-laptop text-primary"></i> <span class="h5 font-w600 sidebar-mini-hide">Point of Sale</span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a class="<?php echo $dashboard; ?>" href="<?php echo $this->Html->url('/admin');?>"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                    </li>

                    <?php if(array_key_exists('sales', $userPrivParent)): ?>
                    <li class="<?php echo $sales;?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-shopping-cart"></i> Penjualan</a>
                        <ul>
                            <?php if(in_array(2, $userPrivParent['sales'])): ?>
                                <li>
                                   <a class="<?php echo $sales_add; ?>" href="<?php echo $this->Html->url('/admin/sales/add'); ?>"><i class="si si-plus"></i> <span class="sidebar-mini-hide">Tambah Penjualan</span></a> 
                                </li>
                            <?php endif; ?>
                            <?php if(in_array(1, $userPrivParent['sales'])): ?>
                                <li>
                                   <a class="<?php echo $sales_lists; ?>" href="<?php echo $this->Html->url('/admin/sales'); ?>"><i class="si si-list"></i> <span class="sidebar-mini-hide">Daftar Penjualan</span></a> 
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                    <?php if(array_key_exists('products', $userPrivParent)): ?>
                    <li class="<?php echo $products;?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-book-open"></i> Produk</a>
                        <ul>
                            <?php if(in_array(2, $userPrivParent['products'])): ?>
                                <li>
                                   <a class="<?php echo $product_add; ?>" href="<?php echo $this->Html->url('/admin/products/add'); ?>"><i class="si si-plus"></i> <span class="sidebar-mini-hide">Tambah Produk</span></a> 
                                </li>
                            <?php endif; ?>
                            <?php if(in_array(1, $userPrivParent['products'])): ?>
                                <li>
                                   <a class="<?php echo $product_lists; ?>" href="<?php echo $this->Html->url('/admin/products'); ?>"><i class="si si-list"></i> <span class="sidebar-mini-hide">Daftar Produk</span></a> 
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                    <?php if(array_key_exists('purchases', $userPrivParent)): ?>
                    <li class="<?php echo $purchases;?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-building-o"></i> Pengeluaran</a>
                        <ul>
                            <?php if(in_array(2, $userPrivParent['purchases'])): ?>
                                <li>
                                   <a class="<?php echo $purchases_add; ?>" href="<?php echo $this->Html->url('/admin/purchases/add'); ?>"><i class="si si-plus"></i> <span class="sidebar-mini-hide">Tambah Pengeluaran</span></a> 
                                </li>
                            <?php endif; ?>
                            <?php if(in_array(1, $userPrivParent['purchases'])): ?>
                                <li>
                                   <a class="<?php echo $purchases_lists; ?>" href="<?php echo $this->Html->url('/admin/purchases'); ?>"><i class="si si-list"></i> <span class="sidebar-mini-hide">Daftar Pengeluaran</span></a> 
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                    <?php if(array_key_exists('customers', $userPrivParent)): ?>
                    <li class="<?php echo $customers;?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-users"></i> Pelanggan</a>
                        <ul>
                            <?php if(in_array(2, $userPrivParent['customers'])): ?>
                                <li>
                                   <a class="<?php echo $customers_add; ?>" href="<?php echo $this->Html->url('/admin/customers/add'); ?>"><i class="si si-plus"></i> <span class="sidebar-mini-hide">Tambah Pelanggan</span></a> 
                                </li>
                            <?php endif; ?>
                            <?php if(in_array(1, $userPrivParent['customers'])): ?>
                                <li>
                                   <a class="<?php echo $customers_lists; ?>" href="<?php echo $this->Html->url('/admin/customers'); ?>"><i class="si si-list"></i> <span class="sidebar-mini-hide">Daftar Pelanggan</span></a> 
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                    <?php if(array_key_exists('employees', $userPrivParent)): ?>
                    <li class="<?php echo $employees; ?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-user"></i> Pegawai</a>
                        <ul>
                            <?php if(in_array(2, $userPrivParent['employees'])): ?>
                                <li>
                                   <a class="<?php echo $employees_add; ?>" href="<?php echo $this->Html->url('/admin/employees/add'); ?>"><i class="si si-plus"></i> <span class="sidebar-mini-hide">Tambah Pegawai</span></a> 
                                </li>
                            <?php endif; ?>
                            <?php if(in_array(1, $userPrivParent['employees'])): ?>
                                <li>
                                   <a class="<?php echo $employees_lists; ?>" href="<?php echo $this->Html->url('/admin/employees'); ?>"><i class="si si-list"></i> <span class="sidebar-mini-hide">Daftar Pegawai</span></a> 
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                    <?php if(array_key_exists('divisions', $userPrivParent)): ?>
                    <li class="<?php echo $divisions;?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-building-o"></i> Divisi</a>
                        <ul>
                            <?php if(in_array(2, $userPrivParent['divisions'])): ?>
                                <li>
                                   <a class="<?php echo $divisions_add; ?>" href="<?php echo $this->Html->url('/admin/divisions/add'); ?>"><i class="si si-plus"></i> <span class="sidebar-mini-hide">Tambah Divisi</span></a> 
                                </li>
                            <?php endif; ?>
                            <?php if(in_array(1, $userPrivParent['divisions'])): ?>
                                <li>
                                   <a class="<?php echo $divisions_lists; ?>" href="<?php echo $this->Html->url('/admin/divisions'); ?>"><i class="si si-list"></i> <span class="sidebar-mini-hide">Daftar Divisi</span></a> 
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                    <?php if(array_key_exists('reports', $userPrivParent)): ?>
                    <li class="<?php echo $reports;?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-building-o"></i> Laporan</a>
                        <ul>
                            <?php if(in_array(1, $userPrivParent['reports'])): ?>
                                <li>
                                   <a class="<?php echo $reports_lists; ?>" href="<?php echo $this->Html->url('/admin/reports/customers'); ?>"><i class="si si-list"></i> <span class="sidebar-mini-hide">Laporan Pelanggan</span></a> 
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                    <li class="<?php echo $settings?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-cogs"></i><span class="sidebar-mini-hide">Settings</span></a>
                        
                        <ul>
                            <?php if(array_key_exists('users', $userPrivParent)): ?>
                            <li class="<?php echo $users?>">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-users"></i> <span class="sidebar-mini-hide">Users</span> </a>
                                <ul>
                                    <?php if(in_array(2, $userPrivParent['users'])): ?>
                                    <li>
                                        <a class="<?php echo $user_add; ?>" href="<?php echo $this->Html->url('/admin/users/add');?>"><i class="si si-plus"></i><span class="sidebar-mini-hide">Tambah User</span></a>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(in_array(1, $userPrivParent['users'])): ?>
                                    <li>
                                        <a class="<?php echo $user_lists; ?>" href="<?php echo $this->Html->url('/admin/users');?>"><i class="si si-list"></i><span class="sidebar-mini-hide">Daftar User</span></a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <li>
                               <a class="<?php echo $privileges; ?>" href="<?php echo $this->Html->url('/admin/privileges');?>"><i class="si si-key"></i><span class="sidebar-mini-hide">Privilege</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->
