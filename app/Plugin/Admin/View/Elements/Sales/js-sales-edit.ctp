$('#checkbox-toogle-payment').on('change',function(e){
	if($(this).is(':checked')){
		$('#div-payment-container').show();
	} else {
		$('#div-payment-container').hide();
	}
});

$('#btn-submit-edit-sale').on('click',function(e){
	e.preventDefault();
	//CEK KALAU ADA MINIMAL 1 PRODUK DIPILIH ELEMENT product-picker
	if(!isExistSaleDetail()){
		$('#alert-table-product-error').show();
		$('#p-alert-table-product-error').html('- Harus ada minimal 1 produk untuk dijual.<br/> ');
		$('html, body').animate({
	        scrollTop: $("#anchor-sale-detail-table").offset().top-100
	    }, 200);
	    return;
	}	

	//CEK KALAU ADA PEMBAYARAN
	if($('#checkbox-toogle-payment').is(':checked')){

		//CEK FORM PEMBAYARAN PADA ELEMENT payment
		if(!isPaymentFormValid()){

			$('html, body').animate({
		        scrollTop: $("#div-payment-container").offset().top-100
		    }, 200);
		}

		//UPDATE REMAINING-PAYMENT PADA JS-PAYMENT BIAR BISA DICEK JUMLAH DAN NOMINALNYA.
		//ambil dari product-picker, lempar ke payment
		var sale_total = getProductTotalNominal();
		updateRemainingPayment(sale_total);

		if(!isPaymentNominalCorrect()) {
			$('html, body').animate({
		        scrollTop: $("#div-payment-container").offset().top-100
		    }, 200);
		    return;
		}
		
	}

	showConfirmDialog("Apakah Anda yakin akan menyimpan <strong>Form Penjualan</strong> ini ?",function(result) {
        if(result){
            $("#form-edit-sales").submit();
        }
    });
})