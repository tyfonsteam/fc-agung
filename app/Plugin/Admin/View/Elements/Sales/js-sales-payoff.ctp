$('#btn-submit-payoff-sale').on('click',function(e){
	e.preventDefault();
	var pass = true;
	//CEK FORM PEMBAYARAN PADA ELEMENT payment
	if(!isPaymentFormValid()){

		$('html, body').animate({
	        scrollTop: $("#div-payment-container").offset().top-100
	    }, 200);
	    pass = false;
	}

	if(!isPaymentNominalCorrect()) {
		$('html, body').animate({
	        scrollTop: $("#div-payment-container").offset().top-100
	    }, 200);
	    pass = false;
	}

	if(!pass){
		return;
	}

	showConfirmDialog("Apakah Anda yakin akan menyimpan <strong>Form Pelunasan</strong> ini ?",function(result) {
        if(result){
            $("#form-payoff-sales").submit();
        }
    });
})