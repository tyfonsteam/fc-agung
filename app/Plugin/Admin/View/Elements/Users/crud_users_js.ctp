$(document).on("click", ".btn-delete", function(e){ 
    e.preventDefault();
    showDeleteConfirmationDialog('Admin', $(this).attr('href'));
});

$(document).on('click', '#btn-check', function(e){
	var state = $(this).data('state');
	if(state == 'check'){
		$(this).text('Uncheck All');
		$(this).data('state', 'uncheck');
        $('.privs-check').each(function() { 
            this.checked = true;
        });
	}else if(state == 'uncheck'){
		$(this).text('Check All');
		$(this).data('state', 'check');
        $('.privs-check').each(function() { 
            this.checked = false;
        });
	}
});

$(document).on('click', '#btn-check-one-module', function(e){
    var state = $(this).data('state');
    var parent_name = $(this).data('parent-name');
    if(state == 'check'){
        $(this).text('Uncheck All');
        $(this).data('state', 'uncheck');
        $('#'+parent_name+' .privs-check').each(function() { 
            this.checked = true;
        });
    }else if(state == 'uncheck'){
        $(this).text('Check All');
        $(this).data('state', 'check');
        $('#'+parent_name+' .privs-check').each(function() { 
            this.checked = false;
        });
    }
});

//select image and render
$(document).ready( function() {
    $('#imgInp:file').on('change', function() {
        readURL(this);
    });
});

function readURL(input) {
    if (!input.files || !input.files[0]) {
        return;
    }
    var html = "";
    $('#preview-div').html(html);
    var reader = new FileReader();
    var counter = 0;
    reader.readAsDataURL(input.files[counter++]);

    reader.onload = function (e) {
        html += "<div class='col-sm-6 col-md-4 col-lg-3 animated fadeIn'>";
        html += "<a class='img-link img-thumb' href ='" + e.target.result + "'>";
        html += "<img class='img-responsive' src='" + e.target.result + "'/>" ;
        html += "</a>" ;
        html += "</div>" ;
        $('#preview-div').append(html);
        html = "";
        if(counter==input.files.length)return;
        reader.readAsDataURL(input.files[counter++]);
    }   
}

$('.btn-save').on('click',function(e){
    showConfirmDialog("Are you sure to save your update ?",function(result) {
        if(result){
            $("#form-user").submit();
        }
    });
})