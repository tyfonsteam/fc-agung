$(document).on('click', '.btn-expand', function(e){
	if($(this).data('icon') == 'expand'){
		$(this).data('icon','compress');
		$(this).html('<i class="fa fa-compress"></i>');
	}else{
		$(this).data('icon','expand');
		$(this).html('<i class="fa fa-expand"></i>');
	}
	$('#'+$(this).data('child')).toggle('slow');
});

$(document).on('click', '.btn-delete', function(e){
	e.preventDefault();
    showDeleteConfirmationDialog($(this).data('message'), $(this).attr('href'));
	
});