Dropzone.autoDiscover = false;
$("#dZUpload").dropzone({
    paramName: 'photo',
    url: "<?php echo $this->Html->url('/pos/users/profile'); ?>",
    addRemoveLinks: true,
    success: function (file, response) {
        var imgName = response;
        file.previewElement.classList.add("dz-success");
        console.log(file);
        $("#photo").val(file);
    },
    error: function (file, response) {
        file.previewElement.classList.add("dz-error");
    },
    accept: function(file, done) {
        console.log("uploaded");
        done();
      },
      init: function() {
        this.on("addedfile", function() {
          if (this.files[1]!=null){
            this.removeFile(this.files[0]);
          }
        });
      },
    sending: function(file, xhr, formData) {
      // Will send the filesize along with the file as POST data.
      formData.append("file", file);
    }
});
