<div class="alert alert-<?php echo h($class); ?> alert-dismissable" style="display:none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h3 class="font-w300 push-15"><?php echo ucfirst(h($class)); ?></h3>
    <p><?php echo h($message)?></p>
</div>
