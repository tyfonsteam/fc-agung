<ol class="breadcrumb push-10-t">
	<?php foreach($breadcrumbs as $key => $crumb): ?>
    <li <?php echo $key == count($breadcrumbs) -1 ? 'class="active"':'';?>>
        <i class="fa <?php echo $crumb[2]?>"></i> 
        <?php if($crumb[1] == ''): echo $crumb[0];?>

    	<?php else: ?>
    		<a class='link-effect' href="<?php echo $this->Html->url($crumb[1]);?>"><?php echo $crumb[0];?></a>
    	<?php endif; ?>
    </li>
    <?php endforeach; ?>
</ol>