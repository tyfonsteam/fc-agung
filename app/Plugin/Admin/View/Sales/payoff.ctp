<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Penjualan <small>Pelunasan</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
	<ul>
	<?php foreach($validationErrors as $key => $errors): ?>
		<?php foreach($errors as $error): ?>
			<li>
			<?php echo $error; ?>
			</li>
		<?php endforeach; ?>
		
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>
<div class='content'>
	<div class="row">
		<div class="col-md-12">
			<form id = 'form-payoff-sales' autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data">
				<div class='block block-themed'>
					<div class='block-header bg-primary'>
						<h3 class='block-title'> Informasi Penjulan </h3>
					</div>
					<div class='block-content'>
						<div class='row items-push'>
							<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
								<div class="form-group">
			                            <div class="form-material">
			                                <input class="form-control" type="text" readonly value = '<?php echo (isset($data['Sale']['ref_number']) && !empty($data['Sale']['ref_number']))? $data['Sale']['ref_number'] : 'Tidak diketahui' ?>'>
			                                <label for="name">Nomor Nota</label>
			                            </div>
			                    </div>
			                </div>
				               <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
									<div class="form-group">
				                            <div class="form-material">
				                                <input class="form-control" type="text" readonly  value = '<?php echo (isset($data['Sale']['total_nominal']) && !empty($data['Sale']['total_nominal']))? $this->Number->currency( $data['Sale']['total_nominal'], $currency) : 'Rp. 0' ?>'>
				                                <label for="name">Total Nominal</label>
				                            </div>
				                    </div>
				                </div>
	                    </div>
	                    <div class='row items-push'>
							<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
			                </div>
				            <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
								<div class="form-group">
			                            <div class="form-material">
			                                <input class="form-control" type="text" readonly  value = '<?php echo (isset($total_payment) && !empty($total_payment))? $this->Number->currency( $total_payment, $currency) : 'Rp. 0' ?>'>
			                                <label for="name">Nominal Terbayar</label>
			                            </div>
			                    </div>
				            </div>
	                    </div>
	                    <div class='row items-push'>
							<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
			                </div>
				            <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
								<div class="form-group">
			                            <div class="form-material">
			                                <input class="form-control" type="text" readonly value = '<?php echo (isset($remaining_payment) && !empty($remaining_payment))? $this->Number->currency( $remaining_payment, $currency) : 'Rp. 0' ?>'>
			                                <label for="name">Sisa Nominal</label>
			                            </div>
			                    </div>
				            </div>
	                    </div>
					</div>
					
				</div>
				<div class='block block-themed'>
					<div class='block-header bg-primary'>
						<div class = 'col-lg-6'>
			    			<h3 class="block-title"> Riwayat Pembayaran </h3>
			    		</div>
					</div>
					<div class='block-content'>
						<div class='row items-push'>
							<div class="col-md-12 table-responsive">
								<table class = 'table table-bordered'>
									<thead>
										<tr>
											<th class="text-center col-lg-1" ></th>
		                                    <th class = 'col-lg-3'>Tanggal Pembayaran</th>
		                                    <th class="text-center col-lg-2" >Admin</th>

		                                    <th class="text-center col-lg-3" >Jenis Pembayaran</th>
		                                    <th class="text-center col-lg-3" >Nominal</th>
										</tr>
									</thead>
									<tbody>
                                <?php if (isset($payments) && !empty($payments)): 
                                        $row_number = 1;
                                    ?>
                                    <?php foreach ($payments as $key => $value): 

                                    ?>
                                        <?php if (isset($value['Payment']['id']) && !empty($value['Payment']['id'])): ?>
                                            <tr>
                                                <td class="text-center"><?php echo $row_number; $row_number++; ?></td>
                                                <td>
                                                    <?php echo (isset($value['Payment']['payment_date']) && !empty($value['Payment']['payment_date'])) ? $this->Presentation->dateFormat($value['Payment']['payment_date'], 'd F Y') : '-'; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php 
                                                        echo (isset($value['Admin']['name']) && !empty($value['Admin']['name']))? $value['Admin']['name'] : 'Tidak Ada';
                                                     ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php
                                                    $class = 'info';
                                                    $purpose = 'Tidak diketahui';
                                                    if (isset($value['Payment']['payment_purpose'])&& !empty($value['Payment']['payment_purpose'])) {
                                                        switch ($value['Payment']['payment_purpose']) {
                                                            case 'dp':
                                                                $purpose = "DP";
                                                                $class = 'warning';
                                                                break;
                                                            case 'receivable':
                                                                $purpose = "Angsuran";
                                                                $class = 'info';
                                                                break;
                                                            case 'paid_off':
                                                                $purpose = "Pelunasan";
                                                                $class = 'success';
                                                                break;
                                                        }
                                                    }
                                                    ?>
                                                    <span class="badge badge-<?php echo $class; ?>"><?php echo $purpose; ?></span>
                                                </td>
                                                <td class="text-right">
                                                    <?php 
                                                        echo (isset($value['Payment']['total_nominal']) && !empty($value['Payment']['total_nominal']))? $this->Number->currency( $value['Payment']['total_nominal'], $currency) : 'Rp 0';
                                                     ?>
                                                </td>
                                                
                                            </tr>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                                <tr class = 'success'>
                                    <td class = 'text-center' colspan='4'>Total Pembayaran</td>
                                    <td class = 'text-right'><?php echo (isset($total_payment) && !empty($total_payment)) ? $this->Number->currency( $total_payment, $currency): 'Rp 0'; ?></td>
                                </tr>
                            </tbody>

								</table>
							</div>
						</div>
					</div>
				</div>

				<div id = 'div-payment-container'>
					<?php $payment_type = array(
						'paid_off' => true,
						'dp' => false,
						'receivable' => false
					); 
					?>
					<?php echo $this->element('payment',array('data' => $data,'remaining_payment' => $remaining_payment,'payment_type' => $payment_type)); ?>
				</div>
				
				<div class='block block-themed'>
					<div class='block-header bg-flat-dark'>
						<h3 class='block-title'> Konfirmasi</h3>
					</div>
					<div class='block-content'>
						<div class='row items-push'>
							<div class="col-md-12">
								<label> Apakah Anda yakin ? </label>

								<button id = 'btn-submit-payoff-sale' type="submit" class="btn btn-success pull-right"><i class="fa fa-check-circle-o"></i> Simpan </button>

								<a style="margin-right:10px;" href="<?php echo $this->Html->url('/configure/users');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>