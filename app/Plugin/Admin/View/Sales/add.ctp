<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Penjualan <small>Tambah Penjualan</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
	<ul>
	<?php foreach($validationErrors as $key => $errors): ?>
		<?php foreach($errors as $error): ?>
			<li>
			<?php echo $error; ?>
			</li>
		<?php endforeach; ?>
		
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>
<div class='content'>
	<div class="row">
		<div class="col-md-12">
			<form id = 'form-new-sales' autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data">
				<?php echo $this->element('product-picker',array('data'=>$data,'divisions'=>$divisions)); ?>

				<div class='block block-themed'>
					<div class='block-header bg-primary'>
						<h3 class='block-title'> Informasi Dasar </h3>
					</div>
					<div class='block-content'>
						<div class='row items-push'>
							<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
								<div class="form-group">
			                            <div class="form-material">
			                                <input class="form-control" type="text" readonly value ='Otomatis Dibuat'>
			                                <label for="name">Nomor Nota</label>
			                            </div>
			                    </div>
			                </div>
			                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
								<div class="form-group">
		                            <div class="form-material">
		                            	<?php 
		                            	$order = '';
		                            	$onsite = '';
		                            	if (isset($data['Sale']['sales_type']) && !empty($data['Sale']['sales_type'])) {

		                            		switch ($data['Sale']['sales_type']) {
		                            			case 'onsite':
		                            				$onsite = 'selected';
		                            				break;
		                            			
		                            			case 'order':
		                            				$order = 'selected';
		                            				break;
		                            		}
		                            		# code...
		                            	} ?>
		                                <select class = 'form-control' name = 'data[Sale][sales_type]'>
		                                	<option <?php echo $onsite; ?> value = 'onsite'>Langsung</option>
		                                	<option <?php echo $order; ?> value = 'order'>Pesan</option>
		                                </select>
		                                <label for="name">Tipe Penjualan</label>
		                            </div>
			                    </div>
			                </div>
	                    </div>
	                    <?php //debug($customers) ?>
	                    <div class='row items-push'>
			                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
			                    <div class="form-group">
		                            <div class="form-material">
		                            	<select name = 'data[Sale][customer_id]' class='js-select2 form-control' id="divisi" data-allow-clear="true" data-placeholder="Pilih pelanggan.." style="width:100%">
		                            		<option></option>
		                            		<?php 
		                            			if (isset($customers) && !empty($customers)): ?>
		                            			<?php foreach($customers as $key => $value): ?>
		                            				<?php if (isset($value['Customer']['id']) && !empty($value['Customer']['id'])): 
		                            					$selected = '';
		                            					if (isset($data['Sale']['customer_id']) && !empty($data['Sale']['customer_id']) && $data['Sale']['customer_id'] == $value['Customer']['id']) {
				                            				$selected = 'selected';
				                            			}
		                            				?>
		                            					<option <?php echo $selected; ?> value="<?php echo (isset($value['Customer']['id']) && !empty($value['Customer']['id']))? $value['Customer']['id'] : '';?>"> <?php echo (isset($value['Customer']['name']) && !empty($value['Customer']['name']))? $value['Customer']['name'] : '';?> </option>
		                            				<?php endif ?>
													
												<?php endforeach; ?>
		                            		<?php endif ?>
		                            		
										</select>
		                                <label for="divisi">Nama Pelanggan</label>
		                            </div>
		                        </div>
			                </div>
	                    </div>
	                    <div class='row items-push'>
							<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
								<div class="form-group">
									<label class="css-input switch switch-info">
									    <input <?php echo (isset($data['Sale']['is_paid']) && !empty($data['Sale']['is_paid']) && $data['Sale']['is_paid'] == 'on') ? 'checked' : ''; ?> id = 'checkbox-toogle-payment' type="checkbox" name="data[Sale][is_paid]"><span></span> <span id="switch-text">Pembayaran ? </span>
									</label>
			                    </div>
			                </div>
	                    </div>
					</div>
				</div>

				
				<div id = 'div-payment-container' style = '<?php echo (isset($data['Sale']['is_paid']) && !empty($data['Sale']['is_paid']) && $data['Sale']['is_paid'] == 'on') ? '' : 'display:none'; ?>'>
					<?php $payment_type = array(
						'paid_off' => true,
						'dp' => true,
						'receivable' => false
					); ?>
					<?php echo $this->element('payment',array('data'=>$data,'payment_type'=>$payment_type)); ?>
				</div>
				<div class='block block-themed'>
					<div class='block-header bg-flat-dark'>
						<h3 class='block-title'> Konfirmasi</h3>
					</div>
					<div class='block-content'>
						<div class='row items-push'>
							<div class="col-md-12">
								<label> Apakah Anda yakin ? </label>

								<button id = 'btn-submit-new-sale' type="submit" class="btn btn-success pull-right"><i class="fa fa-check-circle-o"></i> Simpan </button>

								<a style="margin-right:10px;" href="<?php echo $this->Html->url('/admin/sales');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>