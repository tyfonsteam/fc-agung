<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Penjualan <small>Daftar Penjualan</small>
	        </h1>
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<div class="block">
<div class="block-header">
	<div class="row">
		
			<div class="'col-md-4 col-lg-4 col-sm-4 col-xs-4">
				<form role="form" class="form" action = '<?php echo $this->Html->url('/admin/sales'); ?>'>
				<div class="input-group">
					<input name="query" type="text" class="form-control" placeholder="Search for...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
				</form>
			</div>
			<div class = 'col-md-8 col-lg-8 col-sm-8 col-xs-8 text-right'>
				<?php if ($this->Session->check('session_id')) : ?>
			        <a href="<?php echo $this->Html->url('/admin/sales/add'); ?>" class = 'btn btn-primary'><i class = 'fa fa-plus-circle'></i> Tambah Penjualan</a>
			    <?php endif; ?>
			</div>	
	</div>
	
	<?php if($searchData != null && $searchData['query'] != ''):?>
		<div class="row">
			<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
				<label class="label label-default">
					Search result : 
					<?php echo $searchData['query']; ?>
				</label>
			</div>
		</div>
	<?php endif; ?>
</div>


<div class='block-content'>
	<div class="row items-push">
		<div class="col-lg-12 text-right">
			<a class="btn btn-primary" href="<?php echo $this->Html->url('/admin/sales/add'); ?>"><i class="fa fa-plus"></i> Tambah Penjualan</a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 table-responsive">
			<table class="table table-striped table-bordered table-header-bg">
				<thead>
					<th class="text-center"> No </th>
					<th class="text-center"> Tg. Pembuatan </th>
					<th class="text-center"> Ref Number </th>
					<th class="text-center"> Pelanggan </th>
					<th class="text-center"> Status Pembayaran </th>
					<th class="text-center"> Tipe Penjualan </th>
					<th class="text-center"> Total Nominal </th>
					<th class="text-center"> Aksi </th>
				</thead>
				<tbody>
					<?php if (isset($data) && !empty($data)):
						$row_number = 1;
						 ?>
						<?php foreach($data as $key => $value): ?>
							<?php if (isset($value['Sale']['id']) && !empty($value['Sale']['id'])): ?>
								<tr> 
									<td class="text-center"> <?php echo $row_number;
										$row_number++;
									 ?> </td>
									<td class="text-center"> <?php echo (isset($value['Sale']['created_date']) && !empty($value['Sale']['created_date'])) ? $this->Presentation->dateFormat($value['Sale']['created_date'], 'd F Y') : '-'; ?> </td>
									<td class="text-center"> <?php echo (isset($value['Sale']['ref_number']) && !empty($value['Sale']['ref_number']))? $value['Sale']['ref_number'] : '-'; ?> </td>
									<td class="text-center"> 
										<?php echo (isset($value['Customer']['name']) && !empty($value['Customer']['name']))? $value['Customer']['name'] : '-'; ?>
									</td>
									<td class="text-center"> 
										<?php
											$status = 'Tidak Diketahui';
											$class = 'default';
											if (isset($value['Sale']['status_payment']) && !empty($value['Sale']['status_payment'])) {
												switch ($value['Sale']['status_payment']) {
													case 'dp':
														$status = 'DP';
														$class = 'warning';
														break;
													case 'paid_off':
														$status = 'Lunas';
														$class = 'success';
														break;
													default :
														$status = 'Belum Terbayar';
														$class = 'danger';
														break;
												}
											}
										 ?>
										<span class = 'badge badge-<?php echo $class; ?>'>
											<?php echo $status; ?>
										</span>
										
									</td>
									<td class="text-center"> 
										<?php
											$status = 'Tidak Diketahui';
											$class = 'default';
											if (isset($value['Sale']['sales_type']) && !empty($value['Sale']['sales_type'])) {
												switch ($value['Sale']['sales_type']) {
													case 'order':
														$status = 'Pesanan';
														$class = 'info';
														break;
													case 'onsite':
														$status = 'Beli ditempat';
														$class = 'warning';
														break;
												}
											}
										 ?>
										<span class = 'badge badge-<?php echo $class; ?>'>
											<?php echo $status; ?>
										</span>
										
									</td>
									<td class="text-right"> 
										<?php echo (isset($value['Sale']['total_nominal'])&& !empty($value['Sale']['total_nominal'])) ? $this->Number->currency( $value['Sale']['total_nominal'], $currency)  : 'Rp. 0'; ?>
									</td>
									<td class="text-center">

										<a data-toggle="tooltip"  title="Detil Penjualan" data-placement="left" href="<?php echo $this->Html->url('/admin/sales/detail/'.$value['Sale']['id']);?>" class="btn btn-default btn-xs"> <i class="fa fa-list"></i> </a>
										<?php if (isset($value['Sale']['status_payment']) && !empty($value['Sale']['status_payment']) && $value['Sale']['status_payment'] != 'paid_off'): ?>
											<a data-toggle="tooltip" title="Pelunasan Penjualan" data-placement="left" href="<?php echo $this->Html->url('/admin/sales/payoff/'.$value['Sale']['id']);?>" class="btn btn-success btn-xs"> <i class="fa fa-money"></i> </a>
										<?php endif ?>
										
										
										<a data-toggle="tooltip" title="Edit Penjualan" data-placement="left" href="<?php echo $this->Html->url('/admin/sales/edit/'.$value['Sale']['id']);?>" class="btn btn-primary btn-xs"> <i class="fa fa-pencil"></i> </a>
										
										<a data-toggle="tooltip" data-msg = 'Penjualan' title="Hapus Penjualan" data-placement="left" href="<?php echo $this->Html->url('/admin/sales/delete/'.$value['Sale']['id']);?>" class="btn btn-danger btn-xs btn-delete"> <i class="fa fa-trash"></i> </a>
										
									</td>
								</tr>

							<?php endif ?>
							
						<?php endforeach; ?>
					<?php else: ?>
						<tr>
							<td class = 'text-center' colspan = '8'>Tidak ada data penjualan.</td>
						</tr>
					<?php endif ?>
					
				</tbody>

				
			</table>
			<?php if($this->Paginator->hasNext() || $this->Paginator->hasPrev()): ?>
			<div class="paginate pull-right">
				<ul class="pagination">
			<?php
				echo $this->Paginator->prev(__('<< Previous'), array('tag' => 'li', 'disabledTag'=>'a'),null, array('tag'=>'li','disabledTag'=>'a', 'class'=>'disabled'));
				echo $this->Paginator->numbers(array(
				    'separator' => '',
				    'currentClass' => 'active',
				    'currentTag' => 'a',
				    'tag' => 'li'
				));					
				echo $this->Paginator->next(__('Next >>'), array('tag' => 'li', 'disabledTag'=>'a'),null, array('tag'=>'li','disabledTag'=>'a', 'class'=>'disabled'));
			?>
				</ul>	
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
</div>
