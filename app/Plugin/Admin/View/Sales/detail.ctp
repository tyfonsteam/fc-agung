<div class='content bg-gray-lighter'>
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Penjualan <small>Detil Penjualan</small>
            </h1>
        </div>
        <div class='col-sm-5 text-right hidden-xs'>
            <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
        </div>
    </div>
</div>
<div class = 'content'>
    <div class = 'row'>
        <div class="col-md-12">
            <div class="block">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <!-- Print Page functionality is initialized in App() -> uiHelperPrint() -->
                            <button type="button" onclick="App.initHelper('print-page');"><i class="si si-printer"></i> Print Invoice</button>
                        </li>
                        <li>
                            <button type="button" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">NOMOR NOTA : <?php echo (isset($data['Sale']['ref_number']) && !empty($data['Sale']['ref_number']))? $data['Sale']['ref_number'] : ''; ?></h3>
                </div>
                <div class="block-content block-content-narrow">
                    <!-- Invoice Info -->
                    <div class="h1 text-center push-30-t push-30 hidden-print">Detil Penjualan</div>
                    <hr class="hidden-print">
                    <div class="row items-push-2x">
                        <!-- Company Info -->
                        <div class="col-xs-6 col-sm-4 col-lg-6">
                            <p class="h2 font-w400 push-5"><?php echo $global_settings['company_name']; ?></p>
                                <?php echo $global_settings['company_detail']; ?><br>
                                <?php echo $global_settings['address']; ?><br>
                                <?php echo $global_settings['website']; ?><br>
                                <i class="si si-call-end"></i> <?php echo $global_settings['phone']; ?>
                        </div>
                        <!-- END Company Info -->

                        <!-- Client Info -->
                        <div class="col-xs-6 col-sm-4 col-lg-6  text-right">
                            <p class="h2 font-w400 push-5">Pelanggan</p>
                            <address>
                                Address<br>
                                Total Belanja : <h5><?php echo (isset($data['Sale']['total_nominal']) && !empty($data['Sale']['total_nominal'])) ? $this->Number->currency( $data['Sale']['total_nominal'], $currency): 'Rp 0'; ?></h5>
                                 Total Pembayaran : <h5><?php echo (isset($total_payment) && !empty($total_payment)) ? $this->Number->currency( $total_payment, $currency): 'Rp 0'; ?></h5>
                                <i class="si si-call-end"></i> (000) 000-0000
                            </address>
                        </div>
                        <!-- END Client Info -->
                    </div>
                    <!-- END Invoice Info -->

                    <!-- Table -->
                    <div class ='row'>
                        <div class = 'col-lg-12'>
                            <h4>Daftar penjualan produk</h4>
                        </div>
                    </div>
                    <div class="table-responsive push-50">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;"></th>
                                    <th>Nama Produk</th>
                                    <th class="text-right" style="width: 120px;">Jumlah</th>
                                    <th class="text-right" style="width: 120px;">Satuan</th>
                                    <th class="text-right" style="width: 120px;">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($data['SaleItem']) && !empty($data['SaleItem'])): 
                                        $row_number = 1;
                                    ?>
                                    <?php foreach ($data['SaleItem'] as $key => $value): 

                                    ?>
                                        <?php if (isset($value['id']) && !empty($value['id'])): ?>
                                            <tr>
                                                <td class="text-center"><?php echo $row_number; $row_number++; ?></td>
                                                <td>
                                                    <p class="font-w600 push-10"><?php echo (isset($value['Product']['name']) && !empty($value['Product']['name'])) ? $value['Product']['name'] : ''; ?></p>
                                                    <div class="text-muted"><?php echo (isset($value['Division']['name']) && !empty($value['Division']['name'])) ? $value['Division']['name'] : 'Tidak Ada Data'; ?> / <?php echo (isset($value['Employee']['name']) && !empty($value['Employee']['name'])) ? $value['Employee']['name'] : 'Tidak Ada Data'; ?></div>
                                                </td>
                                                <td class="text-center"><span class="badge badge-primary"><?php echo (isset($value['qty']) && !empty($value['qty'])) ? $value['qty'] : '0'; ?></span></td>
                                                <td class="text-right"><?php echo (isset($value['unit_price'])&& !empty($value['unit_price'])) ? $this->Number->currency( $value['unit_price'], $currency)  : 'Rp. 0'; ?></td>
                                                <td class="text-right"><?php echo (isset($value['total_price'])&& !empty($value['total_price'])) ? $this->Number->currency( $value['total_price'], $currency)  : 'Rp. 0'; ?></td>
                                            </tr>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                                <tr class = 'success'>
                                    <td class = 'text-center' colspan='4'>Total Belanja</td>
                                    <td class = 'text-right'><?php echo (isset($data['Sale']['total_nominal']) && !empty($data['Sale']['total_nominal'])) ? $this->Number->currency( $data['Sale']['total_nominal'], $currency): 'Rp 0'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                     <div class ='row'>
                        <div class = 'col-lg-12'>
                            <h4>Riwayat pembayaran</h4>
                        </div>
                    </div>
                    <div class="table-responsive push-50">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center col-lg-1" ></th>
                                    <th class = 'col-lg-3'>Tanggal Pembayaran</th>
                                    <th class="text-center col-lg-2" >Admin</th>

                                    <th class="text-center col-lg-3" >Jenis Pembayaran</th>
                                    <th class="text-center col-lg-3" >Nominal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($payments) && !empty($payments)): 
                                        $row_number = 1;
                                    ?>
                                    <?php foreach ($payments as $key => $value): 

                                    ?>
                                        <?php if (isset($value['Payment']['id']) && !empty($value['Payment']['id'])): ?>
                                            <tr>
                                                <td class="text-center"><?php echo $row_number; $row_number++; ?></td>
                                                <td>
                                                    <?php echo (isset($value['Payment']['payment_date']) && !empty($value['Payment']['payment_date'])) ? $this->Presentation->dateFormat($value['Payment']['payment_date'], 'd F Y') : '-'; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php 
                                                        echo (isset($value['Admin']['name']) && !empty($value['Admin']['name']))? $value['Admin']['name'] : 'Tidak Ada';
                                                     ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php
                                                    $class = 'info';
                                                    $purpose = 'Tidak diketahui';
                                                    if (isset($value['Payment']['payment_purpose'])&& !empty($value['Payment']['payment_purpose'])) {
                                                        switch ($value['Payment']['payment_purpose']) {
                                                            case 'dp':
                                                                $purpose = "DP";
                                                                $class = 'warning';
                                                                break;
                                                            case 'receivable':
                                                                $purpose = "Angsuran";
                                                                $class = 'info';
                                                                break;
                                                            case 'paid_off':
                                                                $purpose = "Pelunasan";
                                                                $class = 'success';
                                                                break;
                                                        }
                                                    }
                                                    ?>
                                                    <span class="badge badge-<?php echo $class; ?>"><?php echo $purpose; ?></span>
                                                </td>
                                                <td class="text-right">
                                                    <?php 
                                                        echo (isset($value['Payment']['total_nominal']) && !empty($value['Payment']['total_nominal']))? $this->Number->currency( $value['Payment']['total_nominal'], $currency) : 'Rp 0';
                                                     ?>
                                                </td>
                                                
                                            </tr>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                                <tr class = 'success'>
                                    <td class = 'text-center' colspan='4'>Total Pembayaran</td>
                                    <td class = 'text-right'><?php echo (isset($total_payment) && !empty($total_payment)) ? $this->Number->currency( $total_payment, $currency): 'Rp 0'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- END Table -->
                    <!-- END Footer -->
                </div>
            </div>
        </div>
    </div>
</div>

