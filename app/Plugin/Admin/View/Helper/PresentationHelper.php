<?php
App::uses('AppHelper','View/Helper');
class PresentationHelper extends AppHelper{
	public function dateFormat($strDate = '', $format='Y-m-d H:i:s')
	{
		if($strDate != ''){
			$date = new DateTime($strDate);		
		}else{
			$date = new DateTime();
		}
		return $date->format($format);
	}

	function makeLink($match) {
	    // Parse link.
	     $substr = substr($match, 0, 6);
	     if ($substr != 'http:/' && $substr != 'https:' && $substr != 'ftp://' && $substr != 'news:/' && $substr != 'file:/') {
	        $url = 'http://' . $match;
	     } else {
	        $url = $match;
	     }

	     return $url;
	}


	public function say($nominal)
	{
	  $word = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	  if ($nominal < 12)
	    return " " . $word[$nominal];
	  elseif ($nominal < 20)
	    return $this->say($nominal - 10) . "belas";
	  elseif ($nominal < 100)
	    return $this->say($nominal / 10) . " puluh" . $this->say($nominal % 10);
	  elseif ($nominal < 200)
	    return " seratus" . $this->say($nominal - 100);
	  elseif ($nominal < 1000)
	    return $this->say($nominal / 100) . " ratus" . $this->say($nominal % 100);
	  elseif ($nominal < 2000)
	    return " seribu" . $this->say($nominal - 1000);
	  elseif ($nominal < 1000000)
	    return $this->say($nominal / 1000) . " ribu" . $this->say($nominal % 1000);
	  elseif ($nominal < 1000000000)
	    return $this->say($nominal / 1000000) . " juta" . $this->say($nominal % 1000000);
	}
}