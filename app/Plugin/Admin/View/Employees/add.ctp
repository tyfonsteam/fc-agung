<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Pegawai <small>Tambah Pegawai</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
	<ul>
	<?php foreach($validationErrors as $key => $errors): ?>
		<?php foreach($errors as $error): ?>
			<li>
			<?php echo $error; ?>
			</li>
		<?php endforeach; ?>
		
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<div class='content'>
<div class="row">
	<div class="col-md-12">
		<form autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data" id="form-new-employee">
			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Data Pegawai </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="name" name="data[Employee][name]" value="<?php echo empty($data) ? '': $data['Employee']['name']?>">
		                                <label for="name">Nama Pegawai</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control numericText" type="text" id="phone" name="data[Employee][phone]" value="<?php echo empty($data) ? '': $data['Employee']['phone']?>">
		                                <label for="phone">No. Telp</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <textarea class="form-control" id="address" name="data[Employee][address]"><?php echo empty($data) ? '': $data['Employee']['address']; ?></textarea>
		                                <label for="address">Alamat</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control numericText" type="text" id="base-salary" name="data[Employee][base_salary]" value="<?php echo empty($data) ? '': $data['Employee']['base_salary']?>">
		                                <label for="base-salary">Gaji</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control js-datepicker" name="data[Employee][join_date]" value="<?php echo empty($data) ? '': $data['Employee']['join_date']?>">
		                                <label for="birthday">Tanggal Bergabung</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control js-datepicker" name="data[Employee][resign_date]" value="<?php echo empty($data) ? '': $data['Employee']['resign_date']?>">
		                                <label for="base-salary">Tanggal Keluar</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

				</div>
			</div>
			
			<div class='block block-themed'>
				<div class='block-header bg-flat-dark'>
					<h3 class='block-title'> Confirm</h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class="col-md-12">
							<label> Are you sure ? </label>

							<button id="btn-submit-new-employee" type="submit" class="btn btn-success pull-right"><i class="fa fa-check-circle-o"></i> Save </button>

							<a style="margin-right:10px;" href="<?php echo $this->Html->url('/admin/employees');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
