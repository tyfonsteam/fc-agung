<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Pegawai <small>Detil Pegawai</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>

<div class='content'>
<div class="row">
	<div class="col-md-12">
		<form autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data">
			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Data Pegawai </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
								<div class="col-sm-9">
									<div class="form-material">
										<input class="form-control" type="text" id="name" value="<?php echo empty($data) ? '': $data['Employee']['name']?>" disabled>
										<label for="material-text">Nama Pegawai</label>
									</div>
                                </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="phone" value="<?php echo empty($data) ? '': $data['Employee']['phone']?>" disabled>
		                                <label for="phone">No. Telp</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <textarea class="form-control" id="address" disabled><?php echo empty($data) ? '': $data['Employee']['address']; ?></textarea>
		                                <label for="address">Alamat</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<input class="form-control" type="text" id="base-salary" value="Rp <?php echo empty($data) ? '': $data['Employee']['base_salary']?>" disabled>
		                                <label for="base-salary">Gaji</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<input class="form-control js-datepicker" name="data[Employee][join_date]" value="<?php echo empty($data) ? '': $this->Presentation->dateFormat($data['Employee']['join_date'], 'd F Y'); ?>" disabled>
		                                <label for="join-date">Tanggal Bergabung</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<input class="form-control js-datepicker" name="data[Employee][resign_date]" value="<?php echo empty($data['Employee']['resign_date']) ? 'Masih Bekerja': $this->Presentation->dateFormat($data['Employee']['resign_date'], 'd F Y'); ?>" disabled>
		                                <label for="join-date">Tanggal Keluar</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>
				</div>
			</div>
			
			<div class='block block-themed'>
				<div class='block-content'>
					<div class='row items-push'>
						<div class="col-md-12">
							<a style="margin-right:10px;" href="<?php echo $this->Html->url('/admin/employees');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>