<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Divisi <small>Tambah Divisi</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
	<ul>
	<?php foreach($validationErrors as $key => $errors): ?>
		<?php foreach($errors as $error): ?>
			<li>
			<?php echo $error; ?>
			</li>
		<?php endforeach; ?>
		
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<div class='content'>
<div class="row">
	<div class="col-md-12">
		<form autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data" id="form-new-division">
			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Data Divisi </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="name" name="data[Division][name]" value="<?php echo empty($data) ? '': $data['Division']['name']?>">
		                                <label for="name">Nama Divisi</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>
				</div>
			</div>

			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Form Tambah Pegawai </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<select class='js-select2 form-control' id="select-division-employee-id" data-allow-clear="true" data-placeholder="" style="width:100%">
		                            		<option value="0"></option>
		                            		<?php foreach($employees as $key => $employee): ?>
												<option data-employee-name="<?php echo $employee['Employee']['name']; ?>" value="<?php echo $employee['Employee']['id'];?>"> <?php echo $employee['Employee']['name']; ?> </option>
											<?php endforeach; ?>
										</select>
		                                <label for="name">Nama Pegawai</label>
		                            </div>
		                        </div>
		                        <div class="col-sm-12" id="form-division-employee-warning" style="display:none;">
		                        	<div id="btn-division-employee-warning-close" class="alert alert-warning alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4 class="font-w300 push-15"><i class="fa fa-warning"></i> Warning</h4>
                                        <p id="label-division-employee-warning"></p>
                                    </div>
		                        </div>
		                        <div class="col-sm-12"></div>
		                        <div class="col-sm-12">
		                            <a href="javascript:void(0)" id="btn-add-employee" class="btn-block btn btn-success"><i class="fa fa-plus-circle"></i> Tambah Pegawai</a>
		                        </div>
		                    </div>
		                </div>
                    </div>
				</div>
			</div>

			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Daftar Pegawai </h3>
				</div>
				<div class="block-content">
					<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">No</th>
								<th>Nama</th>
                                <th class="text-center" style="width: 100px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="tbody-division-employee-list" data-employee-row="1">
                            
						</tbody>
					</table>
					</div>
				</div>
			</div>
			
			<div class='block block-themed'>
				<div class='block-header bg-flat-dark'>
					<h3 class='block-title'> Confirm</h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class="col-md-12">
							<label> Are you sure ? </label>

							<button id="btn-submit-new-employee" type="submit" class="btn btn-success pull-right"><i class="fa fa-check-circle-o"></i> Save </button>

							<a style="margin-right:10px;" href="<?php echo $this->Html->url('/admin/divisions');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
