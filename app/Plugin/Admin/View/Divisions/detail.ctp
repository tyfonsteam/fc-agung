<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Divisi <small>Detil Divisi</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
	<ul>
	<?php foreach($validationErrors as $key => $errors): ?>
		<?php foreach($errors as $error): ?>
			<li>
			<?php echo $error; ?>
			</li>
		<?php endforeach; ?>
		
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<div class='content'>
<div class="row">
	<div class="col-md-12">
		<form autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data">
			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Data Divisi </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="name" name="data[Division][name]" value="<?php echo empty($data) ? '': $data['Division']['name']?>" disabled>
		                                <label for="name">Nama Divisi</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>
				</div>
			</div>

			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Daftar Pegawai </h3>
				</div>
				<div class="block-content">
					<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="text-center" style="width: 50px;">No</th>
								<th>Nama</th>
                            </tr>
                        </thead>
                        <tbody id="tbody-division-employee-list">
                            <?php $row = 1; 
                            	foreach($data['DivisionEmployee'] as $key => $employee): ?>
                            		<tr id="<?php echo $employee[0] ?>" class='tr-employe-row'>
                            			<td class='text-center row-number'><?php echo $row; ?></td>
	                            		<input type="hidden" value="<?php echo $employee[0]; ?>" name='data[DivisionEmployee][<?php echo ($row - 1); ?>][employee_id]'>
	                            		<td><?php echo $employee[1] ?></td>
                            		</tr>
							<?php $row++; endforeach;  ?>
						</tbody>
					</table>
					</div>
				</div>
			</div>
			
			<div class='block block-themed'>
				<div class='block-content'>
					<div class='row items-push'>
						<div class="col-md-12">
							<a style="margin-right:10px;" href="<?php echo $this->Html->url('/admin/divisions');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
