<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Pelanggan <small>Detil Pelanggan</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>

<div class='content'>
<div class="row">
	<div class="col-md-12">
		<form autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data">
			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Data Pelanggan </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
								<div class="col-sm-9">
									<div class="form-material">
										<input class="form-control" type="text" id="material-text" name="material-text" placeholder="Please enter your username" value="<?php echo empty($data) ? '': $data['Customer']['name']?>" disabled>
										<label for="material-text">Nama Pelanggan</label>
									</div>
                                </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="phone" name="data[Customer][phone]" value="<?php echo empty($data) ? '': $data['Customer']['phone']?>" disabled>
		                                <label for="phone">No. Telp</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="email" name="data[Customer][email]" value="<?php echo empty($data) ? '': $data['Customer']['email']?>" disabled>
		                                <label for="desc">Email</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<input class="form-control js-datepicker" name="data[Customer][birthday]" value="<?php echo empty($data) ? '': $this->Presentation->dateFormat($data['Customer']['birthday'], 'd F Y'); ?>" disabled>
		                                <label for="birthday">Tanggal Lahir</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <textarea class="form-control" id="address" name="data[Customer][address]" disabled><?php echo empty($data) ? '': $data['Customer']['address']; ?></textarea>
		                                <label for="desc">Alamat</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<input class="form-control" type="text" id="city" name="data[Customer][city]" value="<?php echo empty($data) ? '': $data['Customer']['city']?>" disabled>
		                                <label for="birthday">Kota</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>
                 
				</div>
			</div>
			
			<div class='block block-themed'>
				<div class='block-content'>
					<div class='row items-push'>
						<div class="col-md-12">
							
							<a style="margin-right:10px;" href="<?php echo $this->Html->url('/admin/customers');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>