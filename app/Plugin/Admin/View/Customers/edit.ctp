<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Pelanggan <small>Ubah Pelanggan</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
	<ul>
	<?php foreach($validationErrors as $key => $errors): ?>
		<?php foreach($errors as $error): ?>
			<li>
			<?php echo $error; ?>
			</li>
		<?php endforeach; ?>
		
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<div class='content'>
<div class="row">
	<div class="col-md-12">
		<form autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data" id="form-edit-customer">
			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Data Pelanggan </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="name" name="data[Customer][name]" value="<?php echo empty($data) ? '': $data['Customer']['name']?>">
		                                <label for="name">Nama Pelanggan</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control numericText" type="text" id="phone" name="data[Customer][phone]" value="<?php echo empty($data) ? '': $data['Customer']['phone']?>">
		                                <label for="phone">No. Telp</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="email" name="data[Customer][email]" value="<?php echo empty($data) ? '': $data['Customer']['email']?>">
		                                <label for="email">Email</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<input class="form-control js-datepicker" name="data[Customer][birthday]" value="<?php echo empty($data) ? '': $data['Customer']['birthday']?>">
		                                <label for="birthday">Tanggal Lahir</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <textarea class="form-control" id="address" name="data[Customer][address]"><?php echo empty($data) ? '': $data['Customer']['address']; ?></textarea>
		                                <label for="address">Alamat</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material">
		                            	<input id="city-typeahead" class="form-control" type="text" id="city" name="data[Customer][city]" value="<?php echo empty($data) ? '': $data['Customer']['city']?>">
		                                <label for="birthday">Kota</label>
		                                
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>
                 	
				</div>
			</div>
			
			
			<div class='block block-themed'>
				<div class='block-header bg-flat-dark'>
					<h3 class='block-title'> Confirm</h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class="col-md-12">
							<label> Are you sure ? </label>

							<button id="btn-submit-edit-customer" type="submit" class="btn btn-success pull-right"><i class="fa fa-check-circle-o"></i> Save </button>

							<a style="margin-right:10px;" href="<?php echo $this->Html->url('/admin/customers');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>