<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Laporan Pelanggan
	        </h1>
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>	    </div>
	</div>
</div>

<div class="block">
<div class="block-header">

<div class='block-content'>
	
	<div class="row">
		<div class="col-lg-12 table-responsive">
			<table class="table table-striped table-bordered table-header-bg">
				<thead>
					<th class="text-center"> No </th>
					<th class="text-center"> Nama </th>
					<th class="text-center"> Transaksi Terakhir </th>
					<th class="text-center"> Jumlah Pembelian </th>
					<th class="text-center"> Total Nominal </th>
					<th class="text-center"> # </th>
				</thead>
				<tbody>
					<?php if(count($customers) == 0): ?>
						<tr>
							<td colspan='9' class='text-center'> No Data </td>
						</tr>
					<?php endif; ?>
					<?php foreach ($customers as $key => $customer): ?>
						<tr>
							<td class="text-center"><?php echo ($key+1); ?>&nbsp;</td>
							<td class="text-center"><?php echo $customer['Customer']['name']; ?>&nbsp;</td>
							<td class="text-center"><?php echo $this->Presentation->dateFormat($customer['Sale']['created_date'], 'd F Y'); ?>&nbsp;</td>
							<td class="text-center"><?php echo $customer[0]['total']; ?>&nbsp;</td>
							<td class="text-center"><?php echo ''; ?>&nbsp;</td>
							<td class="text-center">
								<?php if(in_array(16, $userPrivs)): ?>
								<a href="<?php echo $this->Html->url('/admin/reports/customers/detail/'.$customer['Customer']['id']); ?>" class="btn btn-xs btn-default"><i class="fa fa-folder-open"></i></a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
 
				</tbody>

				
			</table>
			<?php if($this->Paginator->hasNext() || $this->Paginator->hasPrev()): ?>			
			<div class="paginate pull-right">
				<ul class="pagination">
				
			<?php
				echo $this->Paginator->prev(__('<< Previous'), array('tag' => 'li', 'disabledTag'=>'a'),null, array('tag'=>'li','disabledTag'=>'a', 'class'=>'disabled'));
				echo $this->Paginator->numbers(array(
				    'separator' => '',
				    'currentClass' => 'active',
				    'currentTag' => 'a',
				    'tag' => 'li'
				));					
				echo $this->Paginator->next(__('Next >>'), array('tag' => 'li', 'disabledTag'=>'a'),null, array('tag'=>'li','disabledTag'=>'a', 'class'=>'disabled'));
			?>				</ul>	
			</div>
			<?php endif; ?>		</div>
	</div>
</div>
</div>