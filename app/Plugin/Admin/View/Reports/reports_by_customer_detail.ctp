<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Laporan <small>Detil Laporan Pelanggan</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
	<ul>
	<?php foreach($validationErrors as $key => $errors): ?>
		<?php foreach($errors as $error): ?>
			<li>
			<?php echo $error; ?>
			</li>
		<?php endforeach; ?>
		
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<div class='content'>
<div class="row">
	<div class="col-lg-12">
		<div class="block">
			<div class="block-header">
				<h3 class="block-title">Tanggal Laporan</h3>
			</div>
			<div class="block-content">
				<form autocomplete="off" class="form form-horizontal" method="POST">
					<div class="form-group">
		                <label class="col-md-2 control-label" for="example-daterange1">Tanggal</label>
		                <div class="col-md-8">
		                    <div class="input-daterange input-group" data-date-format="d MM yyyy">
		                        <input class="form-control" type="text" id="date-from" name="data[from]" placeholder="Dari" value="">
		                        <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
		                        <input class="form-control" type="text" id="date-to" name="data[to]" placeholder="Ke" value="">
		                    </div>
		                </div>
		                <div class="col-md-2 visible-md visible-lg">
		                	<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-filter"></i> Filter</button>
		                </div>
		                <div class="col-sm-12 col-xs-12 visible-sm visible-xs" style="margin-top:5px;">
		                	<button type="submit" class="btn btn-default btn-block"><i class="fa fa-filter"></i> Filter</button>
		                </div>
		            </div>
		        </form>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	    	<div class="block">
		        <div class="block-header">
		            <ul class="block-options">
		                <li>
		                    <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
		                </li>
		            </ul>
		            <h3 class="block-title">Grafik Penjualan Pelanggan</h3>
		        </div>
		        <div class="block-content block-content-full text-center">
		            <!-- Lines Chart Container -->
		            <div style="height: 330px;"><canvas class="js-chartjs-lines" width="447" height="330" style="width: 447px; height: 330px;"></canvas></div>
		        </div>
		    </div>
	    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			<div class="block">
		        <div class="block-header">
		            <ul class="block-options">
		                <li>
		                    <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
		                </li>
		            </ul>
		            <h3 class="block-title">Grafik Penjualan Per Tipe Produk</h3>
		        </div>
		        <div class="block-content block-content-full text-center">
		            <!-- Pie Chart Container -->
		            <div style="height: 330px;"><canvas class="js-chartjs-pie" width="447" height="330" style="width: 447px; height: 330px;"></canvas></div>
		        </div>
		    </div>
	    </div>
	     <div class="col-md-6">
	    	<div class="block">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Grafik Tipe Pembayaran</h3>
                </div>
                <div class="block-content block-content-full text-center">
                    <!-- Bars Chart Container -->
                    <div style="height: 330px;"><canvas class="js-chartjs-bars" width="447" height="330" style="width: 447px; height: 330px;"></canvas></div>
                </div>
            </div>
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<!-- Main Dashboard Chart -->
		<div class="block">
			<div class="block-header">
				<ul class="block-options">
					<li>
						<button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
					</li>
				</ul>
				<h3 class="block-title">Detil Transaksi Pelanggan</h3>
			</div>
			<div class="block-content">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<td class="text-center"> No </td>
							<td class="text-center"> Nama </td>
							<td class="text-center"> Tanggal Transaksi</td>
							<td class="text-center"> Jumlah Pembelian </td>
							<td class="text-center"> Jumlah Tunggakan </td>
						</thead>
						<tbody>
							<?php if(count($sales) == 0): ?>
								<tr>
									<td colspan='9' class='text-center'> No Data </td>
								</tr>
							<?php endif; ?>
							<?php foreach ($sales as $key => $sale): ?>
								<tr>
									<td class="text-center"><?php echo ($key+1); ?>&nbsp;</td>
									<td class="text-center"><?php echo $sale['Customer']['name']; ?>&nbsp;</td>
									<td class="text-center"><?php echo $this->Presentation->dateFormat($sale['Sale']['created_date'], 'd F Y'); ?>&nbsp;</td>
									<td class="text-center"><?php echo $sale['Sale']['total_nominal']; ?>&nbsp;</td>
									<td class="text-center"><?php echo ''; ?>&nbsp;</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
        <!-- END Main Dashboard Chart -->
	</div>
</div>
</div>
