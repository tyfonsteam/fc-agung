<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Privileges <small>Privileges of Module <?php echo ucfirst($module);?></small>
	        </h1>
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>

<div class='content'>
<div class='block'>
<div class='block-header'>
	<div class="row">
		<div class="col-md-12">
			<a href="<?php echo $this->Html->url('/admin/privileges/add/'.$module);?>" class="btn btn-primary btn-icon pull-right"><i class="fa fa-plus-circle"></i> Add Parent </a>
		</div>
	</div>
</div>

<div class='block-content'>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-header-bg">
			<thead>
				<th class="text-center"> Id </th>
				<th class="text-center"> Name </th>
				<th class="text-center"> Code </th>
				<th class="text-center"> Action </th>
			</thead>
			<tbody>
				<?php foreach($privileges as $key => $privilege): if($privilege['Privilege']['parent_id'] != 0) break;?>
					<tr> 
						<td class="text-center"> <?php echo $privilege['Privilege']['id'] ?> </td>
						<td class="text-center"> <?php echo $privilege['Privilege']['name']; ?></td>
						<td class="text-center"> 
							<a data-icon="expand" data-child="parent-<?php echo $privilege['Privilege']['id']?>" href="javascript:void(0);" class="btn btn-default btn-xs btn-expand"><i class="fa fa-expand"></i></a>
						</td>
						<td class="text-center">
							<a data-toggle="tooltip" title="Add Privilege" data-placement="left" class="btn btn-success btn-xs" href="<?php echo $this->Html->url('/admin/privileges/add/'.$privilege['Privilege']['name'].'/'.$module.'/'.$privilege['Privilege']['id']); ?>"><i class="fa fa-plus-circle"></i></a>

							<a data-toggle="tooltip" title="Edit Privilege" data-placement="left" class="btn btn-primary btn-xs" href="<?php echo $this->Html->url('/admin/privileges/edit/'.$privilege['Privilege']['id']);?>"><i class="fa fa-pencil"></i></a>

							<a data-message="Parent Privilege" data-toggle="tooltip" title="Delete Privilege" data-placement="left" class="btn btn-danger btn-xs btn-delete" href="<?php echo $this->Html->url(array('plugin'=>'admin','controller'=>'Privileges', 'action'=>'delete', $privilege['Privilege']['id'])); ?>"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				
					<?php if(!isset($privsChild[$privilege['Privilege']['id']])) continue;?>
					<tbody id="parent-<?php echo $privilege['Privilege']['id']; ?>" style="display:none">
					<?php foreach($privsChild[$privilege['Privilege']['id']] as $child): ?>
						<tr style="background:#eee"> 
							<td class="text-center">  </td>
							<td class="text-center"> <?php echo $child['Privilege']['name']; ?></td>
							<td class="text-center"> 
								<?php echo $child['Privilege']['code']; ?>
							</td>
							<td class="text-center">
								<a data-message="Privilege" data-toggle="tooltip" title="Delete Privilege" data-placement="left" class="btn btn-danger btn-xs btn-delete" href="<?php echo $this->Html->url(array('plugin'=>'admin','controller'=>'Privileges', 'action'=>'delete', $child['Privilege']['id'])); ?>"><i class="fa fa-trash"></i> </a>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>