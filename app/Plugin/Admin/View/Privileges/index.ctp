<!-- Page Heading -->
<div class="content bg-gray-lighter">
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Privileges <small>List of All Module</small>
	        </h1>
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>

<div class='block'>
<div class='block-header'>
	<div class="row">
		<div class="col-md-12">
			<a href="<?php echo $this->Html->url('/admin/privileges/add/module');?>" class="btn btn-primary btn-icon pull-right"><i class="fa fa-plus-circle"></i> Add Module</a>
		</div>
	</div>
</div>

<div class='block-content'>
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-header-bg">
			<thead>
				<th class="text-center"> No </th>
				<th class="text-center"> Module Name </th>
				<th class="text-center"> Action </th>
			</thead>
			<tbody>
				<?php foreach($modules as $key => $module): ?>
					<tr> 
						<td class="text-center"> <?php echo ($key + 1) ?> </td>
						<td class="text-center"> <?php echo ucfirst($module['Privilege']['module']);?></td>
						<td class="text-center">
							<a data-toggle="tooltip" title="Detail Privilege" data-placement="left" href="<?php echo $this->Html->url('/admin/privileges/lists/'.$module['Privilege']['module']);?>" class="btn btn-default btn-xs"> <i class="fa fa-folder-open"></i> </a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
</div>
</div>