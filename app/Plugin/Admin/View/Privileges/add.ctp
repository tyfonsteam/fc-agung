<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-6">
	        <h1 class="page-heading">
	            Privileges <small>Create New Privileges</small>
	        </h1>
	    </div>
	    <div class='col-sm-6 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>

<div class='content'>
<?php if(!empty($validationErrors)): ?>
	<div class="alert alert-danger" role="alert">
		<ul>
		<?php foreach($validationErrors as $key => $errors): ?>
			<?php foreach($errors as $error): ?>
				<li>
				<?php echo $error; ?>
				</li>
			<?php endforeach; ?>
			
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
<div class="row">
	<div class="col-md-12">
		<form role="form" method="POST" action="" class="form form-horizontal push-10-t">
			<input type="hidden" name="data[Privilege][parent_id]" value="<?php echo $parent_id?>"/>
			<div class="block block-themed">
				<div class="block-header bg-primary">
					<h3 class='block-title'> Privilege Information </h3>
				</div>
				<div class="block-content">
					<div class="form-group">
						<div class="col-sm-9">
                            <div class="form-material">
                                <input class="form-control" type="text" id="code" name="data[Privilege][code]" placeholder="Privileges Code">
                                <label for="code">Code</label>
                            </div>
                        </div>
					</div>
					<div class="form-group">
						<div class="col-sm-9">
	                        <div class="form-material">
	                            <input class="form-control" type="text" id="name" name="data[Privilege][name]" placeholder="Privileges Module Name">
	                            <label for="name">Name</label>
	                        </div>
	                    </div>
					</div>
					<div class="form-group">
						<div class="col-sm-9">
	                        <div class="form-material">
	                            <input class="form-control" type="text" id="parent" name="data[Privilege][parent]" readonly="" value="<?php echo $parent; ?>">
	                            <label for="parent">Parent</label>
	                        </div>
	                    </div>
					</div>
					<div class="form-group">
						<div class="col-sm-9">
	                        <div class="form-material">
	                            <input class="form-control" type="text" id="module" name="data[Privilege][module]" readonly="" value="<?php echo $module; ?>">
	                            <label for="module">Module</label>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
			
			<div class='block block-themed'>
				<div class='block-header bg-flat-dark'>
					<h3 class='block-title'> Confirmation </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
					<div class='col-sm-12'>
						<span> Are you sure ? </span>

						<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check-circle"></i> Save </button>
						<a href="<?php echo $this->Html->url('/admin/privileges/lists/'.$module);?>" class="btn btn-default pull-right" style="margin-right:10px"><i class="fa fa-arrow-left"></i> Back</a>
					</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>