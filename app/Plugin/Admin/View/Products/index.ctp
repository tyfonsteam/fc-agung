<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            <?php echo __('Products'); ?> <small>Daftar <?php echo __('Products'); ?></small>
	        </h1>
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>	    </div>
	</div>
</div>

<div class="block">
<div class="block-header">
	<div class="row">
		<form role="form" class="form">
			<div class="col-md-4">
				<div class="input-group">
					<input name="query" type="text" class="form-control" placeholder="Search for...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div><!-- /input-group -->
			</div>
		</form>

	</div>
	
	<?php if($searchData != null && $searchData['query'] != ''):?>		<div class="row">
			<div class="col-md-12">
				<label class="label label-default">
					Search result : 
					<?php echo $searchData['query']; ?> 
				</label>
			</div>
		</div>
	<?php endif; ?></div>

<div class='block-content'>
	
	<div class="row">
		<div class="col-lg-12 table-responsive">
			<table class="table table-striped table-bordered table-header-bg">
				<thead>
					<th class="text-center"> No </th>
					<th class="text-center"> Tanggal Dibuat </th>
					<th class="text-center"> Nama </th>
					<th class="text-center"> Harga </th>
					<th class="text-center"> Admin </th>
					<th class="text-center"> # </th>
				</thead>
				<tbody>
					<?php if(count($products) == 0): ?>
						<tr>
							<td colspan='9' class='text-center'> No Data </td>
						</tr>
					<?php endif; ?>
					<?php foreach ($products as $key => $product): ?>
						<tr>
							<td class="text-center"><?php echo ($key+1); ?>&nbsp;</td>
							<td class="text-center"><?php echo $this->Presentation->dateFormat($product['Product']['created_date'], 'd F Y'); ?>&nbsp;</td>
							<td class="text-center"><?php echo $product['Product']['name']; ?>&nbsp;</td>
							<td class="text-center"><?php echo $product['Product']['price']; ?>&nbsp;</td>
							<td class="text-center"><?php echo $product['Admin']['name']; ?>&nbsp;</td>
							<td class="text-center">
								<?php if(in_array(4, $userPrivs)): ?>
									<a href="<?php echo $this->Html->url('/admin/products/edit/'.$product['Product']['id']); ?>" class="btn btn-edit btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
								<?php endif; ?>

								<?php if(in_array(8, $userPrivs)): ?>
								<a data-msg="Produk" href="<?php echo $this->Html->url(array('plugin'=>'admin', 'controller'=>'Products', 'action'=>'delete', $product['Product']['id'])); ?>" class="btn btn-delete btn-xs btn-danger"><i class="fa fa-trash"></i></a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
 
				</tbody>

				
			</table>
			<?php if($this->Paginator->hasNext() || $this->Paginator->hasPrev()): ?>			
			<div class="paginate pull-right">
				<ul class="pagination">
				
			<?php
				echo $this->Paginator->prev(__('<< Previous'), array('tag' => 'li', 'disabledTag'=>'a'),null, array('tag'=>'li','disabledTag'=>'a', 'class'=>'disabled'));
				echo $this->Paginator->numbers(array(
				    'separator' => '',
				    'currentClass' => 'active',
				    'currentTag' => 'a',
				    'tag' => 'li'
				));					
				echo $this->Paginator->next(__('Next >>'), array('tag' => 'li', 'disabledTag'=>'a'),null, array('tag'=>'li','disabledTag'=>'a', 'class'=>'disabled'));
			?>				</ul>	
			</div>
			<?php endif; ?>		</div>
	</div>
</div>
</div>