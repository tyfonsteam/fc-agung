<!-- Page Heading -->
<div class='content bg-gray-lighter'>
	<div class="row items-push">
	    <div class="col-sm-7">
	        <h1 class="page-heading">
	            Produk <small>Ubah Produk</small>
	        </h1>
	    
	    </div>
	    <div class='col-sm-5 text-right hidden-xs'>
	        <?php echo $this->element('breadcrumbs', $breadcrumbs);?>
	    </div>
	</div>
</div>


<?php if(!empty($validationErrors)): ?>
<div class="alert alert-danger" role="alert">
	<ul>
	<?php foreach($validationErrors as $key => $errors): ?>
		<?php foreach($errors as $error): ?>
			<li>
			<?php echo $error; ?>
			</li>
		<?php endforeach; ?>
		
	<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<div class='content'>
<div class="row">
	<div class="col-md-12">
		<form autocomplete="off" role="form" method="POST" action="" class="js-validation-material push-10-t" enctype="multipart/form-data">
			<div class='block block-themed'>
				<div class='block-header bg-primary'>
					<h3 class='block-title'> Data Produk </h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="name" name="data[Product][name]" value="<?php echo empty($data) ? '': $data['Product']['name']?>">
		                                <label for="name">Nama Produk</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<select class='js-select2 form-control' id="category" name="data[Product][category]">
		                            		<option></option>
		                            		<?php foreach($categories as $key => $category): ?>
												<option value="<?php echo $category['Category']['category'];?>" <?php echo ($category['Category']['category'] == $data['Product']['category']) ? 'selected' : ''; ?>> <?php echo $category['Category']['category']; ?> </option>
											<?php endforeach; ?>
										</select>
		                                <label for="category">Kategori</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                    <div class='row items-push'>
						<div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
							<div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                                <input class="form-control" type="text" id="price" name="data[Product][price]" value="<?php echo empty($data) ? '': $data['Product']['price']?>">
		                                <label for="price">Harga</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12'>
		                    <div class="form-group">
		                        <div class="col-sm-12">
		                            <div class="form-material floating">
		                            	<textarea class="form-control" id="desc" name="data[Product][desc]"><?php echo empty($data) ? '': $data['Product']['desc']; ?></textarea>
		                                <label for="desc">Deskripsi</label>
		                            </div>
		                        </div>
		                    </div>
		                </div>
                    </div>

                 
				</div>
			</div>
			
			
			<div class='block block-themed'>
				<div class='block-header bg-flat-dark'>
					<h3 class='block-title'> Confirm</h3>
				</div>
				<div class='block-content'>
					<div class='row items-push'>
						<div class="col-md-12">
							<label> Are you sure ? </label>

							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check-circle-o"></i> Save </button>

							<a style="margin-right:10px;" href="<?php echo $this->Html->url('/admin/products');?>" class="btn btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Back</a>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>