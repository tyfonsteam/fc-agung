<div class='content bg-gray-lighter'>
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                <?php echo date('d F Y, l'); ?> <span class = 'push'><i class = 'si si-clock'></i></span>  <span id = 'span-transaction-session-time'></span>
            </h1>
        </div>
    </div>
</div>

<div class = 'content'>
    <div class = 'row'>
        <div class="col-md-12">
            <div class="block">
                <form id  = 'form-transaction-session-open' class = 'form' method = 'post' action = '<?php echo $this->Html->url('/admin/sessions/open') ?>'>
                <div class="block-header">
                    <div class = 'col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                        <h3 class="block-title">Nama Admin : <?php echo (isset($admin['Admin']['name']) && !empty($admin['Admin']['name'])) ? $admin['Admin']['name'] : 'Tidak diketahui'; ?></h3>
                    </div>
                    <div class = 'col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right'>
                        <h3 class="block-title">Nominal Saldo Awal : <span id = 'span-transaction-session-init-balance'><?php echo (isset($data['TransactionSession']['init_balance']) && !empty($data['TransactionSession']['init_balance'])) ? $this->Number->currency( $data['TransactionSession']['init_balance'] , $currency) : 'Rp 0';  ?></span></h3>
                    </div>
                </div>
                <div class="block-content block-content-narrow">
                    <div class = 'row'>
                        <div class = 'text-center col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                            <div class="h1 text-center push-30-t push-30 hidden-print">MULAI SESI</div>
                            <hr class="hidden-print">
                        </div>
                    </div>
                    <div class = 'row'>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                        <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12 text-left'>
                             <div style = '<?php echo (!empty($validationErrors))? '' : 'display:none'; ?>' id = 'alert-transaction-sessions-open-error' class="alert alert-warning">
                                <a id = 'btn-close-alert-transaction-sessions-open-error' href = 'javascript:void(0)' id = '' type="button" class="close" >×</a>
                                <i class="fa fa-warning"></i> Error 
                                <p id = 'p-alert-transaction-sessions-open-error'>
                                    <?php if(!empty($validationErrors)): ?>
                                    <?php foreach($validationErrors as $key => $errors): ?>
                                        <?php foreach($errors as $error): ?>
                                             - <?php echo $error; ?> <br/>
                                        <?php endforeach; ?>
                                        
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        </div>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                    </div>
                    <div class = 'row'>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                        <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12 text-center'>
                            <div class="form-group">
                                <label >Masukkan Saldo Awal</label>
                                <input autocomplete = 'off' id = 'input-transaction-session-init-balance' class="form-control" type="text" name="data[TransactionSession][init_balance]" value = '<?php echo (isset($data['TransactionSession']['init_balance']) && !empty($data['TransactionSession']['init_balance']))? $data['TransactionSession']['init_balance'] : ''; ?>' placeholder="0" style = 'width : 100%'>
                            </div>
                        </div>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                    </div>
                    <div class = 'row'>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                        <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12 text-center'>
                            <button id = 'btn-submit-transaction-session-open' type = 'submit' class = 'btn btn-success btn-block'><i class = 'si si-control-play'></i> Mulai Sesi </button>
                        </div>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                    </div>
                    <hr class="hidden-print">
                    <p class="text-muted text-center"><small>Masukkan nominal awal pada cash register dan tekan tombol 'Mulai Sesi'!</small></p>
                </form>
                </div>    
            </div>
        </div>
    </div>
</div>

