<div class='content bg-gray-lighter'>
    <div class="row items-push">
        <div class="col-sm-6">
            <h1 class="page-heading">
                <?php echo date('d F Y, l'); ?> <span class = 'push'><i class = 'si si-clock'></i></span>  <span id = 'span-transaction-session-time'></span>
            </h1>
        </div>
        <div class="col-sm-6 text-right">
            <a href="<?php echo $this->Html->url('/admin/sales/add') ?>" class = 'btn btn-primary'><i class = 'fa fa-plus-circle'></i> Tambah Penjualan</a>
        </div>
    </div>
</div>

<div class = 'content'>
    <div class = 'row'>
        <div class="col-md-12">
            <div class="block">
                <form id  = 'form-transaction-session-close' class = 'form' method = 'post' action = '<?php echo $this->Html->url('/admin/sessions/close') ?>'>
                <div class="block-header">
                    <div class = 'col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                        <h3 class="block-title">Nama Admin : <?php echo (isset($admin['Admin']['name']) && !empty($admin['Admin']['name'])) ? $admin['Admin']['name'] : 'Tidak diketahui'; ?></h3>
                    </div>
                    <div class = 'col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right'>
                        <h3 class="block-title">Nominal Saldo Akhir : <span id = 'span-transaction-session-closed-balance'><?php echo (isset($data['TransactionSession']['closed_balance']) && !empty($data['TransactionSession']['closed_balance'])) ? $this->Number->currency( $data['TransactionSession']['closed_balance'] , $currency) : 'Rp 0';  ?></span></h3>
                    </div>
                </div>
                <div class="block-content block-content-narrow">
                    <div class = 'row'>
                        <div class = 'text-center col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                            <div class="h1 text-center push-30-t push-30 hidden-print">TUTUP SESI</div>
                            <hr class="hidden-print">
                        </div>
                    </div>
                    <div class = 'row'>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                        <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12 text-left'>
                             <div style = '<?php echo (!empty($validationErrors))? '' : 'display:none'; ?>' id = 'alert-transaction-sessions-close-error' class="alert alert-warning">
                                <a id = 'btn-close-alert-transaction-sessions-close-error' href = 'javascript:void(0)' id = '' type="button" class="close" >×</a>
                                <i class="fa fa-warning"></i> Error 
                                <p id = 'p-alert-transaction-sessions-close-error'>
                                    <?php if(!empty($validationErrors)): ?>
                                    <?php foreach($validationErrors as $key => $errors): ?>
                                        <?php foreach($errors as $error): ?>
                                             - <?php echo $error; ?> <br/>
                                        <?php endforeach; ?>
                                        
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        </div>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                    </div>
                    <div class = 'row'>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                        <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12 text-center'>
                            <div class="form-group">
                                <label >Masukkan Saldo Akhir</label>
                                <input autocomplete = 'off' id = 'input-transaction-session-closed-balance' class="form-control" type="text" name="data[TransactionSession][closed_balance]" value = '<?php echo (isset($data['TransactionSession']['closed_balance']) && !empty($data['TransactionSession']['closed_balance']))? $data['TransactionSession']['closed_balance'] : ''; ?>' placeholder="0" style = 'width : 100%'>
                            </div>
                            <div class="form-group">
                                <label >Masukkan Admin Selanjutnya</label>
                                <select class = 'js-select2 form-control' name="data[TransactionSession][next_admin_id]" data-allow-clear="true" data-placeholder="Pilih admin.." style="width:100%">
                                    <option></option>
                                    <?php if (isset($admins) && !empty($admins)): ?>

                                        <?php foreach ($admins as $key => $value): ?>
                                            <?php if (isset($value['Admin']['id']) && !empty($value['Admin']['id'])): ?>
                                                <option value = '<?php echo $value['Admin']['id'] ?>'><?php echo $value['Admin']['name'] ?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        
                                    <?php endif ?>
                                </select>
                                
                            </div>
                        </div>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                    </div>
                    <div class = 'row'>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                        <div class='col-md-6 col-lg-6 col-sm-12 col-xs-12 text-center'>
                            <button id = 'btn-submit-transaction-session-close' type = 'submit' class = 'btn btn-success btn-block'><i class = 'fa fa-stop'></i> Tutup Sesi </button>
                        </div>
                        <div class = 'col-md-3 col-lg-3 col-sm-12 col-xs-12'></div>
                    </div>
                    <hr class="hidden-print">
                    <p class="text-muted text-center"><small>Masukkan nominal akhir pada cash register dan tekan tombol 'Tutup Sesi'!</small></p>
                </form>
                </div>    
            </div>
        </div>
    </div>
</div>

