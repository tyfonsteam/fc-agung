var ValidationUser = function() {
    // Init Login Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationUser = function(){
        jQuery('.js-validation-material').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group .form-material').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            rules: {
                'data[Admin][username]': {
                    required: true,
                    minlength: 3
                },
                'data[Admin][re_password]':{
                	equalTo: '#password',
                },
                'data[Admin][name]':{
                	required: true,
                	minlength: 4
                }
            },
            messages: {
                'data[Admin][username]': {
                    required: 'Please enter a username',
                    minlength: 'Your username must consist of at least 3 characters'
                },
                'data[Admin][re_password]': {
                    equalTo: 'Password And Re-Password do not match'
                },
                'data[Admin][name]':{
                	required: 'Please provide a name',
                    minlength: 'Your name must consist of at least 4 characters'
                }
            }
        });
    };

    return {
        init: function () {
            // Init Login Form Validation
            initValidationUser();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ ValidationUser.init(); });