var ValidationCustomer = function() {
    // Init Login Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationCustomer = function(){
        jQuery('.js-validation-material').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group .form-material').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            rules: {
                'data[Customer][name]': {
                    required: true,
                    digits: false,
                    number: false
                },
                'data[Customer][email]': {
                    email: true
                },
                'data[Customer][address]':{
                	required: true
                },
                'data[Customer][phone]':{
                	required: true,
                    digits: true
                }
            },
            messages: {
                'data[Customer][name]': {
                    required: 'Please fill the name field',
                    digits: 'Please only use alphabet characters',
                    number: 'Please only use alphabet characters'
                },
                'data[Customer][email]': {
                    email: 'Please enter a valid email'
                },
                'data[Customer][address]': {
                    required: 'Please fill the address field'
                },
                'data[Customer][phone]':{
                	required: 'Please fill the phone number field',
                    digits: 'Your phone number must be numeric characters'
                }
            }
        });
    };

    return {
        init: function () {
            // Init Login Form Validation
            initValidationCustomer();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ ValidationCustomer.init(); });