var ValidationEmployee = function() {
    // Init Login Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationEmployee = function(){
        jQuery('.js-validation-material').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group .form-material').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            rules: {
                'data[Employee][name]': {
                    required: true,
                    digits: false,
                    number: false
                },
                'data[Employee][address]':{
                	required: true
                },
                'data[Employee][phone]':{
                	required: true,
                    digits: true
                },
                'data[Employee][base_salary]':{
                    required: true,
                    digits: true
                },
                'data[Employee][join_date]':{
                    required: true
                }
            },
            messages: {
                'data[Employee][name]': {
                    required: 'Please fill the name field',
                    digits: 'Please only use alphabet characters',
                    number: 'Please only use alphabet characters'
                },
                'data[Employee][email]': {
                    email: 'Please enter a valid email'
                },
                'data[Employee][address]': {
                    required: 'Please fill the address field'
                },
                'data[Employee][phone]':{
                	required: 'Please fill the phone number field',
                    digits: 'Your phone number must be numeric characters'
                },
                'data[Employee][base_salary]':{
                    required: 'Please fill the salary field',
                    digits: 'Your salary must be numeric characters'
                },
                'data[Employee][join_date]': {
                    required: 'Please fill the join date field'
                }
            }
        });
    };

    return {
        init: function () {
            // Init Login Form Validation
            initValidationEmployee();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ ValidationEmployee.init(); });