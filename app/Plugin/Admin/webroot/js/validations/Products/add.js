var ValidationUser = function() {
    // Init Login Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationUser = function(){
        jQuery('.js-validation-material').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group .form-material').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            rules: {
                'data[Product][name]': {
                    required: true,
                    minlength: 3
                },
                'data[Product][price]':{
                	required: true
                },
                'data[Product][code_product]':{
                	required: true,
                	minlength: 4
                },
                'data[Product][stock]':{
                    required: true
                }
            },
            messages: {
                'data[Product][name]': {
                    required: 'Nama produk harus terisi'
                },
                'data[Product][price]': {
                    required: 'Harga produk harus terisi'
                },
                'data[Product][code_product]':{
                	required: 'Kode produk harus terisi'
                },
                'data[Product][stock]':{
                    required: 'Jumlah stok harus terisi'
                }
            }
        });
    };

    return {
        init: function () {
            // Init Login Form Validation
            initValidationUser();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ ValidationUser.init(); });