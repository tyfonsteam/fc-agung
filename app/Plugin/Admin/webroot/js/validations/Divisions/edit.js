var ValidationCustomer = function() {
    // Init Login Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationCustomer = function(){
        jQuery('.js-validation-material').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group .form-material').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            rules: {
                'data[Division][name]': {
                    required: true
                }
            },
            messages: {
                'data[Customer][name]': {
                    required: 'Please fill the name field',
                }
            }
        });
    };

    return {
        init: function () {
            // Init Login Form Validation
            initValidationCustomer();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ ValidationCustomer.init(); });