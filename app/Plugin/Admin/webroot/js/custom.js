/* Init Component */
$('.datetimepicker').datetimepicker();

var currency_symbol = "Rp. ";
var server_url = "http://192.168.2.2/tyfons-erp/";// must be same as in app controller

function numericText(e){
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
       // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
       // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) {
           // let it happen, don't do anything
           return;
  }
  // Ensure that it is a number and stop the keypress
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
  }
}

$(document).on('keydown', '.numericText', function(e){
	// Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

Number.prototype.formatMoney = function(c, t, d){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


function formatTanggal (input, pattern) {
    var d = new Date(input);
    var year = d.getFullYear(), nMonth = d.getMonth(), day = d.getDate();
    var month = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];
    //if (month < 10) month = '0' + month;
 
    if (day < 10) day = '0' + day;
    return pattern.replace(/%Y/g, year).replace(/%m/g, month[parseInt(nMonth)]).replace(/%d/g, day);
};

/*
* params : 
* msg => sesuatu yang mau di delete
* link => link untuk action, ex: $(this).attr('href');
*/
function showDeleteConfirmationDialog(msg, link){
  bootbox.dialog({
      message: "Sistem akan <strong>menghapus</strong> <strong>"+msg+"</strong> ini. Lanjutkan?",
      title: "Confirmation",
      buttons: {
        success: {
          label: "No",
          className: "btn-default",
          callback: function() {
            bootbox.hideAll();
          }
        },
        danger: {
          label: "Yes",
          className: "btn-success",
          callback: function() {
            window.location = link;
          }
        }
      }
    });
}

/*
* params : 
* msg => sesuatu yang mau di delete
* link => link untuk action, ex: $(this).attr('href');
*/
function showDeleteConfirmationCallbackDialog(msg, callback){
  bootbox.dialog({
      message: "Sistem akan <strong>menghapus</strong> <strong>"+msg+"</strong> ini. Lanjutkan?",
      title: "Confirmation",
      buttons: {
        success: {
          label: "No",
          className: "btn-default",
          callback: function() {
            callback(false);
          }
        },
        danger: {
          label: "Yes",
          className: "btn-success",
          callback: function() {
            callback(true);
          }
        }
      }
    });
}

/*
* params : 
* msg => sesuatu yang mau di delete
* link => link untuk action, ex: $(this).attr('href');
*/
function showConfirmDialog(msg, callback){
  bootbox.dialog({
      message: msg,
      title: "Konfirmasi",
      buttons: {
        success: {
          label: "No",
          className: "btn-default",
          callback: function() {
            callback(false);
          }
        },
        danger: {
          label: "Yes",
          className: "btn-success",
          callback: function() {
            callback(true);
          }
        }
      }
    });
}

/*
* params : 
* msg => sesuatu yang mau di delete
* link => link untuk action, ex: $(this).attr('href');
*/
function showAlertDialog(msg){
  bootbox.dialog({
      message: msg,
      title: "Alert",
      buttons: {
        success: {
          label: "Close",
          className: "btn-default",
          callback: function() {
            bootbox.hideAll();
          }
        }
      }
    });
}

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
function callNotification(msg, type){
  html = '<div id="javascript-notif" class="fixed-top-notification">';
  html += '<div class="alert alert-'+type+' alert-dismissable" style="display:none">';
    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
    html += '<h3 class="font-w300 push-15">'+type.capitalizeFirstLetter()+'</h3>';
    html += '<p>'+msg+'</p>';
  html += '</div>';
  $('body').prepend(html);
  $(".fixed-top-notification .alert").slideDown(500);
  $(".fixed-top-notification .alert").delay(2000).slideUp(500, function(e){
    $("#javascript-notif").remove();  
  });   
}

function parseInteger(string) {

    if (string == '') {
        return 0;
    } else {
        return parseInt(string);
    }  
   
}

//check if url imgae exists
function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}
