<?php
App::import('Vendor', 'Pos.phpqrcode'.DS.'qrlib');
App::uses('BarcodeHelper','Pos.Lib');
class MyLibs {

    public function generate_qrcode($code, $path, $url, $filename){
        // how to save PNG codes to server 
        //$tempDir =  APP.'Plugin'.DS.'Pos'.DS.'webroot'.DS.'img'.DS.'customers'.DS.'1'.DS; 
        $tempDir = APP.$path;
        
        $pngAbsoluteFilePath = $tempDir.$filename; 
        //$urlRelativeFilePath = '/ti-e-nette/pos/img/customers/1/'.$filename; 
        $urlRelativeFilePath = $url.$filename;
        
        if(!is_dir($tempDir)){
            mkdir($tempDir,0777, true);
        }
        QRcode::png($code, $pngAbsoluteFilePath); 

        return $urlRelativeFilePath;
    }
    /* Generate Barcode dan simpan ke url
    * params1 : nomor barcode
    * params2 : url tempat menyimpan
    * return : url tempat simpan kalo berhasil
    */
    public function generate_barcode($number, $filename, $tempDir, $url=""){
        
        $barcode=new BarcodeHelper();
        if (!isset($tempDir)) {
            $tempDir = APP.'Plugin'.DS.'Pos'.DS.'webroot'.DS.'img'.DS.'barcode'.DS;
        }     
        // Generate Barcode data
        $barcode->barcode();
        $barcode->setType('C128');
        $barcode->setCode($number);
        $barcode->setSize(80,150);
        $barcode->hideCodeType();

        if(!is_dir($tempDir)){
            mkdir($tempDir,0777,true);
        }
        // Generate filename             
        $tempDir .= $filename;

        // Generates image file on server            
        $barcode->writeBarcodeFile($tempDir);

        if($url == "")
            $url = 'pos/img/barcode/'.$filename;
 
        return $url;
    }
    /**
    * Merandom RGB
    * param 1: input integer 1-20
    * param 2: alpha, for transparency
    * return RGB
    */
    public function getColor($num, $alpha = 1) {
        $hash = md5('color' . $num);
        return array(hexdec(substr($hash, 0, 2)), hexdec(substr($hash, 2, 2)), hexdec(substr($hash, 4, 2)), $alpha);
    }

    /*
    * Mengubah string date menjadi datetime 
    * params 1 : string date ex: 21-01-2015
    * params 2 : string format date ex: 'Y-m-d H:i:s'
    * return formated datetime
    */
    public function formatDate($strDate = '', $format = 'Y-m-d H:i:s'){
        if($strDate == '') $temp = new DateTime();
        else $temp = new DateTime($strDate); 
        return $temp->format($format);
    }


    /*
    * mendapatkan hash dari tanggal hari ini
    * return hash string
    */
    public function getDateNowHash(){
        $now = new DateTime();
        return md5($now->format("Y-m-d H:i:s"));
    }
    /*
    * Replace '\' to '/';
    */
    public function dirToUrl($str = ""){
        return str_replace(DS, "/", $str);
    }

    public function uploadFile($url="", $dir=""){
        $handle = new Upload($url);
        if($handle->uploaded){  
            $now = new DateTime();
            $filename = md5($now->format("Y-m-d H:i:s"));

            $file_dir = WWW_ROOT."files".DS.$dir;

            $handle->file_new_name_body = $filename;
            $filePath = $file_dir;
            $path = "files/".$this->dirToUrl($dir).$filename.".".$handle->file_src_name_ext;
            $handle->process($filePath);
            if(!$handle->processed){
                return FALSE;
            }
            
            return $path;    
        }else{
            return FALSE;
        }
    }

    //upload video return path setelah diupload
    public function uploadVideo($url="", $dir = ""){
        $handle = new Upload($url);
        if($handle->uploaded){  
            $now = new DateTime();
            $filename = md5($now->format("Y-m-d H:i:s"));

            $file_dir = WWW_ROOT."videos".DS.$dir;

            $handle->file_new_name_body = $filename;
            $filePath = $file_dir;
            $path = "videos/".$this->dirToUrl($dir).$filename.".".$handle->file_src_name_ext;
            $handle->process($filePath);
            if(!$handle->processed){
                return FALSE;
            }
            
            return $path;    
        }else{
            return FALSE;
        }
    }

    //upload image return path setelah diupload
    public function uploadImage($url = "", $dir="", $identifier = 0){
        $handle = new Upload($url);
        if($handle->uploaded){  
            $now = new DateTime();
            $filename = md5($now->format("Y-m-d H:i:s"));
            $filename .= $identifier;
            
            $file_dir = APP.'Plugin'.DS.'Pos'.DS.'webroot'.DS.'img'.DS.$dir;
            //$file_dir = IMAGES.'Pos'.DS.$dir;

            $handle->file_new_name_body = $filename;
            $filePath = $file_dir;
            //'pos/img/customers/'.$this->Customer->getLastInsertID().'/'
            $path = 'pos/img/'.$this->dirToUrl($dir).$filename.".".$handle->file_src_name_ext;
            //$path = IMAGES_URL.'Pos/'.$this->dirToUrl($dir).$filename.".".$handle->file_src_name_ext;
            $handle->process($filePath);
            if(!$handle->processed){
                return FALSE;
            }
            return $path;                    
        }else{
            return FALSE;
        }
    }

     public function uploadImageWithThumbnail($url = "", $dir="", $identifier = 0){
        $handle = new Upload($url);
        $return = array();
        if($handle->uploaded){  
            $now = new DateTime();
            $filename = md5($now->format("Y-m-d H:i:s"));
            $filename .= $identifier;
            
            $file_dir = APP.'Plugin'.DS.'Pos'.DS.'webroot'.DS.'img'.DS.$dir;
            //$file_dir = IMAGES.'Pos'.DS.$dir;

            $handle->file_new_name_body = $filename;
            $filePath = $file_dir;
            //'pos/img/customers/'.$this->Customer->getLastInsertID().'/'
            $path = 'pos/img/'.$this->dirToUrl($dir).$filename.".".$handle->file_src_name_ext;
            //$path = IMAGES_URL.'Pos/'.$this->dirToUrl($dir).$filename.".".$handle->file_src_name_ext;
            $handle->process($filePath);
            if(!$handle->processed){
                return FALSE;
            }else{
                $return['image_big'] = $path;
                $handle = new Upload($url);
                $filename = md5($now->format("Y-m-d H:i:s"));
                $filename .= $identifier;
                
                $file_dir = APP.'Plugin'.DS.'Pos'.DS.'webroot'.DS.'img'.DS.$dir.DS.'out'.DS;
                //$file_dir = IMAGES.'Pos'.DS.$dir;

                $handle->file_new_name_body = $filename;
                $filePath = $file_dir;
                //'pos/img/customers/'.$this->Customer->getLastInsertID().'/'
                $path = 'pos/img/'.$this->dirToUrl($dir).'out/'.$filename.".".$handle->file_src_name_ext;
                //$path = IMAGES_URL.'Pos/'.$this->dirToUrl($dir).$filename.".".$handle->file_src_name_ext;
                $handle->image_resize = true;
                $handle->image_ratio_y = true;
                $handle->image_x = 122;
                $handle->process($filePath);
                if(!$handle->processed){
                    return FALSE;
                }
                $return['image'] = $path;
            }
            return $return;                    
        }else{
            return FALSE;
        }
    }

    public function downloadFile($file){
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }
}
?>